-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 11, 2012 at 10:09 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dag_db`
--
CREATE DATABASE `dag_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
USE `dag_db`;

DELIMITER $$
--
-- Functions
--
DROP FUNCTION IF EXISTS `GET_STOCK_ONHAND`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GET_STOCK_ONHAND`(id_item INT, id_lot INT, id_package INT, id_pack INT, id_location INT, id_makloon INT) RETURNS decimal(30,3)
BEGIN
    DECLARE opening_balance DECIMAL(30, 3) DEFAULT 0.000;
    DECLARE stock_source DECIMAL(30, 3) DEFAULT 0.000;
    DECLARE stock_dest DECIMAL(30, 3) DEFAULT 0.000;
    DECLARE date_cutoff DATETIME;
    
    SET date_cutoff = (
        SELECT MAX(posting_date) FROM stock_balances 
        WHERE pk_id_item = id_item 
        AND IF(id_makloon IS NULL, pk_id_partner IS NULL, pk_id_partner = id_makloon)
        AND stock_balance_state = 'Done'
    );
    
    SET opening_balance = GET_STOCK_OP_BALANCE(id_item, id_lot, id_package, id_pack, id_location, id_makloon, date_cutoff);
    
    SELECT SUM(UOM_CONVERSION(quantity, sm.pk_id_item_units_of_measure, it.pk_id_item_units_of_measure))
    INTO stock_source
    FROM stock_moves sm
    JOIN stock_locations sl ON sm.source_location_id = sl.pk_id_stock_location   
        AND IF(id_location = 'ALL', TRUE, sm.source_location_id = id_location)
        AND location_type = 'Internal Location'
    JOIN items it ON sm.pk_id_item = it.pk_id_item
    WHERE sm.pk_id_item = id_item
    AND stock_move_state = 'Done'
    AND stock_move_date >= IFNULL(date_cutoff, '0000-00-00')
    AND IF(id_makloon IS NULL, pk_id_partner IS NULL, pk_id_partner = id_makloon)
    AND IF(id_lot = 'ALL', TRUE, IF(id_lot IS NULL, pk_id_production_lot IS NULL, pk_id_production_lot = id_lot))
    AND IF(id_package = 'ALL', TRUE, IF(id_package IS NULL, pk_id_item_package IS NULL, pk_id_item_package = id_package))
    AND IF(id_pack = 'ALL', TRUE, IF(id_pack IS NULL, pk_id_item_pack IS NULL, pk_id_item_pack = id_pack));
    
    SELECT SUM(UOM_CONVERSION(quantity, sm.pk_id_item_units_of_measure, it.pk_id_item_units_of_measure))
    INTO stock_dest
    FROM stock_moves sm
    JOIN stock_locations sl ON sm.destination_location_id = sl.pk_id_stock_location 
        AND IF(id_location = 'ALL', TRUE, sm.destination_location_id = id_location)
        AND location_type = 'Internal Location'
    JOIN items it ON sm.pk_id_item = it.pk_id_item
    WHERE sm.pk_id_item = id_item
    AND stock_move_state = 'Done'
    AND stock_move_date >= IFNULL(date_cutoff, '0000-00-00')
    AND IF(id_makloon IS NULL, pk_id_partner IS NULL, pk_id_partner = id_makloon)
    AND IF(id_lot = 'ALL', TRUE, IF(id_lot IS NULL, pk_id_production_lot IS NULL, pk_id_production_lot = id_lot))
    AND IF(id_package = 'ALL', TRUE, IF(id_package IS NULL, pk_id_item_package IS NULL, pk_id_item_package = id_package))
    AND IF(id_pack = 'ALL', TRUE, IF(id_pack IS NULL, pk_id_item_pack IS NULL, pk_id_item_pack = id_pack));
    
    RETURN IFNULL(opening_balance, 0.000) + 
        (IFNULL(stock_dest, 0.000) - IFNULL(stock_source, 0.000));
END$$

DROP FUNCTION IF EXISTS `GET_STOCK_OP_BALANCE`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GET_STOCK_OP_BALANCE`(id_item INT, id_lot INT, id_package INT, id_pack INT, id_location INT, id_makloon INT, opening_balance_date DATETIME) RETURNS decimal(30,3)
BEGIN
    DECLARE opening_balance DECIMAL(30, 3) DEFAULT 0.000;
    DECLARE date_opening_balance DATETIME;
    
    SET date_opening_balance = IF(opening_balance_date IS NULL, (
        SELECT MAX(posting_date) FROM stock_balances 
        WHERE pk_id_item = id_item 
        AND IF(id_makloon IS NULL, pk_id_partner IS NULL, pk_id_partner = id_makloon)
        AND IF(id_lot = 'ALL', TRUE, IF(id_lot IS NULL, pk_id_production_lot IS NULL, pk_id_production_lot = id_lot))
        AND IF(id_package = 'ALL', TRUE, IF(id_package IS NULL, pk_id_item_package IS NULL, pk_id_item_package = id_package))
        AND IF(id_pack = 'ALL', TRUE, IF(id_pack IS NULL, pk_id_item_pack IS NULL, pk_id_item_pack = id_pack))
        AND IF(id_location = 'ALL', TRUE, pk_id_stock_location = id_location)
        AND stock_balance_state = 'Done'
    ), opening_balance_date);
    
    SELECT SUM(UOM_CONVERSION(balance_quantity, sb.pk_id_item_units_of_measure, it.pk_id_item_units_of_measure)) 
    INTO opening_balance
    FROM stock_balances sb
    JOIN stock_locations sl ON sb.pk_id_stock_location = sl.pk_id_stock_location 
        AND IF(id_location = 'ALL', TRUE, sb.pk_id_stock_location = id_location)
        AND location_type = 'Internal Location'
    JOIN items it ON sb.pk_id_item = it.pk_id_item
    WHERE sb.pk_id_item = id_item
    AND posting_date = IFNULL(date_opening_balance, '0000-00-00')
    AND stock_balance_state = 'Done'
    AND IF(id_makloon IS NULL, pk_id_partner IS NULL, pk_id_partner = id_makloon)
    AND IF(id_lot = 'ALL', TRUE, IF(id_lot IS NULL, pk_id_production_lot IS NULL, pk_id_production_lot = id_lot))
    AND IF(id_package = 'ALL', TRUE, IF(id_package IS NULL, pk_id_item_package IS NULL, pk_id_item_package = id_package))
    AND IF(id_pack = 'ALL', TRUE, IF(id_pack IS NULL, pk_id_item_pack IS NULL, pk_id_item_pack = id_pack));
    
    RETURN IFNULL(opening_balance, 0.000);
END$$

DROP FUNCTION IF EXISTS `UOM_CONVERSION`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `UOM_CONVERSION`(qty DECIMAL(30, 3), id_source_uom INT, id_dest_uom INT) RETURNS decimal(30,3)
BEGIN
    DECLARE qty_converted DECIMAL(30, 3) DEFAULT 0.000;
    DECLARE source_ratio DECIMAL(30, 10) DEFAULT 0.0000000000;    
    DECLARE dest_ratio DECIMAL(30, 10) DEFAULT 0.0000000000;
    
    IF id_source_uom <> id_dest_uom THEN
        
        SELECT ratio INTO source_ratio
        FROM item_units_of_measures
        WHERE pk_id_item_units_of_measure = id_source_uom;
        
        SELECT ratio INTO dest_ratio
        FROM item_units_of_measures
        WHERE pk_id_item_units_of_measure = id_dest_uom;
        
        SET qty_converted = qty * dest_ratio / source_ratio;
    ELSE
        SET qty_converted = qty;
    END IF;
    RETURN IFNULL(qty_converted, 0.000);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `pk_id_city` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `pk_id_country` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_city`),
  KEY `pk_id_country` (`pk_id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`pk_id_city`, `city_name`, `pk_id_country`, `inserted_date`) VALUES
(1, 'Bandung', 2, '2012-05-25 15:32:58'),
(2, 'North Jakarta', 2, '2012-05-25 15:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

DROP TABLE IF EXISTS `configurations`;
CREATE TABLE IF NOT EXISTS `configurations` (
  `pk_id_configuration` int(11) NOT NULL AUTO_INCREMENT,
  `application_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `company_name` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `company_address` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `company_contact_number_1` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `company_contact_number_2` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `company_logo` varchar(16) COLLATE latin1_general_ci NOT NULL DEFAULT 'company_logo.png',
  `after_login_messages` text COLLATE latin1_general_ci,
  PRIMARY KEY (`pk_id_configuration`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`pk_id_configuration`, `application_name`, `company_name`, `company_address`, `company_contact_number_1`, `company_contact_number_2`, `company_logo`, `after_login_messages`) VALUES
(1, 'DAG Application', 'PT. Daya Aditya Gemilang', 'Jln. Ahmad Syam', '0224613982', NULL, 'company_logo.png', '<div style="text-align: right; "><ol><li><font face="Arial, Verdana" size="2"><b>Professional !!</b></font></li><li style="text-align: left; "><font face="Arial, Verdana" size="2"><b>Professional</b></font></li><li style="text-align: center; "><font face="Arial, Verdana" size="2"><b>Pro Abis</b></font></li></ol></div>');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `pk_id_country` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`pk_id_country`, `country_name`, `inserted_date`) VALUES
(1, 'America', '2012-05-25 15:32:30'),
(2, 'Indonesia', '2012-05-25 15:32:34');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `currency_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `currency_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `currencies`
--


-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `pk_id_employee` int(11) NOT NULL AUTO_INCREMENT,
  `employee_code` char(6) COLLATE latin1_general_ci NOT NULL,
  `employee_name` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `identification_id` char(20) COLLATE latin1_general_ci NOT NULL,
  `home_address` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `work_phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `work_email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `gender` varchar(6) COLLATE latin1_general_ci NOT NULL,
  `marital_status` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `pk_id_city` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_employee`),
  UNIQUE KEY `employee_code` (`employee_code`),
  KEY `pk_id_city` (`pk_id_city`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`pk_id_employee`, `employee_code`, `employee_name`, `identification_id`, `home_address`, `work_phone`, `mobile`, `work_email`, `gender`, `marital_status`, `date_of_birth`, `pk_id_city`, `inserted_date`) VALUES
(1, '120501', 'Barry', '091919829829', 'Jalan jalan', '+628179332228', '+628172372827', 'email@email.com', 'Male', 'Single', '1980-05-08', 2, '2012-05-29 13:19:07'),
(2, '120601', 'Demo Name', 'Demo ID', 'Demo Address', '08176256336', '08176256336', 'email.2@gmail.co.id', 'Female', 'Married', '2012-06-03', 2, '2012-06-15 14:55:03');

-- --------------------------------------------------------

--
-- Table structure for table `hr_departments`
--

DROP TABLE IF EXISTS `hr_departments`;
CREATE TABLE IF NOT EXISTS `hr_departments` (
  `pk_id_hr_department` int(11) NOT NULL AUTO_INCREMENT,
  `hr_department_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `parent_hr_department_id` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_hr_department`),
  KEY `parent_hr_department_id` (`parent_hr_department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `hr_departments`
--


-- --------------------------------------------------------

--
-- Table structure for table `hr_job_positions`
--

DROP TABLE IF EXISTS `hr_job_positions`;
CREATE TABLE IF NOT EXISTS `hr_job_positions` (
  `pk_id_hr_job_position` int(11) NOT NULL AUTO_INCREMENT,
  `hr_job_position_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `pk_id_hr_department` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_hr_job_position`),
  KEY `pk_id_hr_department` (`pk_id_hr_department`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `hr_job_positions`
--


-- --------------------------------------------------------

--
-- Table structure for table `item_bill_of_materials`
--

DROP TABLE IF EXISTS `item_bill_of_materials`;
CREATE TABLE IF NOT EXISTS `item_bill_of_materials` (
  `pk_id_item_bill_of_material` int(11) NOT NULL AUTO_INCREMENT,
  `bom_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_bill_of_material`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `item_bill_of_materials`
--

INSERT INTO `item_bill_of_materials` (`pk_id_item_bill_of_material`, `bom_name`, `pk_id_item`, `quantity`, `pk_id_item_units_of_measure`, `is_active`, `inserted_date`) VALUES
(1, 'Thread for NE - 7', 2, 30.000, 4, 1, '2012-05-25 19:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `item_bom_details`
--

DROP TABLE IF EXISTS `item_bom_details`;
CREATE TABLE IF NOT EXISTS `item_bom_details` (
  `pk_id_bom_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `pk_id_item_bill_of_material` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_bom_detail`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_item_bill_of_material` (`pk_id_item_bill_of_material`),
  KEY `pk_id_item` (`pk_id_item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=58 ;

--
-- Dumping data for table `item_bom_details`
--

INSERT INTO `item_bom_details` (`pk_id_bom_detail`, `pk_id_item`, `quantity`, `pk_id_item_units_of_measure`, `pk_id_item_bill_of_material`, `inserted_date`) VALUES
(55, 2, 100.000, 4, 1, '2012-05-30 13:33:16'),
(56, 1, 200.000, 4, 1, '2012-05-30 13:33:16'),
(57, 3, 1.000, 3, 1, '2012-05-30 13:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `item_categories`
--

DROP TABLE IF EXISTS `item_categories`;
CREATE TABLE IF NOT EXISTS `item_categories` (
  `pk_id_item_category` int(11) NOT NULL AUTO_INCREMENT,
  `item_category_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `parent_item_category_id` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_category`),
  KEY `parent_item_category_id` (`parent_item_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item_categories`
--

INSERT INTO `item_categories` (`pk_id_item_category`, `item_category_name`, `parent_item_category_id`, `inserted_date`) VALUES
(1, 'Cotton', NULL, '2012-05-25 15:22:47'),
(2, 'Waste', 1, '2012-05-25 15:23:06'),
(3, 'Thread', NULL, '2012-05-25 15:23:12'),
(4, 'Spare Part', NULL, '2012-05-25 15:23:23'),
(5, 'Item', NULL, '2012-05-25 15:23:26'),
(6, 'Demo Item Category', NULL, '2012-06-27 23:29:34');

-- --------------------------------------------------------

--
-- Table structure for table `item_deliveries`
--

DROP TABLE IF EXISTS `item_deliveries`;
CREATE TABLE IF NOT EXISTS `item_deliveries` (
  `pk_id_item_delivery` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_delivery_code` char(11) COLLATE latin1_general_ci NOT NULL,
  `item_delivery_date` datetime NOT NULL,
  `item_delivery_notes` text COLLATE latin1_general_ci,
  `vehicle_type` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `vehicle_number` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `vehicle_color` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `pk_id_partner` int(11) NOT NULL,
  `delivery_address` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `delivery_type` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `item_delivery_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_delivery`),
  KEY `pk_id_partner` (`pk_id_partner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_deliveries`
--

INSERT INTO `item_deliveries` (`pk_id_item_delivery`, `item_delivery_code`, `item_delivery_date`, `item_delivery_notes`, `vehicle_type`, `vehicle_number`, `vehicle_color`, `pk_id_partner`, `delivery_address`, `delivery_type`, `item_delivery_state`, `inserted_date`) VALUES
(2, 'DO/12060001', '2012-06-15 15:31:00', NULL, NULL, NULL, NULL, 1, NULL, 'Purchase Return', 'Done', '2012-06-15 15:37:03'),
(3, 'DO/12070001', '2012-07-05 17:17:00', '', '', '', '', 5, 'St. Dolor Sit Amet', 'Delivery Item', 'Done', '2012-07-05 17:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `item_internal_moves`
--

DROP TABLE IF EXISTS `item_internal_moves`;
CREATE TABLE IF NOT EXISTS `item_internal_moves` (
  `pk_id_item_internal_move` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_internal_move_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `item_internal_move_date` datetime NOT NULL,
  `item_internal_move_notes` text COLLATE latin1_general_ci,
  `internal_move_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_internal_move`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_internal_moves`
--

INSERT INTO `item_internal_moves` (`pk_id_item_internal_move`, `item_internal_move_code`, `item_internal_move_date`, `item_internal_move_notes`, `internal_move_state`, `inserted_date`) VALUES
(2, 'INT/12060001', '2012-06-15 19:07:00', NULL, 'Draft', '2012-06-15 19:08:00'),
(3, 'INT/12060002', '2012-06-15 19:08:00', NULL, 'Done', '2012-06-15 19:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `item_packages`
--

DROP TABLE IF EXISTS `item_packages`;
CREATE TABLE IF NOT EXISTS `item_packages` (
  `pk_id_item_packages` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `standard_weight` decimal(30,3) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_packages`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `item_packages`
--

INSERT INTO `item_packages` (`pk_id_item_packages`, `package_name`, `standard_weight`, `inserted_date`) VALUES
(1, 'BAL', 2.000, '2012-05-25 15:22:18'),
(2, 'bag', 0.250, '2012-06-09 01:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `item_packs`
--

DROP TABLE IF EXISTS `item_packs`;
CREATE TABLE IF NOT EXISTS `item_packs` (
  `pk_id_item_pack` bigint(20) NOT NULL AUTO_INCREMENT,
  `pack_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_pack`),
  UNIQUE KEY `pack_name` (`pack_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `item_packs`
--

INSERT INTO `item_packs` (`pk_id_item_pack`, `pack_name`, `inserted_date`) VALUES
(1, '000001', '2012-05-29 18:59:58'),
(2, '000002', '2012-05-29 18:59:59'),
(3, '000003', '2012-06-01 00:11:30'),
(4, '000004', '2012-06-01 00:11:30'),
(5, '000005', '2012-06-05 14:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `item_receipts`
--

DROP TABLE IF EXISTS `item_receipts`;
CREATE TABLE IF NOT EXISTS `item_receipts` (
  `pk_id_item_receipt` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_purchase_order` bigint(20) DEFAULT NULL,
  `item_receipt_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `item_receipt_date` datetime NOT NULL,
  `item_receipt_schedule_date` datetime DEFAULT NULL,
  `item_receipt_next_schedule_date` datetime DEFAULT NULL,
  `item_receipt_next_receive_control` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `item_receipt_notes` text COLLATE latin1_general_ci,
  `partner_delivery_order` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `partner_pic` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `partner_vehicle_type` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `partner_vehicle_number` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `partner_vehicle_color` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `pic_phone_number` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `pk_id_partner` int(11) NOT NULL,
  `receipt_type` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `item_receipt_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_receipt`),
  UNIQUE KEY `item_receipt_code` (`item_receipt_code`),
  KEY `pk_id_partner` (`pk_id_partner`),
  KEY `pk_id_purchase_order` (`pk_id_purchase_order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_receipts`
--

INSERT INTO `item_receipts` (`pk_id_item_receipt`, `pk_id_purchase_order`, `item_receipt_code`, `item_receipt_date`, `item_receipt_schedule_date`, `item_receipt_next_schedule_date`, `item_receipt_next_receive_control`, `item_receipt_notes`, `partner_delivery_order`, `partner_pic`, `partner_vehicle_type`, `partner_vehicle_number`, `partner_vehicle_color`, `pic_phone_number`, `pk_id_partner`, `receipt_type`, `item_receipt_state`, `inserted_date`) VALUES
(1, NULL, 'IRC/12050001', '2012-05-25 17:42:00', NULL, NULL, NULL, NULL, 'DO-001', 'Sudono', 'Truck', 'D 6991 PDE', 'Yellow', '08172636744', 1, 'Supplier', 'Done', '2012-05-25 18:18:08'),
(2, NULL, 'IRC/12060001', '2012-06-05 14:33:00', NULL, NULL, NULL, NULL, NULL, NULL, 'Sepeda', 'Ga ada', 'Ijo', NULL, 1, 'Supplier', 'Draft', '2012-06-05 14:35:10'),
(3, NULL, 'IRC/12060002', '2012-06-05 14:38:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Supplier', 'Draft', '2012-06-05 14:40:23');

-- --------------------------------------------------------

--
-- Table structure for table `item_release_request_details`
--

DROP TABLE IF EXISTS `item_release_request_details`;
CREATE TABLE IF NOT EXISTS `item_release_request_details` (
  `pk_id_item_release_request_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_item_release_request` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `release_request_detail_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_release_request_detail_id`),
  KEY `pk_id_item_release_request` (`pk_id_item_release_request`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `item_release_request_details`
--

INSERT INTO `item_release_request_details` (`pk_id_item_release_request_detail_id`, `pk_id_item_release_request`, `pk_id_item`, `quantity`, `release_request_detail_state`, `pk_id_item_units_of_measure`, `inserted_date`) VALUES
(1, 1, 1, 2000.000, 'Draft', 4, '2012-06-15 21:30:53');

-- --------------------------------------------------------

--
-- Table structure for table `item_release_requests`
--

DROP TABLE IF EXISTS `item_release_requests`;
CREATE TABLE IF NOT EXISTS `item_release_requests` (
  `pk_id_item_release_request` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_release_request_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `item_release_request_date` datetime NOT NULL,
  `item_release_request_notes` text COLLATE latin1_general_ci,
  `item_release_request_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_release_request`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `item_release_requests`
--

INSERT INTO `item_release_requests` (`pk_id_item_release_request`, `item_release_request_code`, `item_release_request_date`, `item_release_request_notes`, `item_release_request_state`, `inserted_date`) VALUES
(1, 'IRQ/12060001', '2012-06-15 21:26:00', 'dsadsa', 'Draft', '2012-06-15 21:30:53');

-- --------------------------------------------------------

--
-- Table structure for table `item_releases`
--

DROP TABLE IF EXISTS `item_releases`;
CREATE TABLE IF NOT EXISTS `item_releases` (
  `pk_id_item_release` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_release_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `item_release_date` datetime NOT NULL,
  `item_release_notes` text COLLATE latin1_general_ci,
  `pk_id_item_release_request` bigint(20) DEFAULT NULL,
  `pk_id_manufacture_order` bigint(20) DEFAULT NULL,
  `pk_id_maintenance_machine` bigint(20) DEFAULT NULL,
  `pk_id_maintenance_repair_order` bigint(20) DEFAULT NULL,
  `release_reason` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'Other',
  `item_release_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_release`),
  KEY `pk_id_item_release_request` (`pk_id_item_release_request`),
  KEY `pk_id_manufacture_order` (`pk_id_manufacture_order`),
  KEY `pk_id_maintenance_machine` (`pk_id_maintenance_machine`),
  KEY `pk_id_maintenance_repair_order` (`pk_id_maintenance_repair_order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `item_releases`
--

INSERT INTO `item_releases` (`pk_id_item_release`, `item_release_code`, `item_release_date`, `item_release_notes`, `pk_id_item_release_request`, `pk_id_manufacture_order`, `pk_id_maintenance_machine`, `pk_id_maintenance_repair_order`, `release_reason`, `item_release_state`, `inserted_date`) VALUES
(1, 'IRL/12060001', '2012-06-26 01:52:00', NULL, 1, NULL, NULL, NULL, 'Other', 'Draft', '2012-06-26 02:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `item_suppliers`
--

DROP TABLE IF EXISTS `item_suppliers`;
CREATE TABLE IF NOT EXISTS `item_suppliers` (
  `pk_id_item_supplier` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_item` int(11) NOT NULL,
  `pk_id_partner` int(11) NOT NULL,
  `supplier_item_name` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_supplier`),
  UNIQUE KEY `pk_id_item` (`pk_id_item`,`pk_id_partner`),
  KEY `pk_id_partner` (`pk_id_partner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `item_suppliers`
--

INSERT INTO `item_suppliers` (`pk_id_item_supplier`, `pk_id_item`, `pk_id_partner`, `supplier_item_name`, `inserted_date`) VALUES
(19, 1, 1, 'FS - Flat Strip', '2012-06-07 15:55:46'),
(23, 3, 2, NULL, '2012-06-27 21:44:12'),
(24, 4, 4, 'Rotor T010', '2012-06-27 21:49:38'),
(27, 5, 5, 'Item Lorem', '2012-06-28 03:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `item_units_of_measures`
--

DROP TABLE IF EXISTS `item_units_of_measures`;
CREATE TABLE IF NOT EXISTS `item_units_of_measures` (
  `pk_id_item_units_of_measure` int(11) NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `ratio` decimal(30,10) NOT NULL,
  `pk_id_item_uom_category` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_units_of_measure`),
  KEY `pk_id_item_uom_category` (`pk_id_item_uom_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item_units_of_measures`
--

INSERT INTO `item_units_of_measures` (`pk_id_item_units_of_measure`, `uom_name`, `ratio`, `pk_id_item_uom_category`, `is_active`, `inserted_date`) VALUES
(3, 'PCE', 1.0000000000, 2, 1, '2012-05-25 15:21:33'),
(4, 'kg', 1.0000000000, 1, 1, '2012-05-25 15:21:51'),
(5, 'bale', 181.4400000000, 1, 1, '2012-05-25 15:22:00'),
(6, 'm', 1.0000000000, 3, 1, '2012-06-25 19:38:00'),
(7, 'km', 0.0010000000, 3, 0, '2012-06-25 19:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `item_uom_categories`
--

DROP TABLE IF EXISTS `item_uom_categories`;
CREATE TABLE IF NOT EXISTS `item_uom_categories` (
  `pk_id_item_uom_category` int(11) NOT NULL AUTO_INCREMENT,
  `uom_category_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item_uom_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item_uom_categories`
--

INSERT INTO `item_uom_categories` (`pk_id_item_uom_category`, `uom_category_name`, `inserted_date`) VALUES
(1, 'Weight', '2012-05-25 15:19:19'),
(2, 'Unit', '2012-05-25 15:19:21'),
(3, 'Length', '2012-06-25 19:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `pk_id_item` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `pk_id_item_category` int(11) NOT NULL,
  `is_can_be_sold` tinyint(4) NOT NULL DEFAULT '1',
  `is_can_be_purchased` tinyint(4) NOT NULL DEFAULT '1',
  `is_can_be_produced` tinyint(4) NOT NULL DEFAULT '1',
  `sale_price` decimal(30,2) NOT NULL DEFAULT '0.00',
  `cost_price` decimal(30,2) NOT NULL DEFAULT '0.00',
  `life_time` int(11) DEFAULT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `stock_on_sale_posting` decimal(30,3) NOT NULL DEFAULT '0.000',
  `stock_on_purchase_posting` decimal(30,3) NOT NULL DEFAULT '0.000',
  `stock_on_produce_posting` decimal(30,3) NOT NULL DEFAULT '0.000',
  `location_production_id` int(11) DEFAULT NULL,
  `location_loss_id` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_item`),
  KEY `default_uom_id` (`pk_id_item_units_of_measure`),
  KEY `location_production_id` (`location_production_id`),
  KEY `location_loss_id` (`location_loss_id`),
  KEY `pk_id_item_category` (`pk_id_item_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`pk_id_item`, `item_name`, `pk_id_item_category`, `is_can_be_sold`, `is_can_be_purchased`, `is_can_be_produced`, `sale_price`, `cost_price`, `life_time`, `pk_id_item_units_of_measure`, `stock_on_sale_posting`, `stock_on_purchase_posting`, `stock_on_produce_posting`, `location_production_id`, `location_loss_id`, `is_active`, `inserted_date`) VALUES
(1, '[FS] Cotton - Flat Strip', 2, 1, 1, 0, 20000.00, 13000.00, NULL, 4, 0.000, 0.000, 0.000, 10, 12, 1, '2012-05-25 15:26:15'),
(2, '[NE - 7] Thread', 3, 1, 0, 1, 25000.00, 0.00, NULL, 5, 0.000, 0.000, 0.000, 10, 12, 1, '2012-05-25 15:29:51'),
(3, 'cones', 5, 0, 1, 0, 0.00, 200.00, NULL, 3, 0.000, 0.000, 0.000, 10, 12, 1, '2012-05-25 15:31:12'),
(4, 'rotor', 4, 0, 1, 0, 0.00, 0.00, 365, 3, 0.000, 0.000, 0.000, NULL, 12, 1, '2012-06-27 21:49:38'),
(5, 'Demo Item', 6, 1, 1, 1, 10000.00, 5000.00, NULL, 4, 0.000, 0.000, 0.000, 10, 12, 1, '2012-06-27 23:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `maintenance_machine_consumed_items`
--

DROP TABLE IF EXISTS `maintenance_machine_consumed_items`;
CREATE TABLE IF NOT EXISTS `maintenance_machine_consumed_items` (
  `pk_id_maintenance_machine_consumed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_stock_move` bigint(20) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `item_cost` decimal(30,2) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_machine_consumed_item`),
  KEY `pk_id_stock_move` (`pk_id_stock_move`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_machine_consumed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `maintenance_machine_needed_items`
--

DROP TABLE IF EXISTS `maintenance_machine_needed_items`;
CREATE TABLE IF NOT EXISTS `maintenance_machine_needed_items` (
  `pk_id_maintenance_machine_needed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_maintenance_machine` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_machine_needed_item`),
  KEY `pk_id_maintenance_machine` (`pk_id_maintenance_machine`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_item` (`pk_id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_machine_needed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `maintenance_machines`
--

DROP TABLE IF EXISTS `maintenance_machines`;
CREATE TABLE IF NOT EXISTS `maintenance_machines` (
  `pk_id_maintenance_machine` bigint(20) NOT NULL AUTO_INCREMENT,
  `maintenance_machine_code` char(13) COLLATE latin1_general_ci NOT NULL,
  `maintenance_machine_date` datetime NOT NULL,
  `pk_id_production_machine` int(11) NOT NULL,
  `maintenance_machine_notes` text COLLATE latin1_general_ci,
  `maintenance_machine_state` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_machine`),
  KEY `pk_id_machine` (`pk_id_production_machine`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_machines`
--


-- --------------------------------------------------------

--
-- Table structure for table `maintenance_repair_order_consumed_items`
--

DROP TABLE IF EXISTS `maintenance_repair_order_consumed_items`;
CREATE TABLE IF NOT EXISTS `maintenance_repair_order_consumed_items` (
  `pk_id_maintenance_repair_order_consumed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_stock_move` bigint(20) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `item_cost` decimal(30,2) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_repair_order_consumed_item`),
  KEY `pk_id_stock_move` (`pk_id_stock_move`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_repair_order_consumed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `maintenance_repair_order_needed_items`
--

DROP TABLE IF EXISTS `maintenance_repair_order_needed_items`;
CREATE TABLE IF NOT EXISTS `maintenance_repair_order_needed_items` (
  `pk_id_maintenance_repair_order_needed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_maintenance_repair_order` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_repair_order_needed_item`),
  KEY `pk_id_maintenance_repair_order` (`pk_id_maintenance_repair_order`),
  KEY `pk_id_item` (`pk_id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_repair_order_needed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `maintenance_repair_orders`
--

DROP TABLE IF EXISTS `maintenance_repair_orders`;
CREATE TABLE IF NOT EXISTS `maintenance_repair_orders` (
  `pk_id_maintenance_repair_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `maintenance_repair_order_code` char(13) COLLATE latin1_general_ci NOT NULL,
  `maintenance_repair_order_date` datetime NOT NULL,
  `pk_id_production_machine` int(11) NOT NULL,
  `repair_cost` decimal(30,2) NOT NULL,
  `maintenance_repair_order_notes` text COLLATE latin1_general_ci,
  `maintenance_repair_order_state` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_maintenance_repair_order`),
  KEY `pk_id_machine` (`pk_id_production_machine`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `maintenance_repair_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_consumed_items`
--

DROP TABLE IF EXISTS `manufacture_consumed_items`;
CREATE TABLE IF NOT EXISTS `manufacture_consumed_items` (
  `pk_id_manufacture_consumed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_stock_move` bigint(20) NOT NULL,
  `pk_id_manufacture_order` bigint(20) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `item_cost` decimal(30,2) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_consumed_item`),
  KEY `pk_id_stock_move` (`pk_id_stock_move`),
  KEY `pk_id_manufacture_order` (`pk_id_manufacture_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_consumed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_incident_memos`
--

DROP TABLE IF EXISTS `manufacture_incident_memos`;
CREATE TABLE IF NOT EXISTS `manufacture_incident_memos` (
  `pk_id_manufacture_incident_memo` bigint(20) NOT NULL AUTO_INCREMENT,
  `manufacture_incident_memo_date` datetime NOT NULL,
  `manufacture_incident_memo_notes` text COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_incident_memo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_incident_memos`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_incident_work_order_impacts`
--

DROP TABLE IF EXISTS `manufacture_incident_work_order_impacts`;
CREATE TABLE IF NOT EXISTS `manufacture_incident_work_order_impacts` (
  `pk_id_manufacture_incident_work_order_impact` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_manufacture_incident_memo` bigint(20) NOT NULL,
  `pk_id_manufacture_work_order` bigint(20) NOT NULL,
  `impact_notes` text COLLATE latin1_general_ci,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_incident_work_order_impact`),
  KEY `pk_id_manufacture_incident_memo` (`pk_id_manufacture_incident_memo`),
  KEY `pk_id_manufacture_work_order` (`pk_id_manufacture_work_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_incident_work_order_impacts`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_machines`
--

DROP TABLE IF EXISTS `manufacture_machines`;
CREATE TABLE IF NOT EXISTS `manufacture_machines` (
  `pk_id_manufacture_machine` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_manufacture_work_order` bigint(20) NOT NULL,
  `pk_id_production_machine` int(11) NOT NULL,
  `is_finished` tinyint(4) NOT NULL DEFAULT '0',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_machine`),
  KEY `pk_id_production_machine` (`pk_id_production_machine`),
  KEY `pk_id_manufacture_work_order` (`pk_id_manufacture_work_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_machines`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_needed_items`
--

DROP TABLE IF EXISTS `manufacture_needed_items`;
CREATE TABLE IF NOT EXISTS `manufacture_needed_items` (
  `pk_id_manufacture_needed_item` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `pk_id_manufacture_order` bigint(20) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_needed_item`),
  KEY `pk_id_manufacture_order` (`pk_id_manufacture_order`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_item` (`pk_id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_needed_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_orders`
--

DROP TABLE IF EXISTS `manufacture_orders`;
CREATE TABLE IF NOT EXISTS `manufacture_orders` (
  `pk_id_manufacture_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `manufacture_order_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `manufacture_order_date` datetime NOT NULL,
  `manufacture_expected_finish_date` datetime NOT NULL,
  `pk_id_partner` int(11) DEFAULT NULL,
  `manufacture_order_notes` text COLLATE latin1_general_ci,
  `pk_id_production_lot` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `pk_id_item_bill_of_material` int(11) NOT NULL,
  `manufacture_order_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_order`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_production_lot` (`pk_id_production_lot`),
  KEY `pk_id_partner` (`pk_id_partner`),
  KEY `pk_id_item_bill_of_material` (`pk_id_item_bill_of_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `manufacture_work_orders`
--

DROP TABLE IF EXISTS `manufacture_work_orders`;
CREATE TABLE IF NOT EXISTS `manufacture_work_orders` (
  `pk_id_manufacture_work_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_production_work_center` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `total_cycle` int(11) NOT NULL,
  `standard_cost` decimal(30,2) NOT NULL,
  `formula_time` time NOT NULL DEFAULT '00:00:00',
  `pk_id_manufacture_order` bigint(20) NOT NULL,
  `manufacture_work_order_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_manufacture_work_order`),
  KEY `pk_id_manufacture_order` (`pk_id_manufacture_order`),
  KEY `pk_id_production_work_center` (`pk_id_production_work_center`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `manufacture_work_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `partner_categories`
--

DROP TABLE IF EXISTS `partner_categories`;
CREATE TABLE IF NOT EXISTS `partner_categories` (
  `pk_id_partner_category` int(11) NOT NULL AUTO_INCREMENT,
  `partner_category_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `parent_partner_category_id` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_partner_category`),
  KEY `parent_partner_category_id` (`parent_partner_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `partner_categories`
--

INSERT INTO `partner_categories` (`pk_id_partner_category`, `partner_category_name`, `parent_partner_category_id`, `inserted_date`) VALUES
(1, 'All Supplier', NULL, '2012-06-20 22:32:09'),
(2, 'Supplier Local', 1, '2012-06-20 22:32:28'),
(3, 'Supplier Abroad', 1, '2012-06-20 22:32:46'),
(4, 'All Customer', NULL, '2012-06-20 22:32:55'),
(5, 'Buyer', 4, '2012-06-20 22:33:05'),
(6, 'Makloon', 4, '2012-06-20 22:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE IF NOT EXISTS `partners` (
  `pk_id_partner` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(200) COLLATE latin1_general_ci NOT NULL,
  `contact_name` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `street` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `is_supplier` tinyint(4) NOT NULL DEFAULT '1',
  `is_makloon` tinyint(4) NOT NULL DEFAULT '1',
  `is_buyer` tinyint(4) NOT NULL DEFAULT '1',
  `supplier_location_id` int(11) DEFAULT NULL,
  `buyer_location_id` int(11) DEFAULT NULL,
  `makloon_location_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `pk_id_city` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_partner`),
  KEY `pk_id_cities` (`pk_id_city`),
  KEY `supplier_location_id` (`supplier_location_id`),
  KEY `buyer_location_id` (`buyer_location_id`),
  KEY `makloon_location_id` (`makloon_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`pk_id_partner`, `partner_name`, `contact_name`, `street`, `zip`, `phone`, `mobile`, `fax`, `email`, `is_supplier`, `is_makloon`, `is_buyer`, `supplier_location_id`, `buyer_location_id`, `makloon_location_id`, `is_active`, `pk_id_city`, `inserted_date`) VALUES
(1, 'PT. Penghasil Kapas', 'Bpk. Mulyadi', 'Jln. Ruko Street. St.', 14520, '0224330239', '081738273829', '022455389', 'mulyadi.adi@pengasilkapas.com', 1, 0, 0, 6, NULL, NULL, 1, 1, '2012-05-25 15:38:24'),
(2, 'Warung Cones', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 6, 8, NULL, 1, 1, '2012-05-25 15:39:29'),
(3, 'PT. Makloon Supadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, NULL, 7, 7, 1, NULL, '2012-05-31 20:50:38'),
(4, 'PT. Machine GGTQ', 'Terminator T-100', 'Jln. Sky Net', 99999, NULL, '081723837448', NULL, 'terminator@t-100.com', 1, 0, 0, 6, NULL, NULL, 1, NULL, '2012-06-27 21:48:22'),
(5, 'Demo Partner', 'Mr. Lorem Ipsum', 'St. Dolor Sit Amet', 102930, '0229384789', NULL, NULL, 'lorem@amet.sit', 1, 1, 1, 6, 8, 7, 1, NULL, '2012-06-27 23:31:27');

-- --------------------------------------------------------

--
-- Table structure for table `production_departments`
--

DROP TABLE IF EXISTS `production_departments`;
CREATE TABLE IF NOT EXISTS `production_departments` (
  `pk_id_production_department` int(11) NOT NULL AUTO_INCREMENT,
  `production_department_name` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_department`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `production_departments`
--

INSERT INTO `production_departments` (`pk_id_production_department`, `production_department_name`, `inserted_date`) VALUES
(1, 'Blowing', '2012-05-25 15:40:33'),
(2, 'Carding', '2012-05-25 15:40:43'),
(3, 'Open End', '2012-05-25 15:40:49');

-- --------------------------------------------------------

--
-- Table structure for table `production_lots`
--

DROP TABLE IF EXISTS `production_lots`;
CREATE TABLE IF NOT EXISTS `production_lots` (
  `pk_id_production_lot` bigint(20) NOT NULL AUTO_INCREMENT,
  `production_lot_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_lot`),
  UNIQUE KEY `production_lot_name` (`production_lot_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `production_lots`
--

INSERT INTO `production_lots` (`pk_id_production_lot`, `production_lot_name`, `inserted_date`) VALUES
(1, 'HRS-12-801', '2012-05-31 22:33:05'),
(2, 'ARG-12-234', '2012-05-31 22:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `production_machine_categories`
--

DROP TABLE IF EXISTS `production_machine_categories`;
CREATE TABLE IF NOT EXISTS `production_machine_categories` (
  `pk_id_production_machine_category` int(11) NOT NULL AUTO_INCREMENT,
  `machine_category_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pk_id_production_department` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_machine_category`),
  KEY `pk_id_production_department` (`pk_id_production_department`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `production_machine_categories`
--

INSERT INTO `production_machine_categories` (`pk_id_production_machine_category`, `machine_category_name`, `pk_id_production_department`, `inserted_date`) VALUES
(1, '[SA - 1] Blowing Machine', 1, '2012-05-25 15:43:59');

-- --------------------------------------------------------

--
-- Table structure for table `production_machine_parameters`
--

DROP TABLE IF EXISTS `production_machine_parameters`;
CREATE TABLE IF NOT EXISTS `production_machine_parameters` (
  `pk_id_production_machine_parameter` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_description` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `pk_id_machine_categories` int(11) NOT NULL,
  PRIMARY KEY (`pk_id_production_machine_parameter`),
  KEY `pk_id_machine_categories` (`pk_id_machine_categories`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `production_machine_parameters`
--

INSERT INTO `production_machine_parameters` (`pk_id_production_machine_parameter`, `parameter_description`, `pk_id_machine_categories`) VALUES
(3, 'Gear Accelerator', 1),
(4, 'Rotor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `production_machine_spare_parts`
--

DROP TABLE IF EXISTS `production_machine_spare_parts`;
CREATE TABLE IF NOT EXISTS `production_machine_spare_parts` (
  `pk_id_production_machine_spare_part` int(11) NOT NULL AUTO_INCREMENT,
  `pk_id_machine_category` int(11) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_machine_spare_part`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_machine_category` (`pk_id_machine_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `production_machine_spare_parts`
--


-- --------------------------------------------------------

--
-- Table structure for table `production_machines`
--

DROP TABLE IF EXISTS `production_machines`;
CREATE TABLE IF NOT EXISTS `production_machines` (
  `pk_id_production_machine` int(11) NOT NULL AUTO_INCREMENT,
  `machine_code` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `pk_id_machine_category` int(11) NOT NULL,
  `capacity` decimal(30,3) DEFAULT NULL,
  `pk_id_stock_location` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_machine`),
  UNIQUE KEY `machine_code` (`machine_code`),
  KEY `pk_id_machine_category` (`pk_id_machine_category`),
  KEY `pk_id_stock_location` (`pk_id_stock_location`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `production_machines`
--

INSERT INTO `production_machines` (`pk_id_production_machine`, `machine_code`, `pk_id_machine_category`, `capacity`, `pk_id_stock_location`, `is_active`, `inserted_date`) VALUES
(1, '001', 1, 1000.000, 1, 1, '2012-06-25 22:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `production_standard_costs`
--

DROP TABLE IF EXISTS `production_standard_costs`;
CREATE TABLE IF NOT EXISTS `production_standard_costs` (
  `pk_id_production_standard_cost` int(11) NOT NULL AUTO_INCREMENT,
  `standard_cost_description` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `cost` decimal(30,2) NOT NULL,
  `unit_time` varchar(7) COLLATE latin1_general_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_standard_cost`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `production_standard_costs`
--

INSERT INTO `production_standard_costs` (`pk_id_production_standard_cost`, `standard_cost_description`, `cost`, `unit_time`, `is_active`, `inserted_date`) VALUES
(1, 'Electricity', 100.00, 'Minute', 1, '2012-05-25 15:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `production_work_center_cost_details`
--

DROP TABLE IF EXISTS `production_work_center_cost_details`;
CREATE TABLE IF NOT EXISTS `production_work_center_cost_details` (
  `pk_id_production_work_center_cost_detail` int(11) NOT NULL AUTO_INCREMENT,
  `pk_id_production_work_center` int(11) NOT NULL,
  `pk_id_production_standard_cost` int(11) NOT NULL,
  `cost` decimal(30,2) NOT NULL,
  `unit_time` varchar(7) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_work_center_cost_detail`),
  KEY `pk_id_production_work_center` (`pk_id_production_work_center`),
  KEY `pk_id_production_standard_cost` (`pk_id_production_standard_cost`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `production_work_center_cost_details`
--

INSERT INTO `production_work_center_cost_details` (`pk_id_production_work_center_cost_detail`, `pk_id_production_work_center`, `pk_id_production_standard_cost`, `cost`, `unit_time`, `inserted_date`) VALUES
(13, 2, 0, 0.00, '', '2012-06-25 23:23:15'),
(15, 1, 1, 100.00, ' Minute', '2012-06-25 23:23:53'),
(16, 3, 0, 0.00, '', '2012-06-28 03:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `production_work_centers`
--

DROP TABLE IF EXISTS `production_work_centers`;
CREATE TABLE IF NOT EXISTS `production_work_centers` (
  `pk_id_production_work_center` int(11) NOT NULL AUTO_INCREMENT,
  `work_center_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `resource_type` varchar(7) COLLATE latin1_general_ci DEFAULT NULL,
  `pk_id_production_department` int(11) DEFAULT NULL,
  `pk_id_production_machine_category` int(11) DEFAULT NULL,
  `capacity_per_cycle` decimal(30,2) NOT NULL DEFAULT '0.00',
  `time_cycle` time NOT NULL DEFAULT '00:00:00',
  `time_start` time NOT NULL DEFAULT '00:00:00',
  `time_stop` time NOT NULL DEFAULT '00:00:00',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_production_work_center`),
  KEY `pk_id_production_department` (`pk_id_production_department`),
  KEY `pk_id_production_machine_category` (`pk_id_production_machine_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `production_work_centers`
--

INSERT INTO `production_work_centers` (`pk_id_production_work_center`, `work_center_name`, `resource_type`, `pk_id_production_department`, `pk_id_production_machine_category`, `capacity_per_cycle`, `time_cycle`, `time_start`, `time_stop`, `is_active`, `inserted_date`) VALUES
(1, 'Mixing', 'Human', 3, NULL, 1.00, '01:00:00', '00:00:00', '00:00:00', 1, '2012-06-05 17:04:07'),
(2, 'Carding', 'Machine', 2, 1, 0.00, '00:00:00', '00:00:00', '00:00:00', 1, '2012-06-25 23:14:10'),
(3, 'Packaging', 'Human', NULL, NULL, 0.00, '00:00:00', '00:00:00', '00:00:00', 1, '2012-06-28 03:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_details`
--

DROP TABLE IF EXISTS `purchase_order_details`;
CREATE TABLE IF NOT EXISTS `purchase_order_details` (
  `pk_id_purchase_order_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_purchase_order` bigint(20) NOT NULL,
  `pk_id_purchase_request_detail` bigint(20) DEFAULT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_purchase_order_detail`),
  KEY `pk_id_purchase_order` (`pk_id_purchase_order`),
  KEY `pk_id_purchase_request_detail` (`pk_id_purchase_request_detail`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `purchase_order_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

DROP TABLE IF EXISTS `purchase_orders`;
CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `pk_id_purchase_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_order_code` char(11) COLLATE latin1_general_ci NOT NULL,
  `purchase_order_date` datetime NOT NULL,
  `purchase_order_notes` text COLLATE latin1_general_ci,
  `receive_control` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `invoice_control` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `contract_number` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `contract_type` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `purchase_order_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `po_paid_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Not Finished',
  `po_received_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Not Finished',
  `po_shipping_schedule_date` datetime NOT NULL,
  `pk_id_stock_warehouse` int(11) NOT NULL,
  `pk_id_stock_location` int(11) NOT NULL,
  `pk_id_partner` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_purchase_order`),
  KEY `pk_id_stock_warehouse` (`pk_id_stock_warehouse`),
  KEY `pk_id_stock_location` (`pk_id_stock_location`),
  KEY `pk_id_partner` (`pk_id_partner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `purchase_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `purchase_request_details`
--

DROP TABLE IF EXISTS `purchase_request_details`;
CREATE TABLE IF NOT EXISTS `purchase_request_details` (
  `pk_id_purchase_request_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_purchase_request` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `purchase_request_detail_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `request_reason` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_purchase_request_detail`),
  KEY `pk_id_purchase_request` (`pk_id_purchase_request`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `purchase_request_details`
--

INSERT INTO `purchase_request_details` (`pk_id_purchase_request_detail`, `pk_id_purchase_request`, `pk_id_item`, `quantity`, `purchase_request_detail_state`, `pk_id_item_units_of_measure`, `request_reason`, `inserted_date`) VALUES
(3, 1, 3, 1500.000, 'Rejected', 3, 'Buat jadi gelang :))', '2012-06-15 21:18:13'),
(4, 2, 1, 5000.520, 'Done', 4, 'Buat main bakar2an :D', '2012-06-15 21:21:23'),
(7, 3, 3, 1000.000, 'Draft', 3, NULL, '2012-06-26 02:31:53'),
(8, 4, 1, 10.000, 'Draft', 4, NULL, '2012-06-26 02:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_requests`
--

DROP TABLE IF EXISTS `purchase_requests`;
CREATE TABLE IF NOT EXISTS `purchase_requests` (
  `pk_id_purchase_request` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_request_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `purchase_request_date` datetime NOT NULL,
  `purchase_request_notes` text COLLATE latin1_general_ci NOT NULL,
  `purchase_request_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_purchase_request`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `purchase_requests`
--

INSERT INTO `purchase_requests` (`pk_id_purchase_request`, `purchase_request_code`, `purchase_request_date`, `purchase_request_notes`, `purchase_request_state`, `inserted_date`) VALUES
(1, 'PRQ/12060001', '2012-06-15 21:13:00', 'helloow', 'Rejected', '2012-06-15 21:15:32'),
(2, 'PRQ/12060002', '2012-06-15 21:20:00', '', 'Done', '2012-06-15 21:21:23'),
(3, 'PRQ/12060003', '2012-06-26 02:30:00', 'halo', 'Draft', '2012-06-26 02:31:27'),
(4, 'PRQ/12060004', '2012-06-26 02:33:00', '', 'Draft', '2012-06-26 02:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_details`
--

DROP TABLE IF EXISTS `sales_order_details`;
CREATE TABLE IF NOT EXISTS `sales_order_details` (
  `pk_id_sales_order_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_sales_order` bigint(20) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_sales_order_detail`),
  KEY `pk_id_sales_order` (`pk_id_sales_order`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sales_order_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `sales_orders`
--

DROP TABLE IF EXISTS `sales_orders`;
CREATE TABLE IF NOT EXISTS `sales_orders` (
  `pk_id_sales_order` bigint(20) NOT NULL AUTO_INCREMENT,
  `sales_order_code` char(11) COLLATE latin1_general_ci NOT NULL,
  `sales_order_date` datetime NOT NULL,
  `sales_order_notes` text COLLATE latin1_general_ci,
  `sales_method` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `invoice_control` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `contract_number` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `contract_type` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `sales_order_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `so_paid_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Not Finished',
  `so_delivered_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Not Finished',
  `so_delivering_schedule_date` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Not Finished',
  `pk_id_stock_warehouse` int(11) NOT NULL,
  `pk_id_stock_location` int(11) NOT NULL,
  `pk_id_partner` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_sales_order`),
  UNIQUE KEY `sales_order_code` (`sales_order_code`),
  KEY `pk_id_stock_warehouse` (`pk_id_stock_warehouse`),
  KEY `pk_id_stock_location` (`pk_id_stock_location`),
  KEY `pk_id_partner` (`pk_id_partner`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sales_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `stock_balances`
--

DROP TABLE IF EXISTS `stock_balances`;
CREATE TABLE IF NOT EXISTS `stock_balances` (
  `pk_id_stock_balance` bigint(20) NOT NULL AUTO_INCREMENT,
  `pk_id_stock_opname` bigint(20) DEFAULT NULL,
  `balance_quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `posting_date` datetime NOT NULL,
  `pk_id_stock_location` int(11) NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `pk_id_production_lot` bigint(20) DEFAULT NULL,
  `pk_id_item_package` int(11) DEFAULT NULL,
  `pk_id_item_pack` bigint(20) DEFAULT NULL,
  `pk_id_partner` int(11) DEFAULT NULL,
  `stock_balance_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_balance`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_stock_location` (`pk_id_stock_location`),
  KEY `pk_id_production_lot` (`pk_id_production_lot`),
  KEY `pk_id_item_package` (`pk_id_item_package`),
  KEY `pk_id_item_pack` (`pk_id_item_pack`),
  KEY `pk_id_partner` (`pk_id_partner`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_stock_opname` (`pk_id_stock_opname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `stock_balances`
--

INSERT INTO `stock_balances` (`pk_id_stock_balance`, `pk_id_stock_opname`, `balance_quantity`, `pk_id_item_units_of_measure`, `posting_date`, `pk_id_stock_location`, `pk_id_item`, `pk_id_production_lot`, `pk_id_item_package`, `pk_id_item_pack`, `pk_id_partner`, `stock_balance_state`, `inserted_date`) VALUES
(29, 9, 1000.000, 3, '2012-06-15 17:12:00', 4, 3, NULL, NULL, NULL, NULL, 'Done', '2012-06-15 17:13:34'),
(30, 10, 10000000.000, 4, '2012-06-15 18:22:00', 4, 1, NULL, NULL, NULL, NULL, 'Waiting Approval', '2012-06-15 18:23:05'),
(35, 11, 121.400, 5, '2012-06-15 21:24:00', 3, 2, 2, 2, 3, NULL, 'Draft', '2012-06-15 21:25:38'),
(36, 11, 181.200, 5, '2012-06-15 21:24:00', 3, 2, 1, 2, 5, NULL, 'Draft', '2012-06-15 21:25:38'),
(37, 12, 611.300, 4, '2012-06-27 23:32:00', 18, 5, 1, 1, 1, NULL, 'Done', '2012-06-27 23:37:35'),
(38, 12, 567.221, 4, '2012-06-27 23:32:00', 18, 5, 1, 1, 2, NULL, 'Done', '2012-06-27 23:37:35'),
(39, 12, 604.500, 4, '2012-06-27 23:32:00', 17, 5, 2, 1, 3, NULL, 'Done', '2012-06-27 23:37:35'),
(40, 12, 572.000, 4, '2012-06-27 23:32:00', 17, 5, 1, 1, 4, NULL, 'Done', '2012-06-27 23:37:35'),
(41, 12, 581.000, 4, '2012-06-27 23:32:00', 17, 5, 2, 1, 5, NULL, 'Done', '2012-06-27 23:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `stock_locations`
--

DROP TABLE IF EXISTS `stock_locations`;
CREATE TABLE IF NOT EXISTS `stock_locations` (
  `pk_id_stock_location` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `location_type` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `parent_location_id` int(11) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_location`),
  KEY `parent_location_id` (`parent_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `stock_locations`
--

INSERT INTO `stock_locations` (`pk_id_stock_location`, `location_name`, `location_type`, `parent_location_id`, `inserted_date`) VALUES
(1, 'Physical Location', 'View', NULL, '2012-05-25 14:26:26'),
(2, 'Cotton Location', 'Internal Location', 1, '2012-05-25 14:27:04'),
(3, 'Thread Location', 'Internal Location', 1, '2012-05-25 14:27:25'),
(4, 'Spare Part & Item Location', 'Internal Location', 1, '2012-05-25 14:27:41'),
(5, 'Partner Location', 'View', NULL, '2012-05-25 14:27:53'),
(6, 'Supplier', 'Supplier Location', 5, '2012-05-25 14:28:07'),
(7, 'Makloon', 'Makloon Location', 5, '2012-05-25 14:29:05'),
(8, 'Buyer', 'Buyer Location', 5, '2012-05-25 15:12:35'),
(9, 'Virtual Location', 'View', NULL, '2012-05-25 15:12:45'),
(10, 'Production', 'Production', 9, '2012-05-25 15:13:20'),
(11, 'Packaging', 'Production', 9, '2012-05-25 15:15:04'),
(12, 'Inventory Loss', 'Inventory', 9, '2012-05-25 15:15:37'),
(13, 'Maintenance', 'Maintenance', 9, '2012-06-25 19:23:16'),
(14, 'Machine 001', 'Maintenance', 13, '2012-06-25 19:26:23'),
(15, 'Machine 002', 'Maintenance', 13, '2012-06-25 19:26:38'),
(16, 'Demo Location', 'Internal Location', NULL, '2012-06-27 23:19:57'),
(17, 'Demo Shelf 1', 'Internal Location', 16, '2012-06-27 23:20:47'),
(18, 'Demo Shelf 2', 'Internal Location', 16, '2012-06-27 23:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `stock_moves`
--

DROP TABLE IF EXISTS `stock_moves`;
CREATE TABLE IF NOT EXISTS `stock_moves` (
  `pk_id_stock_moves` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_move_date` datetime NOT NULL,
  `pk_id_item` int(11) NOT NULL,
  `quantity` decimal(30,3) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `pk_id_production_lot` bigint(20) DEFAULT NULL,
  `pk_id_item_package` int(11) DEFAULT NULL,
  `pk_id_item_pack` bigint(20) DEFAULT NULL,
  `source_location_id` int(11) NOT NULL,
  `destination_location_id` int(11) NOT NULL,
  `pk_id_partner` int(11) DEFAULT NULL,
  `pk_id_purchase_order_detail` bigint(20) DEFAULT NULL,
  `pk_id_stock_opname` bigint(20) DEFAULT NULL,
  `pk_id_item_receipt` bigint(20) DEFAULT NULL,
  `pk_id_internal_move` bigint(20) DEFAULT NULL,
  `pk_id_item_release` bigint(20) DEFAULT NULL,
  `pk_id_item_delivery` bigint(20) DEFAULT NULL,
  `pk_id_manufacture_order` bigint(20) DEFAULT NULL,
  `pk_id_manufacture_work_order` bigint(20) DEFAULT NULL,
  `pk_id_maintenance_machine` bigint(20) DEFAULT NULL,
  `pk_id_maintenance_repair_order` bigint(20) DEFAULT NULL,
  `stock_move_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_moves`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_production_lot` (`pk_id_production_lot`),
  KEY `pk_id_pack` (`pk_id_item_pack`),
  KEY `source_location_id` (`source_location_id`),
  KEY `pk_id_item_receipt` (`pk_id_item_receipt`),
  KEY `pk_id_internal_move` (`pk_id_internal_move`),
  KEY `pk_id_item_release` (`pk_id_item_release`),
  KEY `pk_id_item` (`pk_id_item`),
  KEY `pk_id_item_package` (`pk_id_item_package`),
  KEY `pk_id_partner` (`pk_id_partner`),
  KEY `pk_id_item_delivery` (`pk_id_item_delivery`),
  KEY `pk_id_stock_opname` (`pk_id_stock_opname`),
  KEY `pk_id_purchase_order_detail` (`pk_id_purchase_order_detail`),
  KEY `pk_id_manufacture_order` (`pk_id_manufacture_order`),
  KEY `pk_id_manufacture_work_order` (`pk_id_manufacture_work_order`),
  KEY `pk_id_maintenance_machine` (`pk_id_maintenance_machine`),
  KEY `pk_id_maintenance_repair_order` (`pk_id_maintenance_repair_order`),
  KEY `destination_location_id` (`destination_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `stock_moves`
--

INSERT INTO `stock_moves` (`pk_id_stock_moves`, `stock_move_date`, `pk_id_item`, `quantity`, `pk_id_item_units_of_measure`, `pk_id_production_lot`, `pk_id_item_package`, `pk_id_item_pack`, `source_location_id`, `destination_location_id`, `pk_id_partner`, `pk_id_purchase_order_detail`, `pk_id_stock_opname`, `pk_id_item_receipt`, `pk_id_internal_move`, `pk_id_item_release`, `pk_id_item_delivery`, `pk_id_manufacture_order`, `pk_id_manufacture_work_order`, `pk_id_maintenance_machine`, `pk_id_maintenance_repair_order`, `stock_move_state`, `inserted_date`) VALUES
(1, '2012-05-25 18:05:00', 1, 200.000, 4, NULL, NULL, NULL, 2, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-05-25 18:05:46'),
(6, '2012-05-28 21:51:00', 3, 100.000, 3, NULL, NULL, NULL, 12, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-05-28 21:51:18'),
(11, '2012-05-25 18:18:08', 1, 170.000, 4, NULL, 1, NULL, 6, 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-05-29 18:59:59'),
(12, '2012-05-25 18:18:08', 1, 181.000, 4, NULL, 1, NULL, 6, 2, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-05-29 18:59:59'),
(13, '2012-05-30 13:26:00', 3, 400.000, 3, NULL, NULL, NULL, 12, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-05-30 13:26:45'),
(20, '2012-06-01 18:45:00', 2, 8.000, 4, NULL, NULL, NULL, 3, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-06-01 18:45:16'),
(21, '2012-06-05 14:33:00', 1, 120.000, 4, NULL, 1, 5, 6, 2, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-06-05 14:35:10'),
(28, '2012-06-05 14:38:00', 3, 5000.000, 3, NULL, NULL, NULL, 6, 4, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-06-05 14:42:54'),
(29, '2012-06-07 00:10:00', 1, 1.000, 4, NULL, NULL, NULL, 2, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-07 00:10:02'),
(30, '2012-06-07 01:33:00', 3, 1900.000, 3, NULL, NULL, NULL, 4, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-07 01:33:22'),
(31, '2012-06-07 15:21:00', 1, 1.950, 4, NULL, NULL, NULL, 2, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-07 15:21:58'),
(32, '2012-06-07 15:22:00', 3, 1000.000, 3, NULL, NULL, NULL, 12, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-07 15:22:23'),
(33, '2012-06-07 20:08:00', 3, 200.000, 3, NULL, NULL, NULL, 12, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-07 20:08:53'),
(36, '2012-06-15 15:31:00', 1, 200.000, 4, NULL, NULL, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 'Done', '2012-06-15 15:40:53'),
(37, '2012-06-15 19:07:00', 3, 500.000, 3, NULL, NULL, NULL, 4, 4, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-06-15 19:08:00'),
(38, '2012-06-15 19:08:00', 3, 200.000, 3, NULL, NULL, NULL, 4, 4, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'Done', '2012-06-15 19:08:30'),
(41, '0000-00-00 00:00:00', 1, 100.000, 4, NULL, NULL, NULL, 2, 13, 3, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'Draft', '2012-06-26 02:03:15'),
(43, '2012-07-05 17:17:00', 5, 400.000, 4, NULL, NULL, NULL, 17, 8, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, 'Done', '2012-07-05 17:21:40');

-- --------------------------------------------------------

--
-- Table structure for table `stock_opnames`
--

DROP TABLE IF EXISTS `stock_opnames`;
CREATE TABLE IF NOT EXISTS `stock_opnames` (
  `pk_id_stock_opname` bigint(20) NOT NULL AUTO_INCREMENT,
  `stock_opname_code` char(12) COLLATE latin1_general_ci NOT NULL,
  `stock_opname_date` datetime NOT NULL,
  `stock_opname_notes` text COLLATE latin1_general_ci,
  `stock_opname_state` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT 'Draft',
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_opname`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `stock_opnames`
--

INSERT INTO `stock_opnames` (`pk_id_stock_opname`, `stock_opname_code`, `stock_opname_date`, `stock_opname_notes`, `stock_opname_state`, `inserted_date`) VALUES
(9, 'OPN/12060001', '2012-06-15 17:12:00', NULL, 'Done', '2012-06-15 17:13:33'),
(10, 'OPN/12060002', '2012-06-15 18:22:00', NULL, 'Waiting Approval', '2012-06-15 18:23:05'),
(11, 'OPN/12060003', '2012-06-15 21:24:00', NULL, 'Draft', '2012-06-15 21:25:30'),
(12, 'OPN/12060004', '2012-06-27 23:32:00', 'Demo', 'Done', '2012-06-27 23:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `stock_rules`
--

DROP TABLE IF EXISTS `stock_rules`;
CREATE TABLE IF NOT EXISTS `stock_rules` (
  `pk_id_stock_rule` int(11) NOT NULL AUTO_INCREMENT,
  `pk_id_item` int(11) NOT NULL,
  `pk_id_item_units_of_measure` int(11) NOT NULL,
  `pk_id_stock_warehouse` int(11) NOT NULL,
  `minimum_stock` decimal(30,3) NOT NULL,
  `maximum_stock` decimal(30,3) NOT NULL,
  `reorder_point` decimal(30,3) DEFAULT NULL,
  `economic_order_quantity` decimal(30,3) DEFAULT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_rule`),
  UNIQUE KEY `pk_id_item` (`pk_id_item`,`pk_id_stock_warehouse`),
  KEY `pk_id_item_units_of_measure` (`pk_id_item_units_of_measure`),
  KEY `pk_id_stock_warehouse` (`pk_id_stock_warehouse`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `stock_rules`
--

INSERT INTO `stock_rules` (`pk_id_stock_rule`, `pk_id_item`, `pk_id_item_units_of_measure`, `pk_id_stock_warehouse`, `minimum_stock`, `maximum_stock`, `reorder_point`, `economic_order_quantity`, `inserted_date`) VALUES
(2, 3, 3, 3, 0.000, 2500.000, NULL, NULL, '2012-06-28 23:02:21');

-- --------------------------------------------------------

--
-- Table structure for table `stock_warehouses`
--

DROP TABLE IF EXISTS `stock_warehouses`;
CREATE TABLE IF NOT EXISTS `stock_warehouses` (
  `pk_id_stock_warehouse` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `pk_id_stock_location` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_stock_warehouse`),
  KEY `pk_id_stock_location` (`pk_id_stock_location`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `stock_warehouses`
--

INSERT INTO `stock_warehouses` (`pk_id_stock_warehouse`, `warehouse_name`, `pk_id_stock_location`, `inserted_date`) VALUES
(1, 'Cotton Warehouse', 2, '2012-05-25 15:17:43'),
(2, 'Thread Warehouse', 3, '2012-05-25 15:17:53'),
(3, 'Spare Part & Item Warehouse', 4, '2012-05-25 15:18:12'),
(5, 'Demo Warehouse', 16, '2012-06-27 23:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `pk_id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`pk_id_user`, `username`, `password`, `inserted_date`) VALUES
(1, 'Admin', 'admin', '2012-06-07 20:17:04');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`pk_id_country`) REFERENCES `countries` (`pk_id_country`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`pk_id_city`) REFERENCES `cities` (`pk_id_city`);

--
-- Constraints for table `hr_departments`
--
ALTER TABLE `hr_departments`
  ADD CONSTRAINT `hr_departments_ibfk_1` FOREIGN KEY (`parent_hr_department_id`) REFERENCES `hr_departments` (`pk_id_hr_department`);

--
-- Constraints for table `hr_job_positions`
--
ALTER TABLE `hr_job_positions`
  ADD CONSTRAINT `hr_job_positions_ibfk_1` FOREIGN KEY (`pk_id_hr_department`) REFERENCES `hr_departments` (`pk_id_hr_department`);

--
-- Constraints for table `item_bill_of_materials`
--
ALTER TABLE `item_bill_of_materials`
  ADD CONSTRAINT `item_bill_of_materials_ibfk_1` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `item_bill_of_materials_ibfk_2` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`);

--
-- Constraints for table `item_bom_details`
--
ALTER TABLE `item_bom_details`
  ADD CONSTRAINT `item_bom_details_ibfk_1` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `item_bom_details_ibfk_2` FOREIGN KEY (`pk_id_item_bill_of_material`) REFERENCES `item_bill_of_materials` (`pk_id_item_bill_of_material`),
  ADD CONSTRAINT `item_bom_details_ibfk_3` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`);

--
-- Constraints for table `item_categories`
--
ALTER TABLE `item_categories`
  ADD CONSTRAINT `item_categories_ibfk_1` FOREIGN KEY (`parent_item_category_id`) REFERENCES `item_categories` (`pk_id_item_category`);

--
-- Constraints for table `item_deliveries`
--
ALTER TABLE `item_deliveries`
  ADD CONSTRAINT `item_deliveries_ibfk_1` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`);

--
-- Constraints for table `item_receipts`
--
ALTER TABLE `item_receipts`
  ADD CONSTRAINT `item_receipts_ibfk_1` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`),
  ADD CONSTRAINT `item_receipts_ibfk_2` FOREIGN KEY (`pk_id_purchase_order`) REFERENCES `purchase_orders` (`pk_id_purchase_order`);

--
-- Constraints for table `item_release_request_details`
--
ALTER TABLE `item_release_request_details`
  ADD CONSTRAINT `item_release_request_details_ibfk_2` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `item_release_request_details_ibfk_3` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `item_release_request_details_ibfk_4` FOREIGN KEY (`pk_id_item_release_request`) REFERENCES `item_release_requests` (`pk_id_item_release_request`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_releases`
--
ALTER TABLE `item_releases`
  ADD CONSTRAINT `item_releases_ibfk_4` FOREIGN KEY (`pk_id_maintenance_repair_order`) REFERENCES `maintenance_repair_orders` (`pk_id_maintenance_repair_order`),
  ADD CONSTRAINT `item_releases_ibfk_1` FOREIGN KEY (`pk_id_item_release_request`) REFERENCES `item_release_requests` (`pk_id_item_release_request`),
  ADD CONSTRAINT `item_releases_ibfk_2` FOREIGN KEY (`pk_id_manufacture_order`) REFERENCES `manufacture_orders` (`pk_id_manufacture_order`),
  ADD CONSTRAINT `item_releases_ibfk_3` FOREIGN KEY (`pk_id_maintenance_machine`) REFERENCES `maintenance_machines` (`pk_id_maintenance_machine`);

--
-- Constraints for table `item_suppliers`
--
ALTER TABLE `item_suppliers`
  ADD CONSTRAINT `item_suppliers_ibfk_3` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_suppliers_ibfk_4` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`);

--
-- Constraints for table `item_units_of_measures`
--
ALTER TABLE `item_units_of_measures`
  ADD CONSTRAINT `item_units_of_measures_ibfk_1` FOREIGN KEY (`pk_id_item_uom_category`) REFERENCES `item_uom_categories` (`pk_id_item_uom_category`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_10` FOREIGN KEY (`location_production_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `items_ibfk_11` FOREIGN KEY (`location_loss_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `items_ibfk_12` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `items_ibfk_13` FOREIGN KEY (`pk_id_item_category`) REFERENCES `item_categories` (`pk_id_item_category`);

--
-- Constraints for table `maintenance_machine_consumed_items`
--
ALTER TABLE `maintenance_machine_consumed_items`
  ADD CONSTRAINT `maintenance_machine_consumed_items_ibfk_1` FOREIGN KEY (`pk_id_stock_move`) REFERENCES `stock_moves` (`pk_id_stock_moves`);

--
-- Constraints for table `maintenance_machine_needed_items`
--
ALTER TABLE `maintenance_machine_needed_items`
  ADD CONSTRAINT `maintenance_machine_needed_items_ibfk_3` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `maintenance_machine_needed_items_ibfk_1` FOREIGN KEY (`pk_id_maintenance_machine`) REFERENCES `maintenance_machines` (`pk_id_maintenance_machine`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `maintenance_machine_needed_items_ibfk_2` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`);

--
-- Constraints for table `maintenance_machines`
--
ALTER TABLE `maintenance_machines`
  ADD CONSTRAINT `maintenance_machines_ibfk_1` FOREIGN KEY (`pk_id_production_machine`) REFERENCES `production_machines` (`pk_id_production_machine`);

--
-- Constraints for table `maintenance_repair_order_consumed_items`
--
ALTER TABLE `maintenance_repair_order_consumed_items`
  ADD CONSTRAINT `maintenance_repair_order_consumed_items_ibfk_1` FOREIGN KEY (`pk_id_stock_move`) REFERENCES `stock_moves` (`pk_id_stock_moves`);

--
-- Constraints for table `maintenance_repair_orders`
--
ALTER TABLE `maintenance_repair_orders`
  ADD CONSTRAINT `maintenance_repair_orders_ibfk_1` FOREIGN KEY (`pk_id_production_machine`) REFERENCES `production_machines` (`pk_id_production_machine`);

--
-- Constraints for table `manufacture_consumed_items`
--
ALTER TABLE `manufacture_consumed_items`
  ADD CONSTRAINT `manufacture_consumed_items_ibfk_2` FOREIGN KEY (`pk_id_stock_move`) REFERENCES `stock_moves` (`pk_id_stock_moves`),
  ADD CONSTRAINT `manufacture_consumed_items_ibfk_3` FOREIGN KEY (`pk_id_manufacture_order`) REFERENCES `manufacture_orders` (`pk_id_manufacture_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manufacture_incident_work_order_impacts`
--
ALTER TABLE `manufacture_incident_work_order_impacts`
  ADD CONSTRAINT `manufacture_incident_work_order_impacts_ibfk_2` FOREIGN KEY (`pk_id_manufacture_work_order`) REFERENCES `manufacture_work_orders` (`pk_id_manufacture_work_order`),
  ADD CONSTRAINT `manufacture_incident_work_order_impacts_ibfk_1` FOREIGN KEY (`pk_id_manufacture_incident_memo`) REFERENCES `manufacture_incident_memos` (`pk_id_manufacture_incident_memo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manufacture_machines`
--
ALTER TABLE `manufacture_machines`
  ADD CONSTRAINT `manufacture_machines_ibfk_2` FOREIGN KEY (`pk_id_production_machine`) REFERENCES `production_machines` (`pk_id_production_machine`),
  ADD CONSTRAINT `manufacture_machines_ibfk_3` FOREIGN KEY (`pk_id_manufacture_work_order`) REFERENCES `manufacture_work_orders` (`pk_id_manufacture_work_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manufacture_needed_items`
--
ALTER TABLE `manufacture_needed_items`
  ADD CONSTRAINT `manufacture_needed_items_ibfk_1` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `manufacture_needed_items_ibfk_2` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `manufacture_needed_items_ibfk_3` FOREIGN KEY (`pk_id_manufacture_order`) REFERENCES `manufacture_orders` (`pk_id_manufacture_order`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manufacture_orders`
--
ALTER TABLE `manufacture_orders`
  ADD CONSTRAINT `manufacture_orders_ibfk_1` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`),
  ADD CONSTRAINT `manufacture_orders_ibfk_3` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `manufacture_orders_ibfk_4` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `manufacture_orders_ibfk_5` FOREIGN KEY (`pk_id_item_bill_of_material`) REFERENCES `item_bill_of_materials` (`pk_id_item_bill_of_material`),
  ADD CONSTRAINT `manufacture_orders_ibfk_6` FOREIGN KEY (`pk_id_production_lot`) REFERENCES `production_lots` (`pk_id_production_lot`);

--
-- Constraints for table `manufacture_work_orders`
--
ALTER TABLE `manufacture_work_orders`
  ADD CONSTRAINT `manufacture_work_orders_ibfk_2` FOREIGN KEY (`pk_id_manufacture_order`) REFERENCES `manufacture_orders` (`pk_id_manufacture_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manufacture_work_orders_ibfk_3` FOREIGN KEY (`pk_id_production_work_center`) REFERENCES `production_work_centers` (`pk_id_production_work_center`);

--
-- Constraints for table `partner_categories`
--
ALTER TABLE `partner_categories`
  ADD CONSTRAINT `partner_categories_ibfk_1` FOREIGN KEY (`parent_partner_category_id`) REFERENCES `partner_categories` (`pk_id_partner_category`);

--
-- Constraints for table `partners`
--
ALTER TABLE `partners`
  ADD CONSTRAINT `partners_ibfk_2` FOREIGN KEY (`supplier_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `partners_ibfk_3` FOREIGN KEY (`buyer_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `partners_ibfk_4` FOREIGN KEY (`makloon_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `partners_ibfk_5` FOREIGN KEY (`pk_id_city`) REFERENCES `cities` (`pk_id_city`);

--
-- Constraints for table `production_machine_categories`
--
ALTER TABLE `production_machine_categories`
  ADD CONSTRAINT `production_machine_categories_ibfk_1` FOREIGN KEY (`pk_id_production_department`) REFERENCES `production_departments` (`pk_id_production_department`);

--
-- Constraints for table `production_machine_parameters`
--
ALTER TABLE `production_machine_parameters`
  ADD CONSTRAINT `production_machine_parameters_ibfk_1` FOREIGN KEY (`pk_id_machine_categories`) REFERENCES `production_machine_categories` (`pk_id_production_machine_category`);

--
-- Constraints for table `production_machine_spare_parts`
--
ALTER TABLE `production_machine_spare_parts`
  ADD CONSTRAINT `production_machine_spare_parts_ibfk_1` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `production_machine_spare_parts_ibfk_2` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `production_machine_spare_parts_ibfk_3` FOREIGN KEY (`pk_id_machine_category`) REFERENCES `production_machine_categories` (`pk_id_production_machine_category`);

--
-- Constraints for table `production_machines`
--
ALTER TABLE `production_machines`
  ADD CONSTRAINT `production_machines_ibfk_1` FOREIGN KEY (`pk_id_machine_category`) REFERENCES `production_machine_categories` (`pk_id_production_machine_category`),
  ADD CONSTRAINT `production_machines_ibfk_2` FOREIGN KEY (`pk_id_stock_location`) REFERENCES `stock_locations` (`pk_id_stock_location`);

--
-- Constraints for table `production_work_centers`
--
ALTER TABLE `production_work_centers`
  ADD CONSTRAINT `production_work_centers_ibfk_1` FOREIGN KEY (`pk_id_production_department`) REFERENCES `production_departments` (`pk_id_production_department`),
  ADD CONSTRAINT `production_work_centers_ibfk_2` FOREIGN KEY (`pk_id_production_machine_category`) REFERENCES `production_machine_categories` (`pk_id_production_machine_category`);

--
-- Constraints for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  ADD CONSTRAINT `purchase_order_details_ibfk_4` FOREIGN KEY (`pk_id_purchase_order`) REFERENCES `purchase_orders` (`pk_id_purchase_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_order_details_ibfk_5` FOREIGN KEY (`pk_id_purchase_request_detail`) REFERENCES `purchase_request_details` (`pk_id_purchase_request_detail`),
  ADD CONSTRAINT `purchase_order_details_ibfk_6` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `purchase_order_details_ibfk_7` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`);

--
-- Constraints for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD CONSTRAINT `purchase_orders_ibfk_1` FOREIGN KEY (`pk_id_stock_warehouse`) REFERENCES `stock_warehouses` (`pk_id_stock_warehouse`),
  ADD CONSTRAINT `purchase_orders_ibfk_2` FOREIGN KEY (`pk_id_stock_location`) REFERENCES `stock_warehouses` (`pk_id_stock_location`),
  ADD CONSTRAINT `purchase_orders_ibfk_3` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`);

--
-- Constraints for table `purchase_request_details`
--
ALTER TABLE `purchase_request_details`
  ADD CONSTRAINT `purchase_request_details_ibfk_2` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `purchase_request_details_ibfk_3` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `purchase_request_details_ibfk_4` FOREIGN KEY (`pk_id_purchase_request`) REFERENCES `purchase_requests` (`pk_id_purchase_request`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_order_details`
--
ALTER TABLE `sales_order_details`
  ADD CONSTRAINT `sales_order_details_ibfk_1` FOREIGN KEY (`pk_id_sales_order`) REFERENCES `sales_orders` (`pk_id_sales_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_order_details_ibfk_2` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `sales_order_details_ibfk_3` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`);

--
-- Constraints for table `sales_orders`
--
ALTER TABLE `sales_orders`
  ADD CONSTRAINT `sales_orders_ibfk_1` FOREIGN KEY (`pk_id_stock_warehouse`) REFERENCES `stock_warehouses` (`pk_id_stock_warehouse`),
  ADD CONSTRAINT `sales_orders_ibfk_2` FOREIGN KEY (`pk_id_stock_location`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `sales_orders_ibfk_3` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`);

--
-- Constraints for table `stock_balances`
--
ALTER TABLE `stock_balances`
  ADD CONSTRAINT `stock_balances_ibfk_1` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `stock_balances_ibfk_10` FOREIGN KEY (`pk_id_stock_opname`) REFERENCES `stock_opnames` (`pk_id_stock_opname`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_balances_ibfk_2` FOREIGN KEY (`pk_id_stock_location`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `stock_balances_ibfk_4` FOREIGN KEY (`pk_id_item_package`) REFERENCES `item_packages` (`pk_id_item_packages`),
  ADD CONSTRAINT `stock_balances_ibfk_6` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`),
  ADD CONSTRAINT `stock_balances_ibfk_7` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `stock_balances_ibfk_8` FOREIGN KEY (`pk_id_production_lot`) REFERENCES `production_lots` (`pk_id_production_lot`),
  ADD CONSTRAINT `stock_balances_ibfk_9` FOREIGN KEY (`pk_id_item_pack`) REFERENCES `item_packs` (`pk_id_item_pack`);

--
-- Constraints for table `stock_locations`
--
ALTER TABLE `stock_locations`
  ADD CONSTRAINT `stock_locations_ibfk_1` FOREIGN KEY (`parent_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`);

--
-- Constraints for table `stock_moves`
--
ALTER TABLE `stock_moves`
  ADD CONSTRAINT `stock_moves_ibfk_1` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`),
  ADD CONSTRAINT `stock_moves_ibfk_11` FOREIGN KEY (`pk_id_item_package`) REFERENCES `item_packages` (`pk_id_item_packages`),
  ADD CONSTRAINT `stock_moves_ibfk_13` FOREIGN KEY (`pk_id_partner`) REFERENCES `partners` (`pk_id_partner`),
  ADD CONSTRAINT `stock_moves_ibfk_16` FOREIGN KEY (`pk_id_production_lot`) REFERENCES `production_lots` (`pk_id_production_lot`),
  ADD CONSTRAINT `stock_moves_ibfk_17` FOREIGN KEY (`pk_id_item_pack`) REFERENCES `item_packs` (`pk_id_item_pack`),
  ADD CONSTRAINT `stock_moves_ibfk_18` FOREIGN KEY (`pk_id_item_receipt`) REFERENCES `item_receipts` (`pk_id_item_receipt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_19` FOREIGN KEY (`pk_id_internal_move`) REFERENCES `item_internal_moves` (`pk_id_item_internal_move`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_2` FOREIGN KEY (`source_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `stock_moves_ibfk_20` FOREIGN KEY (`pk_id_item_release`) REFERENCES `item_releases` (`pk_id_item_release`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_23` FOREIGN KEY (`pk_id_item_delivery`) REFERENCES `item_deliveries` (`pk_id_item_delivery`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_26` FOREIGN KEY (`destination_location_id`) REFERENCES `stock_locations` (`pk_id_stock_location`),
  ADD CONSTRAINT `stock_moves_ibfk_27` FOREIGN KEY (`pk_id_stock_opname`) REFERENCES `stock_opnames` (`pk_id_stock_opname`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_28` FOREIGN KEY (`pk_id_purchase_order_detail`) REFERENCES `purchase_order_details` (`pk_id_purchase_order_detail`),
  ADD CONSTRAINT `stock_moves_ibfk_29` FOREIGN KEY (`pk_id_manufacture_order`) REFERENCES `manufacture_orders` (`pk_id_manufacture_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_30` FOREIGN KEY (`pk_id_manufacture_work_order`) REFERENCES `manufacture_work_orders` (`pk_id_manufacture_work_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stock_moves_ibfk_9` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`);

--
-- Constraints for table `stock_rules`
--
ALTER TABLE `stock_rules`
  ADD CONSTRAINT `stock_rules_ibfk_1` FOREIGN KEY (`pk_id_item`) REFERENCES `items` (`pk_id_item`),
  ADD CONSTRAINT `stock_rules_ibfk_3` FOREIGN KEY (`pk_id_stock_warehouse`) REFERENCES `stock_warehouses` (`pk_id_stock_warehouse`),
  ADD CONSTRAINT `stock_rules_ibfk_4` FOREIGN KEY (`pk_id_item_units_of_measure`) REFERENCES `item_units_of_measures` (`pk_id_item_units_of_measure`);

--
-- Constraints for table `stock_warehouses`
--
ALTER TABLE `stock_warehouses`
  ADD CONSTRAINT `stock_warehouses_ibfk_1` FOREIGN KEY (`pk_id_stock_location`) REFERENCES `stock_locations` (`pk_id_stock_location`);
