function default_cellattr_left(rowId, tv, rawObject, cm, rdata) {
	return "style='text-align: left; padding-left: 7px; padding-top: 3px'";
}

function default_cellattr_right(rowId, tv, rawObject, cm, rdata) {
	return "style='text-align: right; padding-right: 14px; padding-top: 3px'";
}

function default_cellattr_center(rowId, tv, rawObject, cm, rdata) {
	return "style='text-align: center; padding: 0; padding-top: 3px'";
}

function serverError(jqXHR, textStatus, errorThrown) {
	var error_messages = "<h4 class='thin underline'>Error Occured on Server Side.</h4>";
	error_messages += "<div class='align-left'><p>Please check whether the data has been modified or not, and contact your Web Administrator.</p>";
	error_messages += "<p>Status Code: " + jqXHR.status + "</p>";
	error_messages += jqXHR.responseText + "</div>";

	$.modal.alert(error_messages);
	$(".modal .button").focus();
}

function formatNumber(nData, thousandsSeparator, decimalSeparator, decimalPlaces) {
	var thousandsSeparator = thousandsSeparator || ",";
	var decimalSeparator = decimalSeparator || ".";
	if(!$.fmatter.isNumber(nData)) {
		nData *= 1;
	}
	if($.fmatter.isNumber(nData)) {
		var bNegative = (nData < 0);
		var sOutput = nData + "";
		var sDecimalSeparator = (decimalSeparator) ? decimalSeparator : ".";
		var nDotIndex;
		if($.fmatter.isNumber(decimalPlaces)) {
			var nDecimalPlaces = decimalPlaces;
			var nDecimal = Math.pow(10, nDecimalPlaces);
			sOutput = Math.round(nData*nDecimal)/nDecimal + "";
			nDotIndex = sOutput.lastIndexOf(".");
			if(nDecimalPlaces > 0) {
				if(nDotIndex < 0) {
					sOutput += sDecimalSeparator;
					nDotIndex = sOutput.length-1;
				}
				
				else if(sDecimalSeparator !== "."){
					sOutput = sOutput.replace(".",sDecimalSeparator);
				}
				
				while((sOutput.length - 1 - nDotIndex) < nDecimalPlaces) {
					sOutput += "0";
				}
			}
		}
		if(thousandsSeparator) {
			var sThousandsSeparator = thousandsSeparator;
			nDotIndex = sOutput.lastIndexOf(sDecimalSeparator);
			nDotIndex = (nDotIndex > -1) ? nDotIndex : sOutput.length;
			var sNewOutput = sOutput.substring(nDotIndex);
			var nCount = -1;
			for (var i=nDotIndex; i>0; i--) {
				nCount++;
				if ((nCount%3 === 0) && (i !== nDotIndex) && (!bNegative || (i > 1))) {
					sNewOutput = sThousandsSeparator + sNewOutput;
				}
				sNewOutput = sOutput.charAt(i-1) + sNewOutput;
			}
			sOutput = sNewOutput;
		}
		
		return sOutput;
	} else {
		return nData;
	}
}

function unFormatNumber(nData, thousandsSeparator) {
	var thousandsSeparator = thousandsSeparator || ",";
	var sep = thousandsSeparator.replace(/([\.\*\_\'\(\)\{\}\+\?\\])/g, "\\$1");
	var stripTag = new RegExp(sep, "g");
	var ret = nData.replace(stripTag ,"");
	
	return ret;
}
					