<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Pdf Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category		Helpers
 * @author		Jack
 * @email		jacky@binus.edu
 */

// ------------------------------------------------------------------------

/**
 * Generate Unique
 *
 * @access	public
 * @param	integer
 * @return	string
 */	
if ( ! function_exists('create_pdf'))
{
	function create_pdf($html, $path, $filename, $stream=TRUE)
	{		
		require_once('add-ons/dompdf/dompdf_config.inc.php');
    	spl_autoload_register('DOMPDF_autoload');
    
    	$dompdf = new DOMPDF();
		//$dompdf->set_paper(array(0,0,612.00,1024.00), 'portrait');
    	$dompdf->load_html($html);
    	$dompdf->render();
		if($stream) 
		{
        	$dompdf->stream($filename.'.pdf');
    	} 
		else 
		{
        	$CI =& get_instance();
        	$CI->load->helper('file');
        	write_file($path.$filename.'.pdf', $dompdf->output());
    	}
	}	
}

/* End of file pdf_helper.php */
/* Location: ./application/helpers/pdf_helper.php */