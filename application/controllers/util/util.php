<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Util extends CI_Controller {
	public function date_time() {	
		$response_data = array(
			"day" => date("d"),
			"month" => date("M"),
			"year" => date("Y"),
			"hour" => date("H"),
			"minute" => date("i"),
			"second" => date("s"),
			"serverTime" => date("Y-m-d H:i")
		);
		
		$response_arr["response"] = $response_data;
		$this->load->view("json_print_page", $response_arr);	
	}
}
