<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Purchases";
		
		$page_content["page_title"] = "Purchases";
		$page_content["title"] = "Purchases";
		
		$menu_params["current_navigable"] = "Home";
		$page_content["menu"] = $this->load->view("purchases/purchases_menu", $menu_params, true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
