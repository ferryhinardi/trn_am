<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Master";
		
		$page_content["page_title"] = "Master";
		$page_content["title"] = "Master";
		$menu_params["current_navigable"] = "Home";
		$page_content["menu"] = $this->load->view("master/master_menu", $menu_params, true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
