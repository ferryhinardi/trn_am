<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Item";
		
		$main_page_params["list_view"] = true;
		$main_page_params["form_view"] = true;
		$main_page_params["calendar_view"] = false;
		
		$main_page_params["list_page"] = $this->load->view("master/item/list_view", "", true);
		
		$sub_form_adjustment_page_params["option_item_packages"] = "<option value=''></option>";
			$this->load->model("items_m", "item");
			$item_packages = $this->item->get_item_package_list()->result_array();
			for($i = 0; $i < count($item_packages); $i++) 
				$sub_form_adjustment_page_params["option_item_packages"] .= 
					"<option value='{$item_packages[$i]["pk_id_item_packages"]}'>{$item_packages[$i]["package_name"]}</option>";
		$form_page_params["sub_form_adjustment"] = $this->load->view("master/item/sub_form_adjustment", $sub_form_adjustment_page_params, true);
		
		$form_page_params["search_item_category"] = $this->load->view("master/item/search_item_category", "", true);
		$form_page_params["search_item_uom"] = $this->load->view("master/item/search_item_uom", "", true);
		$form_page_params["search_stock_location"] = $this->load->view("master/item/search_stock_location", "", true);
		$form_page_params["search_supplier"] = $this->load->view("master/item/search_supplier", "", true);
		$form_page_params["search_item_pack"] = $this->load->view("master/item/search_item_pack", "", true);
		$form_page_params["search_production_lot"] = $this->load->view("master/item/search_production_lot", "", true);
		$main_page_params["form_page"] = $this->load->view("master/item/form_view", $form_page_params, true);
		
		$page_content["content"] = $this->load->view("template/manipulation_template", $main_page_params, true);
		
		$page_content["page_title"] = "Item";
		$page_content["title"] = "Master";
		$menu_params["current_navigable"] = "Item";
		$page_content["menu"] = $this->load->view("master/master_menu", $menu_params, true);
		
		$this->load->view("template/main_template", $page_content);
	}
	
	public function validate($item_name, $pk_id_item_category, $pk_id_item_units_of_measure, $life_time, $sale_price, $cost_price, $op) {
		$error_count = 0; 
		$error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
		$field_occured = array();
		
		if($op == "add" || $op == "edit") {
			if($item_name == "") {
				$error_messages .= "<li>Item Name Must be Filled !</li>";
				array_push($field_occured, "item-name");
				$error_count++;
			} else if(strlen($item_name) > 50) {
				$error_messages .= "<li>Length of Item Name Must not be Greater than 50 !</li>";
				array_push($field_occured, "item-name");
				$error_count++;
			} 
			if($pk_id_item_category == "") {
				$error_messages .= "<li>Item Category Must be Choosed !</li>";
				array_push($field_occured, "item-category-name");
				$error_count++;
			}
			if($pk_id_item_units_of_measure == "") {
				$error_messages .= "<li>Default UOM Must be Choosed !</li>";
				array_push($field_occured, "uom-name");
				$error_count++;
			} 
			if($life_time != "" && $life_time == 0) {	
				$error_messages .= "<li>Life Time Must not be Equal to 0 !</li>";
				array_push($field_occured, "life-time");
				$error_count++;
			} else if($life_time != "" && !is_numeric($life_time)) {
				$error_messages .= "<li>Life Time Must be Numeric Value !</li>";
				array_push($field_occured, "life-time");
				$error_count++;
			} 
			if($sale_price == "") {	
				$error_messages .= "<li>Sale Price Must be Filled !</li>";
				array_push($field_occured, "sale-price");
				$error_count++;
			} else if(!is_numeric($sale_price)) {
				$error_messages .= "<li>Sale Price Must be Numeric Value !</li>";
				array_push($field_occured, "sale-price");
				$error_count++;
			} 
			if($cost_price == "") {	
				$error_messages .= "<li>Cost Price Must be Filled !</li>";
				array_push($field_occured, "cost-price");
				$error_count++;
			} else if(!is_numeric($cost_price)) {
				$error_messages .= "<li>Cost Price Must be Numeric Value !</li>";
				array_push($field_occured, "cost-price");
				$error_count++;
			} 
		}
		$error_messages .= "</ul>";
		
		return array (
			"is_valid" => ($error_count == 0),
			"error_messages" => $error_messages,
			"field_occured" => $field_occured
		);
	}
	
	public function recalculate_stock() {
		$pk_id_item = $this->input->post("id");
		
		$this->load->model("items_m", "item");
		$result = $this->item->get_curr_stock_onhand($pk_id_item)->result_array();
		$response_data["curr_stock_onhand"] = $result[0]["curr_stock_onhand"];
		
		$response_arr["response"] = $response_data;
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function manipulation() {
		$op = $this->input->post("oper");
		$pk_id_item = $this->input->post("id");
		$item_name = $this->input->post("item_name");
		$pk_id_item_category = $this->input->post("pk_id_item_category");
		$is_can_be_sold = $this->input->post("is_can_be_sold");
		$is_can_be_purchased = $this->input->post("is_can_be_purchased");
		$is_can_be_produced = $this->input->post("is_can_be_produced");
		$life_time = $this->input->post("life_time");
		$pk_id_item_units_of_measure = $this->input->post("pk_id_item_units_of_measure");
		$location_production_id = $this->input->post("location_production_id");
		$location_loss_id = $this->input->post("location_loss_id");
		$sale_price = $this->input->post("sale_price");
		$cost_price = $this->input->post("cost_price");
		$is_active = $this->input->post("is_active");
		$pk_id_partners = explode("#$#", $this->input->post("pk_id_partners"));
		$supplier_item_names = explode("#$#", $this->input->post("supplier_item_names"));
		
		$response_data = array(
			"new_id" => null,
			"status" => null,
			"error_messages" => null,
			"field_occured" => null
		);
		
		if($op) {
			$result_validation = $this->validate($item_name, $pk_id_item_category, $pk_id_item_units_of_measure, $life_time, $sale_price, $cost_price, $op);
			if(!$result_validation["is_valid"]) {
				$response_data["status"] = "failed";
				$response_data["error_messages"] = $result_validation["error_messages"];
				$response_data["field_occured"] = $result_validation["field_occured"];
			} else {
				$this->load->model("items_m", "item");
				
				switch($op) {
					case "add": 
						$new_id = $this->item->insert(
							$item_name, $pk_id_item_category, $is_can_be_sold, $is_can_be_purchased, $is_can_be_produced, 
							$life_time, $pk_id_item_units_of_measure, $location_production_id, $location_loss_id, 
							$sale_price, $cost_price, $is_active, $pk_id_partners, $supplier_item_names
						);
						$response_data["new_id"] = $new_id;
						break;
					case "edit": 
						$this->item->update(
							$pk_id_item, $item_name, $pk_id_item_category, $is_can_be_sold, $is_can_be_purchased, $is_can_be_produced, 
							$life_time, $pk_id_item_units_of_measure, $location_production_id, $location_loss_id, 
							$sale_price, $cost_price, $is_active, $pk_id_partners, $supplier_item_names
						); 
						$response_data["new_id"] = $pk_id_item;
						break;
					case "del": $this->item->delete($pk_id_item); break;
				}
				
				$response_data["status"] = "success";
			}
			
			$response_arr["response"] = $response_data;
			$this->load->view("json_print_page", $response_arr);
		}
	}
	
	public function validate_adjust($qty_adjust, $id_location_adjust) {
		$error_count = 0; 
		$error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
		$field_occured = array();
		
		if($qty_adjust == "") {
			$error_messages .= "<li>New Stock Must be Filled !</li>";
			array_push($field_occured, "qty-adjust");
			$error_count++;
		} else if(!is_numeric($qty_adjust)) {
			$error_messages .= "<li>New Stock Must be Numeric Value !</li>";
			array_push($field_occured, "qty-adjust");
			$error_count++;
		} 
		if($id_location_adjust == "") {
			$error_messages .= "<li>Location Must be Filled !</li>";
			array_push($field_occured, "location-adjust");
			$error_count++;
		}
		
		$error_messages .= "</ul>";
		
		return array (
			"is_valid" => ($error_count == 0),
			"error_messages" => $error_messages,
			"field_occured" => $field_occured
		);
	}
	
	public function adjust_stock() {
		$pk_id_item = $this->input->post("id");
		$stock_on_hand = $this->input->post("stock_on_hand");
		$qty_adjust = $this->input->post("qty_adjust");
		$id_location_adjust = $this->input->post("id_location_adjust");
		$id_packaging_adjust = $this->input->post("id_packaging_adjust");
		$id_pack_adjust = $this->input->post("id_pack_adjust");
		$id_lot_adjust = $this->input->post("id_lot_adjust");
		$location_loss_id = $this->input->post("location_loss_id");
		$pk_id_item_units_of_measure = $this->input->post("pk_id_item_units_of_measure");
		$stock_move_date = $this->input->post("date");
		
		$response_data = array(
			"new_id" => null,
			"status" => null,
			"error_messages" => null,
			"field_occured" => null,
			"new_stock" => null
		);
		
		$result_validation = $this->validate_adjust($qty_adjust, $id_location_adjust);
		if(!$result_validation["is_valid"]) {
			$response_data["status"] = "failed";
			$response_data["error_messages"] = $result_validation["error_messages"];
			$response_data["field_occured"] = $result_validation["field_occured"];
		} else {
			$this->load->model("items_m", "item");
			
			$response_data["new_stock"] = $this->item->stock_adjust(
				$pk_id_item, $stock_on_hand, $qty_adjust, $id_location_adjust, $id_packaging_adjust, $id_pack_adjust, 
				$id_lot_adjust, $location_loss_id, $pk_id_item_units_of_measure, $stock_move_date, $qty_adjust < $stock_on_hand
			);
			
			$response_data["status"] = "success";
		}
		
		$response_arr["response"] = $response_data;
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function item_list() {
		$this->load->model("items_m", "item");
		$this->load->model("item_categories_m", "ic");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->item->get_item_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $items = $this->item->get_item_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($items); $i++) {
			$parent_child_category_name = $this->hierarchical_name($items[$i]["item_category_name"], $items[$i]["parent_item_category_id"]);
			
			$response_data->rows[$i]['id'] = $items[$i]["pk_id_item"];
			$response_data->rows[$i]['cell'] = array(
				"<button type='button' title='Edit' class='link-edit tiny icon-pencil' onclick='gridEditClicked({$items[$i]["pk_id_item"]})'></button>",
				$items[$i]["item_name"],
				$parent_child_category_name,
				$items[$i]["sale_price"],
				$items[$i]["cost_price"],
				$items[$i]["stock_on_hand_posting"],
				$items[$i]["uom_name"],
				$items[$i]["balance_quantity"],
				$items[$i]["posting_date"],
				$items[$i]["stock_on_sale_posting"],
				$items[$i]["stock_on_purchase_posting"],
				$items[$i]["stock_on_produce_posting"],
				$items[$i]["pk_id_item_category"],
				$items[$i]["is_can_be_sold"],
				$items[$i]["is_can_be_purchased"],
				$items[$i]["is_can_be_produced"],
				$items[$i]["life_time"],
				$items[$i]["pk_id_item_units_of_measure"],
				$items[$i]["location_production_id"],
				$items[$i]["location_production"],
				$items[$i]["location_loss_id"],
				$items[$i]["location_loss"],
				$items[$i]["item_category_name"],
				$items[$i]["is_active"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function hierarchical_name($item_category_name, $parent_item_category_id) {
		if($parent_item_category_id != null) {
			$result = $this->ic->get_parents($parent_item_category_id)->result_array();
			
			$name = $result[0]["item_category_name"] . " / " . $item_category_name;
			
			return $this->hierarchical_name($name, $result[0]["parent_item_category_id"]);
		} 
		
		return $item_category_name;
	}
	
	public function supplier_search_list() {
		$this->load->model("items_m", "item");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"id_not_in_filter" => $this->input->post("id_not_in"),
			"limit" => null
		);     
		
        $row = $this->item->get_supplier_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $suppliers = $this->item->get_supplier_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($suppliers); $i++) {
			$response_data->rows[$i]['id'] = $suppliers[$i]["pk_id_partner"];
			$response_data->rows[$i]['cell'] = array(
				$suppliers[$i]["partner_name"],
				$suppliers[$i]["contact_name"],
				$suppliers[$i]["email"],
				$suppliers[$i]["contact_no"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function item_supplier_list() {
		$this->load->model("items_m", "item");
		$pk_id_item = $this->input->post("pk_id_item");
		
        $item_suppliers = $this->item->get_item_supplier_list($pk_id_item == null ? 0 : $pk_id_item)->result_array();
		
		$response_data->rows = "";
		
		for($i = 0; $i < count($item_suppliers); $i++) {
			$counter = $i + 1;
			$response_data->rows[$i]['id'] = $counter;
			$response_data->rows[$i]['cell'] = array(
				"<button type='button' title='Edit' class='item-supplier-edit link-edit tiny icon-pencil' onclick='gridItemSupplierEditClicked({$counter})'></button>",
				$item_suppliers[$i]["id_partner"],
				$item_suppliers[$i]["partner_name"],
				$item_suppliers[$i]["supplier_item_name"],
				$item_suppliers[$i]["contact_name"],
				$item_suppliers[$i]["email"],
				$item_suppliers[$i]["contact_no"],
				"<button type='button' title='Delete' class='item-supplier-delete link-edit tiny icon-cross' onclick='gridItemSupplierDelClicked({$counter})'></button>"
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function pack_list() {
		$this->load->model("items_m", "item");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->item->get_pack_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $packs = $this->item->get_pack_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($packs); $i++) {
			$response_data->rows[$i]['id'] = $packs[$i]["pk_id_item_pack"];
			$response_data->rows[$i]['cell'] = array(
				$packs[$i]["pack_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function production_lot_list() {
		$this->load->model("items_m", "item");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->item->get_production_lot_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $lots = $this->item->get_production_lot_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($lots); $i++) {
			$response_data->rows[$i]['id'] = $lots[$i]["pk_id_production_lot"];
			$response_data->rows[$i]['cell'] = array(
				$lots[$i]["production_lot_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function makloon_stock_list() {
		$this->load->model("items_m", "item");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
		$pk_id_item = $this->input->post("pk_id_item");
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null,
			"pk_id_item" => $pk_id_item == null ? 0 : $pk_id_item
		);     
		
        $row = $this->item->get_makloon_stock_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $packs = $this->item->get_makloon_stock_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($packs); $i++) {
			$response_data->rows[$i]['id'] = $packs[$i]["pk_id_partner"];
			$response_data->rows[$i]['cell'] = array(
				$packs[$i]["partner_name"],
				$packs[$i]["stock_on_hand_posting"],
				$packs[$i]["stock_on_produce_posting"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
}
