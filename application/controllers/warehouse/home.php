<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Warehouse";
		
		$page_content["page_title"] = "Warehouse";
		$page_content["title"] = "Warehouse";
		
		$menu_params["current_navigable"] = "Home";
		$page_content["menu"] = $this->load->view("warehouse/warehouse_menu", $menu_params, true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
