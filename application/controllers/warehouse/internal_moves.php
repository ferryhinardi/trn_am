<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Internal_moves extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Internal Moves";
		
		$main_page_params["list_view"] = true;
		$main_page_params["form_view"] = true;
		$main_page_params["calendar_view"] = false;
		
		$main_page_params["list_page"] = $this->load->view("warehouse/internal_moves/list_view", "", true);
		
		$this->load->model("internal_moves_m","im");
		
		$form_page_params["package"] = "first;:";
		
		foreach($this->im->get_package_search_list()->result_array() as $rs)
		{
			$form_page_params["package"].= ";".$rs['pk_id_item_packages'] .":". $rs['package_name'];
		}
		
		$form_page_params["package"] = str_replace("first;","",$form_page_params["package"]);
		
		$form_page_params["search_item_name"] = $this->load->view("warehouse/internal_moves/search_item_name", "", true);
		$form_page_params["search_uom_name"] = $this->load->view("warehouse/internal_moves/search_uom_name", "", true);
		$form_page_params["search_production_lot_name"] = $this->load->view("warehouse/internal_moves/search_production_lot_name", "", true);
		$form_page_params["search_pack_name"] = $this->load->view("warehouse/internal_moves/search_pack_name", "", true);
		$form_page_params["search_source_location_name"] = $this->load->view("warehouse/internal_moves/search_source_location_name", "", true);
		$form_page_params["search_destination_location_name"] = $this->load->view("warehouse/internal_moves/search_destination_location_name", "", true);
		$form_page_params["search_partner_name"] = $this->load->view("warehouse/internal_moves/search_partner_name", "", true);
		
		$main_page_params["form_page"] = $this->load->view("warehouse/internal_moves/form_view",$form_page_params, true);
		
		$page_content["content"] = $this->load->view("template/manipulation_template", $main_page_params, true);
		
		$page_content["page_title"] = "Internal Moves";
		$page_content["title"] = "Warehouse";
		
		$menu_params["current_navigable"] = "Internal Moves";
		$page_content["menu"] = $this->load->view("warehouse/warehouse_menu", $menu_params, true);
		
		$this->load->view("template/main_template", $page_content);
	}
	
	public function validate($item_internal_move_code, $item_internal_move_date, $op) {
		$error_count = 0; 
		$error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
		$field_occured = array();
		
		if($op == "add" || $op == "edit") {
			if($item_internal_move_code == "") {
				$error_messages .= "<li>Internal Move Code Must be Filled !</li>";
				array_push($field_occured, "item-internal-move-code");
				$error_count++;
			} 
			if($item_internal_move_date == "") {
				$error_messages .= "<li>Internal Move Date Must be Filled !</li>";
				array_push($field_occured, "item-internal-move-date");
				$error_count++;
			}
		}
		$error_messages .= "</ul>";
		
		return array (
			"is_valid" => ($error_count == 0),
			"error_messages" => $error_messages,
			"field_occured" => $field_occured
		);
	}
	
	public function set_state() {
		$pk_id_item_internal_move = $this->input->post("id");
		$internal_move_state = $this->input->post("state");
		
		$this->load->model("internal_moves_m", "im");
		$this->im->update_state($pk_id_item_internal_move, $internal_move_state);
	}
	
	function report($type, $id_internal_move, $internal_move_code, $states, $date_from, $date_until)
	{
		$this->load->helper('pdf');
		$this->load->model('internal_moves_m', 'im');
		
		$configurations = $this->im->get_configuration();
		
		if($type == '1')
		{
			$internal_moves = $this->im->get_internal_moves_list_report($id_internal_move,"","","");
			$stock_moves = $this->im->get_stock_moves_list_report($id_internal_move);
			
			$list_stock_moves_report['internal_moves'] = $internal_moves;
			$list_stock_moves_report['stock_moves'] = $stock_moves;
			$list_stock_moves_report['configurations'] = $configurations;
			
			$report_stock_moves = $this->load->view('warehouse/internal_moves/report/report_stock_moves', $list_stock_moves_report, true);
			create_pdf($report_stock_moves, NULL, $internal_move_code.'-'.date("dmY"));
			
		}
		else if($type == '2')
		{

			$internal_moves = $this->im->get_internal_moves_list_report("",$states, $date_from, $date_until);
			$stock_moves = $this->im->get_stock_moves_list_report("");
			
			$list_stock_moves_report['internal_moves'] = $internal_moves;
			$list_stock_moves_report['stock_moves'] = $stock_moves;
			$list_stock_moves_report['configurations'] = $configurations;
			$list_stock_moves_report['date_from'] = $date_from;
			$list_stock_moves_report['date_until'] = $date_until;
			
			$report_internal_moves = $this->load->view('warehouse/internal_moves/report/report_internal_moves', $list_stock_moves_report, true);
			create_pdf($report_internal_moves, NULL, 'REPORT INTERNAL MOVES-'.date("dmY") );
		}
	}
	
	public function manipulation() {
		$op = $this->input->post("oper");
		$pk_id_item_internal_move = $this->input->post("id");
		$item_internal_move_code = $this->input->post("item_internal_move_code");
		$item_internal_move_date = $this->input->post("item_internal_move_date");
		$item_internal_move_notes = $this->input->post("item_internal_move_notes");
		$pk_id_items = explode("#$", $this->input->post("pk_id_items"));
		$quantitys = explode("#$", $this->input->post("quantitys"));
		$pk_id_item_units_of_measures = explode("#$", $this->input->post("pk_id_item_units_of_measures"));
		$pk_id_production_lots = explode("#$", $this->input->post("pk_id_production_lots"));
		$pk_id_item_packages = explode("#$", $this->input->post("pk_id_item_packages"));
		$pk_id_item_packs = explode("#$", $this->input->post("pk_id_item_packs"));
		$source_location_ids = explode("#$", $this->input->post("source_location_ids"));
		$destination_location_ids = explode("#$", $this->input->post("destination_location_ids"));
		$pk_id_partners = explode("#$", $this->input->post("pk_id_partners"));
		
		$response_data = array(
			"new_id" => null,
			"status" => null,
			"error_messages" => null,
			"field_occured" => null
		);
		
		if($op) {
			$result_validation = $this->validate($item_internal_move_code, $item_internal_move_date, $op);
			
			if(!$result_validation["is_valid"]) {
				$response_data["status"] = "failed";
				$response_data["error_messages"] = $result_validation["error_messages"];
				$response_data["field_occured"] = $result_validation["field_occured"];
			} else {
			
				$this->load->model("internal_moves_m", "im");
				
				switch($op) {
					case "add": 
					
						$new_id = $this->im->insert($item_internal_move_code, $item_internal_move_date, 
						$item_internal_move_notes, $pk_id_items, $quantitys, $pk_id_item_units_of_measures, 
						$pk_id_production_lots,$pk_id_item_packages, $pk_id_item_packs, 
						$source_location_ids, $destination_location_ids, $pk_id_partners
						);
						
						$response_data["new_id"] = $new_id;
						break;
					case "edit": 
						$this->im->update($pk_id_item_internal_move, $item_internal_move_code, $item_internal_move_date, 
						$item_internal_move_notes, $pk_id_items, $quantitys, $pk_id_item_units_of_measures, 
						$pk_id_production_lots,$pk_id_item_packages, $pk_id_item_packs, 
						$source_location_ids, $destination_location_ids, $pk_id_partners
						);
						
						$response_data["new_id"] = $pk_id_item_internal_move;
						break;
						
					case "del": $this->im->delete($pk_id_item_internal_move); break;
				}
				
				}
				
				$response_data["status"] = "success";
			}
			
			$response_arr["response"] = $response_data;
			$this->load->view("json_print_page", $response_arr);
		
	}
	
		
	public function internal_moves_list() {
	
		$this->load->model("internal_moves_m", "im");
		
		$type = $this->input->post("type");
		$state = $this->input->post("state");
		$date_from = $this->input->post("date_from");
		$date_until = $this->input->post("date_until");
			
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null,
			"state" => $state,
			"date_from" => $date_from,
			"date_until" => $date_until,
			
		);     
		
        if($type == "") $row = $this->im->get_internal_moves_list($req_param)->result_array();
		else if($type == "search") $row = $this->im->get_internal_moves_list_search($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        if($type == "") $internal_moves_list = $this->im->get_internal_moves_list($req_param)->result_array();
		else if($type == "search") $internal_moves_list = $this->im->get_internal_moves_list_search($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($internal_moves_list); $i++) {
			$response_data->rows[$i]['id'] = $internal_moves_list[$i]["id_internal_move"];
			$response_data->rows[$i]['cell'] = array(
				($internal_moves_list[$i]["internal_move_state"] == "Draft") ? 
				"<button type='button' href='javascript:void(0);' title='Edit' class='link-edit tiny icon-pencil with-tooltip' onclick='gridEditClicked({$internal_moves_list[$i]["id_internal_move"]})'></button>": "",
				$internal_moves_list[$i]["id_internal_move"],
				$internal_moves_list[$i]["item_internal_move_code"],
				$internal_moves_list[$i]["item_internal_move_date"],
				$internal_moves_list[$i]["internal_move_state"],
				$internal_moves_list[$i]["item_internal_move_notes"],
				($internal_moves_list[$i]["internal_move_state"] == "Draft" ? 
				"<button type='button' href='javascript:void(0);' title='Done' class='link-edit tiny icon-flag with-tooltip' onclick='stateClicked({$internal_moves_list[$i]["id_internal_move"]}, \"Done\")'></button>":""),
				"<button type='button' href='javascript:void(0);' title='Print' class='link-edit tiny icon-printer with-tooltip' onclick='gridPrintClicked({$internal_moves_list[$i]["id_internal_move"]},\"1\")'></button>"
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function stock_moves_list() {
		$this->load->model("internal_moves_m", "im");
		$id_internal_move = $this->input->post("id_internal_move");
		
        $stock_moves_list = $this->im->get_stock_moves_list($id_internal_move == null ? 0 : $id_internal_move)->result_array();
		
		$response_data->rows = ""; 
		
		for($i = 0; $i < count($stock_moves_list); $i++) {
			$counter = $i + 1;
			$response_data->rows[$i]['id'] = $counter;
			$response_data->rows[$i]['cell'] = array(
				"<button type='button' title='Edit' class='stock-moves-edit link-edit tiny icon-pencil' onclick='gridStockMovesEditClicked({$counter})'></button>",
				$stock_moves_list[$i]["pk_id_item"],
				$stock_moves_list[$i]["item_name"],
				$stock_moves_list[$i]["quantity"],
				$stock_moves_list[$i]["pk_id_item_units_of_measure"],
				$stock_moves_list[$i]["pk_id_item_uom_category"],
				$stock_moves_list[$i]["uom_name"],
				$stock_moves_list[$i]["pk_id_production_lot"],
				$stock_moves_list[$i]["production_lot_name"],
				$stock_moves_list[$i]["pk_id_item_package"],
				$stock_moves_list[$i]["pk_id_item_pack"],
				$stock_moves_list[$i]["pack_name"],
				$stock_moves_list[$i]["source_location_id"],
				$stock_moves_list[$i]["source_location_name"],
				$stock_moves_list[$i]["destination_location_id"],
				$stock_moves_list[$i]["destination_location_name"],
				$stock_moves_list[$i]["pk_id_partner"],
				$stock_moves_list[$i]["partner_name"],
				"<button type='button' title='Delete' class='stock-moves-delete link-edit tiny icon-cross' onclick='gridStockMovesDelClicked({$counter})'></button>"
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function item_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_item_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_item_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["pk_id_item"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["item_name"],
				$item_list[$i]["uom_name"],
				$item_list[$i]["pk_id_item_units_of_measure"],
				$item_list[$i]["pk_id_item_uom_category"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function uom_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$pk_id_item_uom_category = $this->input->post("pk_id_item_uom_category") != null ? $this->input->post("pk_id_item_uom_category") : 0;
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null,
			"pk_id_item_uom_category" => $pk_id_item_uom_category
		);     
		
        $row = $this->im->get_uom_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_uom_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["pk_id_item_units_of_measure"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["pk_id_item_uom_category"],
				$item_list[$i]["uom_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function production_lot_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_production_lot_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_production_lot_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["pk_id_production_lot"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["production_lot_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function pack_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_pack_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_pack_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["pk_id_item_pack"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["pack_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	
	public function source_location_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_source_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_source_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["id_source_location"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["source_location_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	
	public function destination_location_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 
				
        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_destination_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_destination_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["id_destination_location"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["destination_location_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	
	public function makloon_name_search_list() {
		$this->load->model("internal_moves_m", "im");
		
		$page = $this->input->post("page");
		$limit = $this->input->post("rows");
        $sidx = $this->input->post("sidx"); 
        $sord = $this->input->post("sord"); 
		$is_search = $this->input->post("_search");
        $filter = $is_search ? $this->input->post("filters") : ""; 

        $req_param = array (
			"sort_by" => $sidx,
			"sort_direction" => $sord,
			"search_on" => $is_search,
			"json_filter" => $filter,
			"limit" => null
		);     
		
        $row = $this->im->get_makloon_search_list($req_param)->result_array();
		
        $count = count($row); 
        if( $count >0 ) $total_pages = ceil($count/$limit); 
        else $total_pages = 0; 
        
        if ($page > $total_pages) $page = $total_pages; 
        $start = $limit * $page - $limit; 
        
        $req_param['limit'] = array(
			'start' => $start,
			'end' => $limit
        );
          
        $item_list = $this->im->get_makloon_search_list($req_param)->result_array();
		
		$response_data->page = $page; 
		$response_data->total = $total_pages; 
		$response_data->records = $count;
		
		for($i = 0; $i < count($item_list); $i++) {
			$response_data->rows[$i]['id'] = $item_list[$i]["id_makloon_partner"];
			$response_data->rows[$i]['cell'] = array(
				$item_list[$i]["partner_makloon_name"]
			);
		}
		
		$response_arr["response"] = $response_data;
		
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function generateCode(){
		
		$this->load->helper("date");
		$this->load->model("internal_moves_m", "im");
		

		$datestring = "%y%m";
		$yearstring = "%y";
		$monthstring = "%m";
		
		$time = time();

		$prefix = 'INT/'.mdate($datestring, $time);
		$curr_year = mdate($yearstring, $time);
		$curr_month = mdate($monthstring, $time);
		
        $row = $this->im->get_row()->result_array();
		
        $count = count($row); 
        if( $count == 0 ) $item_internal_move_code = $prefix.'0001';
		else {
		
			$years = $this->im->getMaxYear()->result();
			foreach ($years as $value) {
				$year = $value->max_year;
			}
			
			$months = $this->im->getMaxMonth($year)->result();
			foreach ($months as $value) {
				$month = $value->max_month;
			}
			
			$codes = $this->im->getMaxCode($year, $month)->result();
			
			foreach ($codes as $value) {
				$code = $value->max_code;
			}
			
			if(!$code)
				$item_internal_move_code = $prefix.'0001';
			else {
				if($curr_year == $year && $curr_month == $month) {
							if(strlen($code) == 1)
							{
								$item_internal_move_code = $prefix.'000'.$code;	
							}
							else if(strlen($code) == 2)
							{
								$item_internal_move_code = $prefix.'00'.$code;
								
							}
							
							else if(strlen($code) == 3)
							{
								$item_internal_move_code = $prefix.'0'.$code;
								
							}
							else
							{
								$item_internal_move_code = $prefix.$code;
							}
						}
						
				else 
				{
					$item_internal_move_code = $prefix.'0001';
					
				}
			
			
			}
		}
		$response_data["response"] = $item_internal_move_code;
		$this->load->view("json_print_page", $response_data);
	}
	
}
