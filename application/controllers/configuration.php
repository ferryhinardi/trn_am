<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends CI_Controller {
	public function index()
	{
		$this->load->model("configurations_m", "app_config");
		$content["resultset"] = $this->app_config->get_configuration()->result_array();
		$page_content["content"] = $this->load->view("configuration", $content, true);
		$page_content["page_title"] = "Configuration";
		$page_content["title"] = "Configuration";
		$page_content["menu"] = $this->load->view("main_menu", "", true);
		
		$this->load->view("template/main_template", $page_content);
	}
	
	public function validate($application_name, $company_name, $company_contact_number_1, $company_contact_number_2) {
		$error_count = 0; 
		$error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
		$field_occured = array();
		
		if($application_name == "") {
			$error_messages .= "<li>Application Name Must be Filled !</li>";
			array_push($field_occured, "application-name");
			$error_count++;
		} else if(strlen($application_name) > 30) {
			$error_messages .= "<li>Length of Application Name Must not be Greater than 30 !</li>";
			array_push($field_occured, "application-name");
			$error_count++;
		} 
		if($company_name == "") {
			$error_messages .= "<li>Company Name Must be Filled !</li>";
			array_push($field_occured, "company-name");
			$error_count++;
		} else if(strlen($company_name) > 200) {
			$error_messages .= "<li>Length of Company Name Must not be Greater than 200 !</li>";
			array_push($field_occured, "company-name");
			$error_count++;
		} 		
		if (!preg_match ("/^((?:\+62|62)|0)[2-9]{1}[0-9]+$/", $company_contact_number_1) && $company_contact_number_1 != ""){
			$error_messages .= "<li>Contact Number 1 Must be in Phone Number Format!</li>";
			array_push($field_occured, "company-contact-number-1");		
			$error_count++;
		}
		if (!preg_match ("/^((?:\+62|62)|0)[2-9]{1}[0-9]+$/", $company_contact_number_2) && $company_contact_number_2 != ""){
			$error_messages .= "<li>Contact Number 2 Must be in Phone Number Format!</li>";
			array_push($field_occured, "company-contact-number-2");		
			$error_count++;
		}

		$error_messages .= "</ul>";
		
		return array (
			"is_valid" => ($error_count == 0),
			"error_messages" => $error_messages,
			"field_occured" => $field_occured
		);
	}
	
	public function update() {
		$application_name = $this->input->post("application-name");
		$company_name = $this->input->post("company-name");
		$company_address = $this->input->post("company-address");
		$company_contact_number_1 = $this->input->post("company-contact-number-1");
		$company_contact_number_2 = $this->input->post("company-contact-number-2");
		$after_login_messages = $this->input->post("after-login-messages");

		$response_data = array(
			"status" => null,
			"error_messages" => null,
			"field_occured" => null,
			"file_name" => null
		);
		
		$result_validation = $this->validate($application_name, $company_name, $company_contact_number_1, $company_contact_number_2);
		if(!$result_validation["is_valid"]) {
			$response_data["status"] = "failed";
			$response_data["error_messages"] = $result_validation["error_messages"];
			$response_data["field_occured"] = $result_validation["field_occured"];
		} else {
			$this->load->library('upload');
			$config["upload_path"] = "./media/img/logo/";
			$config["file_name"] = "company_logo";
			$config["overwrite"] = TRUE;
			$config["allowed_types"] = "gif|jpg|png|jpeg";
						
			$this->upload->initialize($config); 
			$this->upload->do_upload("company-logo");
			$upload_data = $this->upload->data();
			
			$this->load->model("configurations_m", "app_config");
			$this->app_config->modify(
				$application_name, $company_name, $company_address, 
				$company_contact_number_1, $company_contact_number_2, $upload_data["file_name"], $after_login_messages
			);
			
			$response_data["status"] = "success";
			$response_data["file_name"] = $upload_data["file_name"];
			
			$this->session->set_userdata(array(
				"application_name" => $application_name,
				"company_name" => $company_name,
				"company_logo" => $upload_data["file_name"],
				"after_login_messages" => $after_login_messages
			));
			
			foreach(glob("./media/img/logo/logo_temp/*.*") as $v) unlink($v);
		}	
		$response_arr["response"] = $response_data;
		$this->load->view("json_print_page", $response_arr);
	}
	
	public function preview_logo() {
		$this->load->library('upload');
		$config["upload_path"] = "./media/img/logo/logo_temp/";
		$config["file_name"] = "company_logo_up_temp";
		$config["overwrite"] = TRUE;
		$config["allowed_types"] = "gif|jpg|png|jpeg";
					
		$this->upload->initialize($config); 
		if($this->upload->do_upload("company-logo")) {
			$response_arr["response"] = $this->upload->data();
			$this->load->view("json_print_page", $response_arr);
		}
	}
}
