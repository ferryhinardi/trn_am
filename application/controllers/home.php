<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Home";
		
		$page_content["page_title"] = "Home";
		$page_content["title"] = "Home";
		$page_content["menu"] = $this->load->view("main_menu", "", true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
