<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Human Resources";
		
		$page_content["page_title"] = "Human Resources";
		$page_content["title"] = "Human Resources";
		
		$menu_params["current_navigable"] = "Home";
		$page_content["menu"] = $this->load->view("hrd/hrd_menu", $menu_params, true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
