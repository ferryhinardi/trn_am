<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$this->load->view("login");
	}
	
	public function do_logout() {
		$this->session->sess_destroy();
		redirect("login");
	}
}
