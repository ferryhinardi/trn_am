<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Manufacture";
		
		$page_content["page_title"] = "Manufacture";
		$page_content["title"] = "Manufacture";
		$page_content["menu"] = $this->load->view("manufacture/manufacture_menu", "", true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
