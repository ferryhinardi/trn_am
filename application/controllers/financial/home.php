<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$main_page_params["subtitle"] = "Financial";
		
		$page_content["page_title"] = "Financial";
		$page_content["title"] = "Financial";
		
		$menu_params["current_navigable"] = "Home";
		$page_content["menu"] = $this->load->view("financial/financial_menu", $menu_params, true);
		$page_content["content"] = "";
		
		$this->load->view("template/main_template", $page_content);
	}
}
