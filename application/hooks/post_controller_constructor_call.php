<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post_controller_constructor_call {
	var $CI;
	
    function __construct(){
		$this->CI =& get_instance();
    }

    function initialize_config() {
		date_default_timezone_set("Asia/Bangkok");
		if($this->CI->session->userdata("application_name") == FALSE) {
			$this->CI->load->model("configurations_m", "app_config");
			$result = $this->CI->app_config->get_configuration()->result_array();
			
			$this->CI->session->set_userdata(array(
				"application_name" => $result[0]["application_name"],
				"company_name" => $result[0]["company_name"],
				"company_logo" => $result[0]["company_logo"],
				"after_login_messages" => $result[0]["after_login_messages"]
			));
		}
    }
}
?>
