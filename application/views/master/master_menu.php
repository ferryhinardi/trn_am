<ul class="big-menu">
	<li class="with-right-arrow">
		<span>Item</span>
		<ul class="big-menu">
			<li><a href="<?php echo site_url("master/item/item")?>" <?php echo ($current_navigable == "Item") ? "class='navigable-current'" : "";?>>Items</a></li>
		</ul>
	</li>
	<li><a href="#">Currencies</a></li>
	<li><a href="#">Departments</a></li>
	<li><a href="#">Job Positions</a></li>
</ul>