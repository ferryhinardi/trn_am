<div id="search-stock-location-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchStockLocation(type, elId, elName) {
		$("#search-stock-location-container").html(
			"<h4 class='thin underline'>Stock Location Grid</h4>" +
			"<table id='search-slocation-grid'></table><div id='search-slocation-pager'></div>"
		);
		
		$("#search-slocation-grid").jqGrid( {
			url: "<?php echo site_url("master/location/location_list");?>",
			datatype: "json",
			gridview: true,
			postData: {location_type: type},
			height: "auto",
			colNames:["", "LOCATION NAME", "LOCATION NAME"],
			colModel:[
				{name:"temp", index:"temp", hidden: true},
				{name:"parent_child_name", index:"parent_child_name", search: false, sortable: false, width: 400, cellattr: default_cellattr_left},
				{name:"child_location_name", index:"child.location_name", hidden: true, searchoptions: {searchhidden: true}}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-slocation-pager",
			sortname: "id_stock_location",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-stock-location-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-slocation-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-slocation-grid_parent_child_name").addClass("jqgrid-column-left");
		$("#jqgh_search-slocation-grid_child_location_type").addClass("jqgrid-column-left");
		
		$("#search-stock-location-container").modal({
			title: "Select Stock Location",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-slocation-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						
						var row_data = $("#search-slocation-grid").jqGrid("getRowData", sel_id);
						$("#" + elId).val(sel_id);
						$("#" + elName).val(row_data.child_location_name);
					
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + elId).val("");
						$("#" + elName).val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-stock-location-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>