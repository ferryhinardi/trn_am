<h4 class="thin underline">Item Grid</h4>
<table id="item-grid"></table>
<div id="item-pager"></div>

<script type="text/javascript">
var curr_idx_sel = 0;
var editClicked = false;

function bindForm() {
	if($("#item-grid").jqGrid("getGridParam", "records") > 0) {
		var sel_id = $("#item-grid").jqGrid("getGridParam", "selrow");
		if(sel_id == null) {
			$(".form-input").val("");
			$("#item-supplier-grid").jqGrid("clearGridData");
			return;
		}
		
		var row_data = $("#item-grid").jqGrid("getRowData", sel_id);
		$("#pk-id-item").val(sel_id);
		$("#item-name").val(row_data.item_name);
		$("#pk-id-item-category").val(row_data.pk_id_item_category);
		$("#item-category-name").val(row_data.parent_child_category_name);
		$("#is-can-be-sold").attr("checked", row_data.is_can_be_sold == 1);
		$("#is-can-be-purchased").attr("checked", row_data.is_can_be_purchased == 1);
		$("#is-can-be-produced").attr("checked", row_data.is_can_be_produced == 1);
		$("#life-time").val(row_data.life_time != "" ? formatNumber(row_data.life_time) : "");
		$("#pk-id-item-units-of-measure").val(row_data.pk_id_item_units_of_measure);
		$("#uom-name").val(row_data.uom_name);
		$("#opening-balance").val(formatNumber(row_data.balance_quantity == "" ? "0.000" : row_data.balance_quantity));
		$("#date-balance").val(row_data.posting_date == "" ? "0000-00-00 00:00" : row_data.posting_date);
		$("#sale-price").val(formatNumber(row_data.sale_price));
		$("#cost-price").val(formatNumber(row_data.cost_price));
		$("#stock-on-hand-posting").val(formatNumber(row_data.stock_on_hand_posting));
		$("#stock-on-sale-posting").val(formatNumber(row_data.stock_on_sale_posting));
		$("#stock-on-purchase-posting").val(formatNumber(row_data.stock_on_purchase_posting));
		$("#stock-on-produce-posting").val(formatNumber(row_data.stock_on_produce_posting));
		$("#location-production-id").val(row_data.location_production_id);
		$("#location-production").val(row_data.location_production);
		$("#location-loss-id").val(row_data.location_loss_id);
		$("#location-loss").val(row_data.location_loss);
		$("#is-active").attr("checked", row_data.is_active == 1);
		
		$("#item-supplier-grid, #makloon-stock-grid").jqGrid("setGridParam", {
			postData: {pk_id_item: sel_id}
		});
		$("#item-supplier-grid").trigger("reloadGrid");
		$("#makloon-stock-pager_left .ui-icon-refresh").click();
	} else {
		$(".form-input").val("");
		$("#item-supplier-grid").jqGrid("clearGridData");
	}
}

function movePrev() {
	curr_idx_sel--;
	setGridSelection()
}

function moveNext() {
	curr_idx_sel++;
	setGridSelection()
}

function setGridSelection() {
	var curr_records = $("#item-grid").jqGrid("getGridParam", "reccount");
	var curr_page = $("#item-grid").jqGrid("getGridParam", "page");
	var last_page = $("#item-grid").jqGrid("getGridParam", "lastpage");
	var curr_row = curr_idx_sel + 1;
	if(curr_records > 0) {
		if(curr_row > curr_records) {
			if(curr_page == last_page) curr_idx_sel--;
			else {
				$("#item-grid").jqGrid("setGridParam", {page: parseInt(curr_page) + 1}).trigger("reloadGrid");
				curr_idx_sel = 0; 
			}
		} else if(curr_row < 1) {
			if(curr_page == 1) curr_idx_sel++;
			else {
				$("#item-grid").jqGrid("setGridParam", {page: parseInt(curr_page) - 1});
				curr_idx_sel = $("#item-grid").trigger("reloadGrid").jqGrid("getGridParam", "rowNum") - 1;
			}
		} else {	
			var curr_id =  $("#item-grid").jqGrid("getDataIDs")[curr_idx_sel];
			$("#item-grid").jqGrid("setSelection", curr_id);
		}
	}
}

function gridEditClicked(clicked_id) {
	$("#item-grid").jqGrid("setSelection", clicked_id);
	editClicked = true;
	$("#button-form-view, #button-edit").click();
}

$(function() {
	$("input:radio[name=button-set-view]").change(function() {
		if($(this).val() == "list-view") editClicked = false;
	});
	
	$("#item-grid").jqGrid( {
		url: "<?php echo site_url("master/item/item/item_list");?>",
		datatype: "json",
		gridview: true,
		height: "auto",
		colNames:["", "ITEM NAME", "ITEM CATEGORY", "SALE PRICE", "COST PRICE", "STOCK", "UOM", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
		colModel:[
			{name:"edit_act", index:"edit_act", search: false, sortable: false, resizable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center},
			{name:"item_name", index:"item_name", width:200, cellattr: default_cellattr_left},
			{name:"parent_child_category_name", index:"parent_child_category_name", sortable: false, width:200, cellattr: default_cellattr_left},
			{name:"sale_price", index:"sale_price", formatter: "integer", width:100, formatoptions:{thousandsSeparator: ","}, cellattr: default_cellattr_right},
			{name:"cost_price", index:"cost_price", formatter: "integer", width:100, formatoptions:{thousandsSeparator: ","}, cellattr: default_cellattr_right},
			{name:"stock_on_hand_posting", index:"stock_on_hand_posting", width:100, formatter: "integer", formatoptions:{thousandsSeparator: ","}, searchoptions: {sopt: ['eq','ne', 'lt', 'le', 'gt', 'ge', 'nu', 'nn', 'in', 'ni']}, cellattr: default_cellattr_right},
			{name:"uom_name", index:"uom_name", width:50, cellattr: default_cellattr_left},
			{name:"balance_quantity", index:"balance_quantity", hidden: true, formatter: "integer", formatoptions:{thousandsSeparator: ","}},
			{name:"posting_date", index:"posting_date", hidden: true},
			{name:"stock_on_sale_posting", index:"stock_on_sale_posting", hidden: true, formatter: "integer", formatoptions:{thousandsSeparator: ","}},
			{name:"stock_on_purchase_posting", index:"stock_on_purchase_posting", hidden: true, formatter: "integer", formatoptions:{thousandsSeparator: ","}},
			{name:"stock_on_produce_posting", index:"stock_on_produce_posting", hidden: true, formatter: "integer", formatoptions:{thousandsSeparator: ","}},
			{name:"pk_id_item_category", index:"pk_id_item_category", hidden: true},
			{name:"is_can_be_sold", index:"is_can_be_sold", hidden: true},
			{name:"is_can_be_purchased", index:"is_can_be_purchased", hidden: true},
			{name:"is_can_be_produced", index:"is_can_be_produced", hidden: true},
			{name:"life_time", index:"life_time", hidden: true},
			{name:"pk_id_item_units_of_measure", index:"pk_id_item_units_of_measure", hidden: true},
			{name:"location_production_id", index:"location_production_id", hidden: true},
			{name:"location_production", index:"location_production", hidden: true},
			{name:"location_loss_id", index:"location_loss_id", hidden: true},
			{name:"location_loss", index:"location_loss", hidden: true},
			{name:"item_category_name", index:"item_category_name", hidden: true},
			{name:"is_active", index:"is_active", hidden: true}
		],
		rowNum: 10,
		rowList: [10,20,30],
		mtype: "POST",
		pager: "#item-pager",
		sortname: "item_name",
		sortorder: "asc",
		toppager: true,
		viewrecords: true,
		gridComplete: function() {
			var rowID = $(this).getDataIDs();

			for (var i = 0; i < rowID.length; i++) {
				var rowData = $(this).jqGrid("getRowData", rowID[i]);
				
				if(rowData.stock_on_hand_posting <= 0)
					$(this).jqGrid("setRowData", rowID[i], false, {color: "red"});
			}
			setGridSelection();
		},
		onSelectRow: function() {
			if(!editClicked) { 
				var sel_id = $(this).jqGrid("getGridParam", "selrow");
				curr_idx_sel = $(this).jqGrid("getInd", sel_id) - 1; 
				if(!after_modify) bindForm();
				after_modify = false;
			}
		},
		loadError : serverError
	})
	.jqGrid("navGrid", "#item-pager",{cloneToTop:true, del:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true, multipleGroup:false, showQuery: false})
	.jqGrid("navButtonAdd","#item-grid_toppager_left",{caption:"New", buttonicon:"ui-icon-plus", onClickButton: function() {$("#button-form-view, #button-new").click();}, title:"New"});

	var topPagerDiv = $('#item-grid_toppager')[0];     
	$("#search_item-grid_top", topPagerDiv).remove();      
	$("#refresh_item-grid_top", topPagerDiv).remove();  
	$("#jqgh_item-grid_item_name").addClass("jqgrid-column-left");
	$("#jqgh_item-grid_parent_child_category_name").addClass("jqgrid-column-left");
	$("#jqgh_item-grid_uom_name").addClass("jqgrid-column-left");
	$("#jqgh_item-grid_sale_price").addClass("jqgrid-column-right");
	$("#jqgh_item-grid_cost_price").addClass("jqgrid-column-right");
	$("#jqgh_item-grid_stock_on_hand_posting").addClass("jqgrid-column-right");
	
	$("#container").bind("resize", function() {
		$("#item-grid").jqGrid("setGridWidth", ($("#container").width()), true);
	}).trigger("resize");
});
</script>