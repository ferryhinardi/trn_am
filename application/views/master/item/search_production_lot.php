<div id="search-production-lot-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchProductionLot(elId, elName) {
		$("#search-production-lot-container").html(
			"<h4 class='thin underline'>Production Lot Grid</h4>" +
			"<table id='search-plot-grid'></table><div id='search-plot-pager'></div>"
		);
		
		$("#search-plot-grid").jqGrid( {
			url: "<?php echo site_url("master/item/item/production_lot_list");?>",
			datatype: "json",
			gridview: true,
			height: "auto",
			colNames:["LOT"],
			colModel:[
				{name:"production_lot_name", index:"production_lot_name", width: 400, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-plot-pager",
			sortname: "inserted_date",
			sortorder: "desc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-production-lot-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-plot-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-plot-grid_production_lot_name").addClass("jqgrid-column-left");
		
		$("#search-production-lot-container").modal({
			title: "Select Production Lot",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-plot-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						
						var row_data = $("#search-plot-grid").jqGrid("getRowData", sel_id);
						$("#" + elId).val(sel_id);
						$("#" + elName).val(row_data.production_lot_name);
					
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + elId).val("");
						$("#" + elName).val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-production-lot-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>