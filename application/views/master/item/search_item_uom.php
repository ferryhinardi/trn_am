<div id="search-item-uom-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchItemUOM() {
		$("#search-item-uom-container").html(
			"<h4 class='thin underline'>UOM Grid</h4>" +
			"<table id='search-iuom-grid'></table><div id='search-iuom-pager'></div>"
		);
		
		$("#search-iuom-grid").jqGrid( {
			url: "<?php echo site_url("master/item/units_of_measure/uom_list");?>",
			datatype: "json",
			gridview: true,
			postData: {is_active: 1},
			height: "auto",
			colNames:["", "UOM NAME", "UOM CATEGORY"],
			colModel:[
				{name:"temp", index:"temp", hidden: true},
				{name:"uom_name", index:"uom_name", width: 400, cellattr: default_cellattr_left},
				{name:"uom_category_name", index:"uom_category_name", cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-iuom-pager",
			sortname: "uom_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-item-uom-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-iuom-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-iuom-grid_uom_name").addClass("jqgrid-column-left");
		$("#jqgh_search-iuom-grid_uom_category_name").addClass("jqgrid-column-left");
		
		$("#search-item-uom-container").modal({
			title: "Select Units of Measure",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-iuom-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						
						var row_data = $("#search-iuom-grid").jqGrid("getRowData", sel_id);
						$("#pk-id-item-units-of-measure").val(sel_id);
						$("#uom-name").val(row_data.uom_name);
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-item-uom-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>