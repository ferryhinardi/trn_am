<div id="search-item-pack-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchItemPack(elId, elName) {
		$("#search-item-pack-container").html(
			"<h4 class='thin underline'>Item Pack Grid</h4>" +
			"<table id='search-ipack-grid'></table><div id='search-ipack-pager'></div>"
		);
		
		$("#search-ipack-grid").jqGrid( {
			url: "<?php echo site_url("master/item/item/pack_list");?>",
			datatype: "json",
			gridview: true,
			height: "auto",
			colNames:["SERIAL"],
			colModel:[
				{name:"pack_name", index:"pack_name", width: 400, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-ipack-pager",
			sortname: "inserted_date",
			sortorder: "desc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-item-pack-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-ipack-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-ipack-grid_pack_name").addClass("jqgrid-column-left");
		
		$("#search-item-pack-container").modal({
			title: "Select Serial",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-ipack-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						
						var row_data = $("#search-ipack-grid").jqGrid("getRowData", sel_id);
						$("#" + elId).val(sel_id);
						$("#" + elName).val(row_data.pack_name);
					
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + elId).val("");
						$("#" + elName).val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-item-pack-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>