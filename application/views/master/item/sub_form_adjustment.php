<div id="sub-form-adjustment-container" style="width: 500px;">
	<form method="post" action="" class="columns" onsubmit="return false">
		<div class="eleven-columns div-form">
			<input type="hidden" name="id-location-adjust" id="id-location-adjust" class="form-input">
			<input type="hidden" name="id-lot-adjust" id="id-lot-adjust" class="form-input">
			<input type="hidden" name="id-packaging-adjust" id="id-packaging-adjust" class="form-input">
			<input type="hidden" name="id-pack-adjust" id="id-pack-adjust" class="form-input">
			<table class="table-form">
				<tr>
					<th colspan="3"><h5 class="align-left thin underline">Adjust Information</h5></th>
				</tr>
				<tr>
					<td class="align-right" style="width: 100px;"><label for="qty-adjust">New Stock :</label></td>
					<td colspan="2" style="padding-right: 5px;"><input type="text" name="qty-adjust" id="qty-adjust" class="adjust-input text"></td>
				</tr>
				<tr>
					<td class="align-right"><label for="location-adjust">Location :</label></td>
					<td style="padding-right: 5px;"><input type="text" name="location-adjust" id="location-adjust" readonly class="adjust-input text-search"></td>
					<td style="width: 20px;"><button type="button" class="button icon-search tiny button-search" id="button-search-location-adjust"></button></td>
				</tr>
				<tr>
					<td class="align-right"><label for="lot-adjust">Lot :</label></td>
					<td style="padding-right: 5px;"><input type="text" name="lot-adjust" id="lot-adjust" readonly class="adjust-input text-search"></td>
					<td style="width: 20px;"><button type="button" class="button icon-search tiny button-search" id="button-search-lot-adjust"></button></td>
				</tr>
				<tr>
					<td class="align-right"><label for="packaging-adjust">Package :</label></td>
					<td colspan="2"><select name="packaging-adjust" id="packaging-adjust" class="adjust-input text">
						<?php echo $option_item_packages; ?>
					</select></td>
				</tr>
				<tr>
					<td class="align-right"><label for="pack-adjust">Serial :</label></td>
					<td style="padding-right: 5px;"><input type="text" name="pack-adjust" id="pack-adjust" readonly class="adjust-input text-search"></td>
					<td style="width: 20px;"><button type="button" class="button icon-search tiny button-search" id="button-search-pack-adjust"></button></td>
				</tr>
			</table>
		</div>
	</form>
</div>

<script type="text/javascript">
	$("#sub-form-adjustment-container").hide();
	$(function() {
		$("#button-search-location-adjust").click(function() {
			searchStockLocation("Internal Location", "id-location-adjust", "location-adjust");
		});
		
		$("#button-search-pack-adjust").click(function() {
			searchItemPack("id-pack-adjust", "pack-adjust");
		});
		
		$("#button-search-lot-adjust").click(function() {
			searchProductionLot("id-lot-adjust", "lot-adjust");
		});
	});
	function validateAdjust(qty_adjust, id_location_adjust) {
		var error_count = 0, error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
		
		$(".error-field").removeClass("error-field");
		
		if(qty_adjust == "") {
			error_messages += "<li>New Stock Must be Filled !</li>"
			$("#qty-adjust").addClass("error-field");
			error_count++;
		} else if(isNaN(qty_adjust)) {
			error_messages += "<li>New Stock Must be Numeric Value !</li>";
			$("#qty-adjust").addClass("error-field");
			error_count++;
		} 
		if(id_location_adjust == "") {
			error_messages += "<li>Location Must be Filled !</li>"
			$("#location-adjust").addClass("error-field");
			error_count++;
		}
		error_messages += "</ul>";
		
		return [(error_count == 0), error_messages];
	}

	function openFormAdjustment() {
		$("#sub-form-adjustment-container").modal({
			title: "Form Adjustment",
			width: 500,
			scrolling: false,
			resizable: false,
			onOpen: function() {
				if($("#pk-id-item").val() != "") {
					$(".error-field").removeClass("error-field");
					$("#sub-form-adjustment-container form")[0].reset();
					$("#sub-form-adjustment-container").show();
				}
			},
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Save': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var id = $("#pk-id-item").val();
						var stock_on_hand = unFormatNumber($("#stock-on-hand-posting").val());
						var qty_adjust = $("#qty-adjust").val();
						var id_location_adjust = $("#id-location-adjust").val();
						var id_packaging_adjust = $("#id-packaging-adjust").val();
						var id_pack_adjust = $("#id-pack-adjust").val();
						var id_lot_adjust = $("#id-lot-adjust").val();
						var location_loss_id = $("#location-loss-id").val();
						var pk_id_item_units_of_measure = $("#pk-id-item-units-of-measure").val();
						
						var result_validation = validateAdjust(qty_adjust, id_location_adjust);
						
						if(!result_validation[0]) {
							$.modal.alert(result_validation[1]);
							$(".modal .button").focus();
						} else {
							$.ajax({
								type: "POST",
								url: "<?php echo site_url('master/item/item/adjust_stock'); ?>",
								data: {
									"id": id,
									"stock_on_hand": stock_on_hand,
									"qty_adjust": qty_adjust,
									"id_location_adjust": id_location_adjust,
									"id_packaging_adjust": id_packaging_adjust,
									"id_pack_adjust": id_pack_adjust,
									"id_lot_adjust": id_lot_adjust,
									"location_loss_id": location_loss_id,
									"pk_id_item_units_of_measure": pk_id_item_units_of_measure,
									"date": serverTime
								},
								dataType: 'json',
								beforeSend: function() {},
								success: function(response) {
									if(response.status == "success") {
										notify("Notification", 
											"Data Successfully Modified", {
											system: false,
											vPos: "top",
											hPos: "right",
											autoClose: true,
											icon: "",
											iconOutside: "outside",
											closeButton: true,
											showCloseOnHover: true,
											groupSimilar: true
										});
										
										$("#item-grid").trigger("reloadGrid");//$("#stock-on-hand-posting").val(response.new_stock);
										win.closeModal();
									} else if(response.status == "failed") {
										$.modal.alert(response.error_messages);
										$(".modal .button").focus();
										for(idx in response.field_occured) {
											$("#" + response.field_occured[idx]).addClass("error-field");
										}
									}
								},
								error: serverError,
								async: false,
								cache: true
							});
						}
					}
				}, 'Cancel': {
					classes: "green-gradient glossy",
					click: function(win) { 
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#sub-form-adjustment-container").hide();
			}
		});
	}
</script>