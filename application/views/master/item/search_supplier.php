<div id="search-supplier-container" style="width: 800px;"></div>
<script type="text/javascript">
	function searchSupplier() {
		var arr_id_not_in = [];
		var grid_ids = $("#item-supplier-grid").jqGrid("getDataIDs");
		var grid_count= $("#item-supplier-grid").getGridParam("records");
		for(var i = 0; i < grid_count; i++) {
			if(item_supplier_id != grid_ids[i]) {
				var data = $("#item-supplier-grid").getRowData(grid_ids[i]);
				arr_id_not_in.push(data.pk_id_partner);
			}
		}	
		
		$("#search-supplier-container").html(
			"<h4 class='thin underline'>Supplier Grid</h4>" +
			"<table id='search-supplier-grid'></table><div id='search-supplier-pager'></div>"
		);
		
		$("#search-supplier-grid").jqGrid( {
			url: "<?php echo site_url("master/item/item/supplier_search_list");?>",
			datatype: "json",
			gridview: true,
			height: "auto",
			postData: {id_not_in: arr_id_not_in.join(",")},
			colNames:["NAME", "CONTACT", "EMAIL", "CONTACT NO."],
			colModel:[
				{name:"partner_name", index:"partner_name", width: 200, cellattr: default_cellattr_left},
				{name:"contact_name", index:"contact_name", width: 200, cellattr: default_cellattr_left},
				{name:"email", index:"email", width: 200, cellattr: default_cellattr_left},
				{name:"contact_no", index:"contact_no", width: 100, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-supplier-pager",
			sortname: "partner_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-supplier-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-supplier-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-supplier-grid_partner_name").addClass("jqgrid-column-left");
		$("#jqgh_search-supplier-grid_contact_name").addClass("jqgrid-column-left");
		$("#jqgh_search-supplier-grid_email").addClass("jqgrid-column-left");
		$("#jqgh_search-supplier-grid_contact_no").addClass("jqgrid-column-left");
		
		$("#search-supplier-container").modal({
			title: "Select Supplier",
			width: 800,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-supplier-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(item_supplier_id != "") {
							var row_data = $("#search-supplier-grid").jqGrid("getRowData", sel_id);
							$("#" + item_supplier_id + "_pk_id_partner").val(sel_id);
							$("#" + item_supplier_id + "_partner_name").val(row_data.partner_name);
							$("#item-supplier-grid").jqGrid("setRowData", item_supplier_id, {
								"contact_name": row_data.contact_name,
								"email": row_data.email,
								"contact_no": row_data.contact_no
							});
						}
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-supplier-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>