<div id="search-item-category-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchItemCategory() {
		$("#search-item-category-container").html(
			"<h4 class='thin underline'>Item Category Grid</h4>" +
			"<table id='search-icategory-grid'></table><div id='search-icategory-pager'></div>"
		);
		
		$("#search-icategory-grid").jqGrid( {
			url: "<?php echo site_url("master/item/item_category/item_category_list");?>",
			datatype: "json",
			gridview: true,
			height: "auto",
			colNames:["", "ITEM CATEGORY NAME", "ITEM CATEGORY NAME"],
			colModel:[
				{name:"temp", index:"temp", hidden: true},
				{name:"parent_child_name", index:"parent_child_name", search: false, sortable: false, width: 400, cellattr: default_cellattr_left},
				{name:"child_item_category_name", index:"child.item_category_name", hidden: true, searchoptions: {searchhidden: true}}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-icategory-pager",
			sortname: "id_item_category",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-item-category-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-icategory-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-icategory-grid_parent_child_name").addClass("jqgrid-column-left");
		$("#jqgh_search-icategory-grid_child_item_category_name").addClass("jqgrid-column-left");
		
		$("#search-item-category-container").modal({
			title: "Select Item Category",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-icategory-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						
						var row_data = $("#search-icategory-grid").jqGrid("getRowData", sel_id);
						$("#pk-id-item-category").val(sel_id);
						$("#item-category-name").val(row_data.parent_child_name);
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-item-category-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>