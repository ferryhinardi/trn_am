<p class="button-height">
	<table style="width: 100%">
		<tr><td>
			<span class="button-group manipulation-button">
				<a href="javascript:void(0)" class="button" id="button-new">New</a>
				<a href="javascript:void(0)" class="button" id="button-edit">Edit</a>
				<a href="javascript:void(0)" class="button" id="button-delete">Delete</a>
			</span>
			<span class="button-group hidden commit-button">
				<a href="javascript:void(0)" class="button" id="button-save">Save</a>
				<a href="javascript:void(0)" class="button" id="button-cancel">Cancel</a>
			</span>
		</td>
		<td class="align-right">
			<span class="button-group manipulation-button">
				<a href="javascript:void(0)" class="button" id="button-prev">Prev</a>
				<a href="javascript:void(0)" class="button" id="button-next">Next</a>
			</span>
		</td></tr>
	</table>
</p>

<form method="post" action="" class="columns" onsubmit="return false">
	<div class="nine-columns div-form">
		<input type="hidden" name="pk-id-item" id="pk-id-item" class="form-input">
		<input type="hidden" name="pk-id-item-category" id="pk-id-item-category" class="form-input">
		<table class="table-form">
			<tr>
				<th colspan="3"><h5 class="align-left thin underline">Item Information</h5></th>
			</tr>
			<tr>
				<td class="align-right" style="width: 115px;"><label for="item-name">Item Name :</label></td>
				<td colspan="2"><input type="text" name="item-name" maxlength="50" id="item-name" class="form-input text" disabled></td>
			</tr>
			<tr>
				<td class="align-right"><label for="item-category-name">Item Category :</label></td>
				<td><input type="text" name="item-category-name" id="item-category-name" readonly class="form-input text-search" disabled></td>
				<td style="width: 20px;"><button type="button" class="button form-input icon-search tiny button-search" id="button-search-item-category" disabled></button></td>
			</tr>
			<tr>
				<td class="align-right"><label for="is-active">Active :</label></td>
				<td colspan="2"><input type="checkbox" name="is-active" id="is-active" value="1" class="form-input" disabled></td>
			</tr>
		</table>
	</div>
	<div class="three-columns div-form">
		<table class="table-form">
			<tr>
				<th colspan="2"><h5 class="align-left thin underline">Characteristics</h5></th>
			</tr>
			<tr>
				<td class="align-right"><label for="is-can-be-sold">Can be Sold :</label></td>
				<td><input type="checkbox" name="is-can-be-sold" id="is-can-be-sold" value="1" class="form-input" disabled></td>
			</tr>
			<tr>
				<td class="align-right"><label for="is-can-be-purchased">Can be Purchased :</label></td>
				<td><input type="checkbox" name="is-can-be-purchased" id="is-can-be-purchased" value="1" class="form-input" disabled></td>
			</tr>
			<tr>
				<td class="align-right"><label for="is-can-be-produced">Can be Produced :</label></td>
				<td><input type="checkbox" name="is-can-be-produced" id="is-can-be-produced" value="1" class="form-input" disabled></td>
			</tr>
		</table>
	</div>
	<div class="twelve-columns standard-tabs large-margin-top">
		<ul class="tabs">
			<li class="active"><a href="#tab-1">General Information</a></li>
			<li><a href="#tab-2">Suppliers</a></li>
			<li><a href="#tab-3">Makloon Stocks</a></li>
		</ul>

		<div class="tabs-content">
			<div id="tab-1" class="with-padding columns">
				<div class="six-columns div-form">
					<input type="hidden" name="pk-id-item-units-of-measure" id="pk-id-item-units-of-measure" class="form-input">
					<table class="table-form">
						<tr>
							<th colspan="2">
								<h5 class="align-left thin underline">Stock Information
									<button type="button" class="button icon-refresh tiny button-recalculate manipulation-button" id="button-recalculate-stock">Recalculate</button>
									<button type="button" class="button icon-gear tiny button-adjust manipulation-button" id="button-adjustment-stock">Adjustment</button>								
								</h5>
							</th>
						</tr>
						<tr>
							<td class="align-right" style="width: 95px;"><label for="stock-on-hand-posting">On Hand :</label></td>
							<td>
								<input type="text" value="0.000" name="stock-on-hand-posting" id="stock-on-hand-posting" class="text" disabled>
							</td>
						</tr>
						<tr>
							<td class="align-right" style="width: 95px;"><label for="stock-on-produce-posting">On Produce :</label></td>
							<td><input type="text" value="0.000" name="stock-on-produce-posting" id="stock-on-produce-posting" class="text" disabled></td>
						</tr>
						<tr>
							<td class="align-right" style="width: 95px;"><label for="stock-on-sale-posting">On Sale :</label></td>
							<td><input type="text" value="0.000" name="stock-on-sale-posting" id="stock-on-sale-posting" class="text" disabled></td>
						</tr>
						<tr>
							<td class="align-right" style="width: 95px;"><label for="stock-on-purchase-posting">On Purchase :</label></td>
							<td><input type="text" value="0.000" name="stock-on-purchase-posting" id="stock-on-purchase-posting" class="text" disabled></td>
						</tr>
					</table>
				</div>
				<div class="six-columns div-form">
					<table class="table-form">
						<tr>
							<th colspan="2"><h5 class="align-left thin underline">Price Information</h5></th>
						</tr>
						<tr>
							<td class="align-right" style="width: 80px;"><label for="sale-price">Sale Price :</label></td>
							<td><input type="text" name="sale-price" id="sale-price" class="form-input text-search" disabled></td>
						</tr>
						<tr>
							<td class="align-right"><label for="cost-price">Cost Price :</label></td>
							<td><input type="text" name="cost-price" id="cost-price" class="form-input text-search" disabled></td>
						</tr>
					</table>
				</div>
				<div class="six-columns new-row div-form">
					<table class="table-form">
						<tr>
							<th colspan="2"><h5 class="align-left thin underline">Balance Information</h5></th>
						</tr>
						<tr>
							<td class="align-right" style="width: 90px;"><label for="opening-balance">Opening :</label></td>
							<td><input type="text" name="opening-balance" id="opening-balance" value="0.000" class="text" disabled></td>
						</tr>
						<tr>
							<td class="align-right"><label for="date-balance">Posting Date :</label></td>
							<td><input type="text" name="date-balance" value="0000-00-00 00:00" id="date-balance" class="text" disabled></td>
						</tr>
					</table>
				</div>
				<div class="six-columns div-form">
					<input type="hidden" name="pk-id-item-units-of-measure" id="pk-id-item-units-of-measure" class="form-input">
					<table class="table-form">
						<tr>
							<th colspan="3"><h5 class="align-left thin underline">Default Information</h5></th>
						</tr>
						<tr>
							<td class="align-right" style="width: 120px;"><label for="uom-name">UOM :</label></td>
							<td style="padding-right: 5px;"><input type="text" name="uom-name" readonly id="uom-name" class="form-input text-search" disabled></td>
							<td style="width: 20px;"><button type="button" class="button form-input icon-search tiny button-search" id="button-search-uom" disabled></button></td>
						</tr>
						<tr>
							<td class="align-right"><label for="life-time">Life Time (Day) :</label></td>
							<td colspan="2"><input type="text" name="life-time" id="life-time" class="form-input text-search" disabled></td>
						</tr>
					</table>
				</div>
				<div class="twelve-columns new-row div-form">
					<input type="hidden" name="location-production-id" id="location-production-id" class="form-input">
					<input type="hidden" name="location-loss-id" id="location-loss-id" class="form-input">
					<table class="table-form">
						<tr>
							<th colspan="3"><h5 class="align-left thin underline">Location Information</h5></th>
						</tr>
						<tr>
							<td class="align-right" style="width: 80px;"><label for="location-production">Production :</label></td>
							<td style="padding-right: 5px;"><input type="text" name="location-production" maxlength="30" readonly id="location-production" class="form-input text-search" disabled></td>
							<td style="width: 20px;"><button type="button" class="button form-input icon-search tiny button-search" id="button-search-location-production" disabled></button></td>
						</tr>
						<tr>
							<td class="align-right"><label for="location-loss">Loss :</label></td>
							<td style="padding-right: 5px;"><input type="text" name="location-loss" maxlength="30" readonly id="location-loss" class="form-input text-search" disabled></td>
							<td style="width: 20px;"><button type="button" class="button form-input icon-search tiny button-search" id="button-search-location-loss" disabled></button></td>
						</tr>
					</table>
				</div>
			</div>
		
			<div id="tab-2" class="with-padding columns">
				<div id="item-supplier-grid-container" class="twelve-columns div-form">
					<table id="item-supplier-grid"></table>
					<div id="item-supplier-pager"></div>
				</div>
			</div>
		
			<div id="tab-3" class="with-padding columns">
				<div id="makloon-stock-grid-container" class="twelve-columns div-form">
					<table id="makloon-stock-grid"></table>
					<div id="makloon-stock-pager"></div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php echo $search_item_category;?>
<?php echo $search_item_uom;?>
<?php echo $search_stock_location;?>
<?php echo $search_supplier;?>
<?php echo $search_item_pack;?>
<?php echo $search_production_lot;?>
<?php echo $sub_form_adjustment;?>

<script type="text/javascript">
var oper = "";
var after_modify = false;
var item_supplier_id = "";
var item_supplier_counter = 0;
var item_supplier_oper = "";

function toggleState(state){
	$("#item-supplier-grid").jqGrid("setGridParam", {
		datatype: state ? "local" : "json"
	});
	
	$(".form-input").attr("disabled", !state);
	
	if(state) {
		$(".manipulation-button").addClass("hidden");
		$(".commit-button").removeClass("hidden");
		$("#item-supplier-grid-new, .item-supplier-edit, .item-supplier-delete").removeClass("hidden");
		$("#life-time").val($("#life-time").val() != "" ? unFormatNumber($("#life-time").val()) : "");
		$("#sale-price").val(unFormatNumber($("#sale-price").val()));
		$("#cost-price").val(unFormatNumber($("#cost-price").val()));
	} else {
		$("#item-supplier-grid").trigger("reloadGrid");
		$(".manipulation-button").removeClass("hidden");
		$(".commit-button").addClass("hidden");
		$(".error-field").removeClass("error-field");
		$("#item-supplier-grid-new, #item-supplier-grid-save, #item-supplier-grid-cancel, .item-supplier-edit, .item-supplier-delete, .button-search-supplier").addClass("hidden");
		if(!after_modify) bindForm();
		else {
			$("#life-time").val($("#life-time").val() != "" ? formatNumber($("#life-time").val()) : "");
			$("#sale-price").val(formatNumber($("#sale-price").val()));
			$("#cost-price").val(formatNumber($("#cost-price").val()));
		}
		oper = "";
	}
	
}	

function validate(item_name, pk_id_item_category, pk_id_item_units_of_measure, life_time, sale_price, cost_price) {
	var error_count = 0, error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
	
	$(".error-field").removeClass("error-field");
	
	if(oper == "add" || oper == "edit") {
		if(item_name == "") {
			error_messages += "<li>Item Name Must be Filled !</li>";
			$("#item-name").addClass("error-field");
			error_count++;
		} else if(item_name.length > 50) {
			error_messages += "<li>Length of Item Name Must not be Greater than 50 !</li>";
			$("#item-name").addClass("error-field");
			error_count++;
		} 
		if(pk_id_item_category == "") {
			error_messages += "<li>Item Category Must be Choosed !</li>";
			$("#item-category-name").addClass("error-field");
			error_count++;
		}
		if(pk_id_item_units_of_measure == "") {
			error_messages += "<li>Default UOM Must be Choosed !</li>";
			$("#uom-name").addClass("error-field");
			error_count++;
		} 
		if(life_time != "" && life_time == 0) {	
			error_messages += "<li>Life Time Must not be Equal to 0 !</li>";
			$("#life-time").addClass("error-field");
			error_count++;
		} else if(life_time != "" && isNaN(life_time)) {
			error_messages += "<li>Life Time Must be Numeric Value !</li>";
			$("#life-time").addClass("error-field");
			error_count++;
		} 
		if(sale_price == "") {	
			error_messages += "<li>Sale Price Must be Filled !</li>";
			$("#sale-price").addClass("error-field");
			error_count++;
		} else if(isNaN(sale_price)) {
			error_messages += "<li>Sale Price Must be Numeric Value !</li>";
			$("#sale-price").addClass("error-field");
			error_count++;
		} 
		if(cost_price == "") {	
			error_messages += "<li>Cost Price Must be Filled !</li>";
			$("#cost-price").addClass("error-field");
			error_count++;
		} else if(isNaN(cost_price)) {
			error_messages += "<li>Cost Price Must be Numeric Value !</li>";
			$("#cost-price").addClass("error-field");
			error_count++;
		} 
		if(item_supplier_id != "") {
			error_messages += "<li>Item Supplier Transaction Must be Finished !</li>"
			error_count++;
		}
	}
	error_messages += "</ul>";
	
	return [(error_count == 0), error_messages];
}

function manipulation() {
	var 
		id = $("#pk-id-item").val(),
		item_name = $("#item-name").val(),
		pk_id_item_category = $("#pk-id-item-category").val(),
		is_can_be_sold = $("#is-can-be-sold").is(":checked") ? 1 : 0,
		is_can_be_purchased = $("#is-can-be-purchased").is(":checked") ? 1 : 0,
		is_can_be_produced = $("#is-can-be-produced").is(":checked") ? 1 : 0,
		life_time = $("#life-time").val(),
		pk_id_item_units_of_measure = $("#pk-id-item-units-of-measure").val(),
		location_production_id = $("#location-production-id").val(),
		location_loss_id = $("#location-loss-id").val(),
		sale_price = $("#sale-price").val(),
		cost_price = $("#cost-price").val(),
		is_active = $("#is-active").is(":checked") ? 1 : 0;
	
	var 
		pk_id_partners = [],
		supplier_item_names = [];
		
	var grid_ids = $("#item-supplier-grid").jqGrid("getDataIDs");
	var grid_count= $("#item-supplier-grid").getGridParam("records");
	for(var i = 0; i < grid_count; i++) {
		var data = $("#item-supplier-grid").getRowData(grid_ids[i]);
		pk_id_partners.push(data.pk_id_partner);
		supplier_item_names.push(data.supplier_item_name);
	}	
	
	var result_validation = validate(item_name, pk_id_item_category, pk_id_item_units_of_measure, life_time, sale_price, cost_price);
	
	if(!result_validation[0]) {
		$.modal.alert(result_validation[1]);
		$(".modal .button").focus();
	} else {
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('master/item/item/manipulation'); ?>",
			data: {
				"oper": oper,
				"id": id,
				"item_name": item_name,
				"pk_id_item_category": pk_id_item_category,
				"is_can_be_sold": is_can_be_sold,
				"is_can_be_purchased": is_can_be_purchased,
				"is_can_be_produced": is_can_be_produced,
				"life_time": life_time,
				"pk_id_item_units_of_measure": pk_id_item_units_of_measure,
				"location_production_id": location_production_id,
				"location_loss_id": location_loss_id,
				"sale_price": sale_price,
				"cost_price": cost_price,
				"is_active": is_active,
				"pk_id_partners": pk_id_partners.join("#$#"),
				"supplier_item_names": supplier_item_names.join("#$#")
			},
			dataType: 'json',
			beforeSend: function() {},
			success: function(response) {
				if(response.status == "success") {
					notify("Notification", 
						"Data Successfully " + (oper == "add" ? "Inserted" : (oper == "edit" ? "Modified" : "Deleted")), {
						system: false,
						vPos: "top",
						hPos: "right",
						autoClose: true,
						icon: "",
						iconOutside: "outside",
						closeButton: true,
						showCloseOnHover: true,
						groupSimilar: true
					});
					
					if(oper != "del") {
						after_modify = true;
						$("#pk-id-item").val(response.new_id);
						
						$("#item-supplier-grid").jqGrid("setGridParam", {
							postData: {pk_id_item: response.new_id}
						}).trigger("reloadGrid");
					} else movePrev();
					
					$("#item-grid").trigger("reloadGrid");
					toggleState(false);
				} else if(response.status == "failed") {
					$.modal.alert(response.error_messages);
					$(".modal .button").focus();
					for(idx in response.field_occured) {
						$("#" + response.field_occured[idx]).addClass("error-field");
					}
				}
			},
			error: serverError,
			async: false,
			cache: true
		});
	}
}

$(function() {
	$("#button-recalculate-stock").click(function() {
		$.ajax({
			type: "POST",
			url: "<?php echo site_url("master/item/item/recalculate_stock"); ?>",
			dataType: "json",
			data: {"id": $("#pk-id-item").val()},
			success: function(response) {
				$("#stock-on-hand-posting").val(formatNumber(response.curr_stock_onhand));
			},
			error: serverError,
			async: true
		});
	});
	
	$("#button-adjustment-stock").click(function() {
		openFormAdjustment();
	});
	
	$("#button-cancel").click(function() {
		toggleState(false);
		editClicked = false;
	});
	
	$("#button-new").click(function() {
		toggleState(true);
		$(".form-input").val("");
		$("#item-name").focus();
		$("#opening-balance, #stock-on-hand-posting, #stock-on-purchase-posting, #stock-on-produce-posting, #stock-on-sale-posting").val("0").keyup();
		$("#date-balance").val("0000-00-00 00:00");
		$("#sale-price, #cost-price").val("0").keyup();
		$("#is-active").attr("checked", true);
		$("#is-can-be-sold, #is-can-be-produce, #is-can-be-purchased, #is-track-lots, #is-track-packs").attr("checked", false);
		$("#item-supplier-grid, #makloon-stock-grid").jqGrid("clearGridData");
		oper = "add";
	});
	
	$("#button-edit").click(function() {
		if($("#pk-id-item").val() == "") {
			$.modal.alert("No Data Selected.");
			$(".modal .button").focus();
			return;
		}
		toggleState(true);
		$("#item-name").focus().select();
		oper = "edit";
	});
	
	$("#button-delete").click(function() {
		if($("#pk-id-item").val() == "") {
			$.modal.alert("No Data Selected.");
			$(".modal .button").focus();
			return;
		}
		
		$.modal.confirm("Do you really want to delete this record ?", function() {
			oper = "del";
			manipulation();
		}, function() {});
		$(".modal .button").focus();
	});
	
	$("#button-save").click(function() {
		manipulation();
		editClicked = false;
	});
	
	$("#button-next").click(function() {
		moveNext();
	});
	
	$("#button-prev").click(function() {
		movePrev();
	});
	
	$("input:radio[name=button-set-view]").change(function() {
		if($(this).val() == "form-view") toggleState(false);
	});
	
	$("#item-category-name").keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 27) $("#item-category-name, #pk-id-item-category").val("");
	});
	$("#uom-name").keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 27) $("#uom-name, #pk-id-item-units-of-measure").val("");
	});
	$("#location-production").keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 27) $("#location-production, #location-production-id").val("");
	});
	$("#location-loss").keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 27) $("#location-loss, #location-loss-id").val("");
	});
 	
	$("#button-search-item-category").click(function() {
		searchItemCategory();
		$(".modal .button").focus();
	});
	$("#button-search-uom").click(function() {
		searchItemUOM();
		$(".modal .button").focus();
	});
	$("#button-search-location-production").click(function() {
		searchStockLocation("Production", "location-production-id", "location-production");
		$(".modal .button").focus();
	});
	$("#button-search-location-loss").click(function() {
		searchStockLocation("Inventory", "location-loss-id", "location-loss");
		$(".modal .button").focus();
	});
	
	$("#item-supplier-grid").jqGrid( {
		url: "<?php echo site_url("master/item/item/item_supplier_list");?>",
		datatype: "json",
		postData: {pk_id_item: 0},
		ajaxGridOptions: {async: false},
		gridview: true,
		height: "auto",
		colNames:["", "", "SUPPLIER&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchSupplier()'></button>", "ITEM NAME", "CONTACT", "EMAIL", "CONTACT NO.", ""],
		colModel:[
			{name:"edit_act", index:"edit_act", search: false, sortable: false, resizable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center},
			{name:"pk_id_partner", index:"itms.pk_id_partner", hidden: true, editable: true},
			{name:"partner_name", index:"partner_name", width:180, sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"supplier_item_name", index:"supplier_item_name", width:120, sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"contact_name", index:"contact_name", sortable: false, width: 150, cellattr: default_cellattr_left},
			{name:"email", index:"email", width: 200, sortable: false, cellattr: default_cellattr_left},
			{name:"contact_no", index:"contact_no", width: 100, sortable: false, cellattr: default_cellattr_left},
			{name:"del_act", index:"del_act", search: false, sortable: false, resizable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center}
		],
		pgbuttons: false,
		pginput: false,
		pgtext: false,
		mtype: "POST",
		pager: "#item-supplier-pager",
		toppager: true,
		loadComplete: function() {
			$("#item-supplier-grid-new, #item-supplier-grid-save, #item-supplier-grid-cancel, .item-supplier-edit, .item-supplier-delete").addClass("hidden");
			item_supplier_id = "";
			item_supplier_counter = $(this).jqGrid("getGridParam", "records");
		},
		loadError : serverError
	})
	.jqGrid("navGrid", "#item-supplier-pager",{cloneToTop:true, del:false, add:false, edit:false, search:false, refresh:false})
	.jqGrid("navButtonAdd","#item-supplier-grid_toppager_left",{id: "item-supplier-grid-new", caption:"New", buttonicon:"ui-icon-plus", onClickButton: function() {gridItemSupplierNewClicked();}, title:"New"})
	.jqGrid("navButtonAdd","#item-supplier-grid_toppager_left",{id: "item-supplier-grid-save", caption:"Save", buttonicon:"ui-icon-disk", onClickButton: function() {gridItemSupplierSaveClicked(item_supplier_id);}, title:"Save"})
	.jqGrid("navButtonAdd","#item-supplier-grid_toppager_left",{id: "item-supplier-grid-cancel", caption:"Cancel", buttonicon:"ui-icon-close", onClickButton: function() {gridItemSupplierCancelClicked(item_supplier_id);}, title:"Cancel"});
	
	$("#jqgh_item-supplier-grid_partner_name").addClass("jqgrid-column-left");
	$("#jqgh_item-supplier-grid_contact_name").addClass("jqgrid-column-left");
	$("#jqgh_item-supplier-grid_email").addClass("jqgrid-column-left");
	$("#jqgh_item-supplier-grid_contact_no").addClass("jqgrid-column-left");
	$("#jqgh_item-supplier-grid_supplier_item_name").addClass("jqgrid-column-left");
	
	var itemSupplierTopPagerDiv = $('#item-supplier-grid_toppager')[0];     
	$("#search_item-supplier-grid_top", itemSupplierTopPagerDiv).remove();      
	$("#refresh_item-supplier-grid_top", itemSupplierTopPagerDiv).remove();  
	
	$("#item-supplier-grid-container").bind("resize", function() {
		$("#item-supplier-grid").jqGrid("setGridWidth", $("#item-supplier-grid-container").width(), false);
	}).trigger("resize");
	
	
	$("#makloon-stock-grid").jqGrid( {
		url: "<?php echo site_url("master/item/item/makloon_stock_list");?>",
		datatype: "json",
		postData: {pk_id_item: 0},
		ajaxGridOptions: {async: false},
		gridview: true,
		height: "auto",
		colNames:["MAKLOON", "ON HAND", "ON PRODUCE"],
		colModel:[
			{name:"partner_name", index:"partner_name", width:350, editable: true, cellattr: default_cellattr_left},
			{name:"stock_on_hand_posting", index:"stock_on_hand_posting", width:100, formatter: "integer", formatoptions:{thousandsSeparator: ","}, cellattr: default_cellattr_right},
			{name:"stock_on_produce_posting", index:"stock_on_produce_posting", width:100, formatter: "integer", formatoptions:{thousandsSeparator: ","}, cellattr: default_cellattr_right}
		],
		rowNum: 5,
		rowList: [5,10,15],
		sortname: "partner_name",
		sortorder: "asc",
		mtype: "POST",
		toppager: true,
		pager: "#makloon-stock-pager",
		loadComplete: function() {},
		loadError : serverError
	})
	.jqGrid("navGrid", "#makloon-stock-pager",{del:false, add:false, edit:false, search:true, refresh:true}, {}, {}, {}, {multipleSearch: true, multipleGroup:false, showQuery: false});
	
	$("#jqgh_makloon-stock-grid_partner_name").addClass("jqgrid-column-left");
	$("#jqgh_makloon-stock-grid_stock_on_hand_posting").addClass("jqgrid-column-right");
	$("#jqgh_makloon-stock-grid_stock_on_produce_posting").addClass("jqgrid-column-right");
	
	$("#makloon-stock-grid-container").bind("resize", function() {
		$("#makloon-stock-grid").jqGrid("setGridWidth", ($("#makloon-stock-grid-container").width()));
	}).trigger("resize");
	
});

function gridItemSupplierEditClicked(clicked_id) {
	if(item_supplier_id == "") {
		$("#item-supplier-grid").jqGrid("editRow", clicked_id, false);
		item_supplier_id = clicked_id;
		item_supplier_oper = "edit";
		$("#item-supplier-grid-new, .item-supplier-edit, .item-supplier-delete").addClass("hidden");
		$("#item-supplier-grid-save, #item-supplier-grid-cancel, .button-search-supplier").removeClass("hidden");
	}
}
function gridItemSupplierNewClicked() {
	$("#item-supplier-grid").jqGrid("addRowData", ++item_supplier_counter, {
			edit_act: "<button type='button' title='Edit' class='item-supplier-edit link-edit tiny icon-pencil' onclick='gridItemSupplierEditClicked(" + item_supplier_counter + ")'></button>",
			pk_id_partner: "",
			partner_name: "",
			supplier_item_name: "",
			del_act: "<button type='button' title='Delete' class='item-supplier-delete link-edit tiny icon-cross' onclick='gridItemSupplierDelClicked(" + item_supplier_counter + ")'></button>"	
	}, "first");
	$("#item-supplier-grid").jqGrid("editRow", item_supplier_counter, false);
	item_supplier_id = item_supplier_counter;
	item_supplier_oper = "add";
	$("#item-supplier-grid-new, .item-supplier-edit, .item-supplier-delete").addClass("hidden");
	$("#item-supplier-grid-save, #item-supplier-grid-cancel, .button-search-supplier").removeClass("hidden");
}
function gridItemSupplierDelClicked(clicked_id) {
	$("#item-supplier-grid").jqGrid("delRowData", clicked_id);
}
function gridItemSupplierSaveClicked(clicked_id) {
	var valid = true;
	var id_partner = $("#" + clicked_id + "_pk_id_partner").val();
	if(id_partner == "") {
		$.modal.alert("Partner Name Must be Choosed !");
		$("#" + clicked_id + "_partner_name").addClass("error-field").focus().select();
		valid = false;
	}
	
	if(valid) {
		$("#item-supplier-grid").jqGrid("saveRow", clicked_id, true, "clientArray");
		$("#item-supplier-grid").jqGrid("restoreRow", clicked_id, false);
		item_supplier_id = "";
		$("#item-supplier-grid-new, .item-supplier-edit, .item-supplier-delete").removeClass("hidden");
		$("#item-supplier-grid-save, #item-supplier-grid-cancel, .button-search-supplier").addClass("hidden");
	}
}
function gridItemSupplierCancelClicked(clicked_id) {
	$("#item-supplier-grid").jqGrid("restoreRow", clicked_id, false);
	if(item_supplier_oper == "add") $("#item-supplier-grid").jqGrid("delRowData", clicked_id);
	item_supplier_id = "";
	$("#item-supplier-grid-new, .item-supplier-edit, .item-supplier-delete").removeClass("hidden");
	$("#item-supplier-grid-save, #item-supplier-grid-cancel, .button-search-supplier").addClass("hidden");
}
</script>