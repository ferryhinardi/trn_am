<ul class="big-menu">
	<li><a href="<?php echo site_url("purchases/purchase_requests")?>" <?php echo ($current_navigable == "Purchase Requests") ? "class='navigable-current'" : "";?>>Purchase Requests</a></li>
	<li><a href="<?php echo site_url("purchases/purchase_order")?>" <?php echo ($current_navigable == "Purchase Order") ? "class='navigable-current'" : "";?>>Purchase Orders</a></li>
	<li><a href="#">Histories & Reports</a></li>
</ul>