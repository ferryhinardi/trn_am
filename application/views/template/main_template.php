<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php echo $this->session->userdata("application_name");?> | <?php echo $page_title?></title>
	<meta name="description" content="Web-Based Business Application">
	<meta name="author" content="<?php echo $this->session->userdata("company_name");?>">

	<!-- http://davidbcalhoun.com/2010/viewport-metatag -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- For all browsers -->
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/reset-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/style-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/colors-v=1.css">
	<link rel="stylesheet" media="print" href="<?php echo base_url("media")?>/css/print-v=1.css">
	<!-- For progressively larger displays -->
	<link rel="stylesheet" media="only all and (min-width: 480px)" href="<?php echo base_url("media")?>/css/480-v=1.css">
	<link rel="stylesheet" media="only all and (min-width: 768px)" href="<?php echo base_url("media")?>/css/768-v=1.css">
	<link rel="stylesheet" media="only all and (min-width: 992px)" href="<?php echo base_url("media")?>/css/992-v=1.css">
	<link rel="stylesheet" media="only all and (min-width: 1200px)" href="<?php echo base_url("media")?>/css/1200-v=1.css">
	<!-- For Retina displays -->
	<link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="<?php echo base_url("media")?>/css/2x-v=1.css">

	<!-- Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

	<!-- Additional styles -->
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/agenda-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/dashboard-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/subtitle-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/form-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/modal-v=1.css">
	<!--<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/progress-slider-v=1.css">-->
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/styles/switches-v=1.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/others/jquery-ui-1.8.20.custom.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/others/ui.jqgrid.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/css/others/standard-style.css">
	<link rel="stylesheet" href="<?php echo base_url("media")?>/js/libs/CLEditor/jquery.cleditor.css">

	<!-- JavaScript at bottom except for Modernizr -->
	<script src="<?php echo base_url("media")?>/js/libs/modernizr.custom.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery-1.7.2.min.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery-ui-1.8.20.custom.min.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/i18n/grid.locale-en.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery.jqGrid.min.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery.ba-resize.min.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery.slider-access.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery.datetimepicker.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/jquery.form.js"></script>
	<script src="<?php echo base_url("media")?>/js/libs/standard-script.js"></script>

	<!-- For Modern Browsers -->
	<link rel="shortcut icon" href="<?php echo base_url("media")?>/img/favicons/favicon.png">
	<!-- For everything else -->
	<link rel="shortcut icon" href="<?php echo base_url("media")?>/img/favicons/favicon.ico">
	<!-- For retina screens -->
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url("media")?>/img/favicons/apple-touch-icon-retina.png">
	<!-- For iPad 1-->
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url("media")?>/img/favicons/apple-touch-icon-ipad.png">
	<!-- For iPhone 3G, iPod Touch and Android -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo base_url("media")?>/img/favicons/apple-touch-icon.png">

	<!-- iOS web-app metas -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">

	<!-- Startup image for web apps -->
	<link rel="apple-touch-startup-image" href="<?php echo base_url("media")?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
	<link rel="apple-touch-startup-image" href="<?php echo base_url("media")?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
	<link rel="apple-touch-startup-image" href="<?php echo base_url("media")?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

	<!-- Microsoft clear type rendering -->
	<meta http-equiv="cleartype" content="on">

	<!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
	<meta name="application-name" content="<?php echo $this->session->userdata("application_name");?>">
</head>

<body class="clearfix with-menu with-shortcuts">
	<div id="after-login-messages-container"></div>
	<!-- Prompt IE 6 users to install Chrome Frame -->
	<!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

	<!-- Title bar -->
	<header role="banner" id="title-bar">
		<h2><?php echo $this->session->userdata("company_name");?> (<?php echo $this->session->userdata("application_name");?>)</h2>
	</header>

	<!-- Button to open/hide menu -->
	<a href="javascript:void(0);" id="open-menu"><span>Menu</span></a>

	<!-- Button to open/hide shortcuts -->
	<a href="javascript:void(0);" id="open-shortcuts"><span class="icon-thumbs"></span></a>

	<!-- Main content -->
	<section role="main" id="main">

		<noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>
		<hgroup id="main-title" class="thin">
			<h1><?php echo $title;?></h1>
			<h2 id="server-time"><?php echo date("M");?> <?php echo date("d");?>, <?php echo date("Y");?> - <strong><?php echo date("H");?>:<?php echo date("i");?>:<?php echo date("s");?></strong></h2>
		</hgroup>
		
		<?php echo $content;?>

	</section>
	<!-- End main content -->

	<!-- Side tabs shortcuts -->
	<ul id="shortcuts" role="complementary" class="children-tooltip tooltip-right">
		<li <?php echo $title == "Master" ? "class='current'" : ""; ?>><a href="<?php echo site_url("master");?>" class="shortcut-dashboard" title="Master">Master</a></li>
		<li <?php echo $title == "Warehouse" ? "class='current'" : ""; ?>><a href="<?php echo site_url("warehouse");?>" class="shortcut-messages" title="Warehouse">Warehouse</a></li>
		<li <?php echo $title == "Manufacture" ? "class='current'" : ""; ?>><a href="<?php echo site_url("manufacture");?>" class="shortcut-agenda" title="Manufacture & Maintenance">Manufacture & Maintenance</a></li>
		<li <?php echo $title == "Purchases" ? "class='current'" : ""; ?>><a href="<?php echo site_url("purchases");?>" class="shortcut-contacts" title="Purchases">Purchases</a></li>
		<li <?php echo $title == "Sales" ? "class='current'" : ""; ?>><a href="<?php echo site_url("sales");?>" class="shortcut-medias" title="Sales">Sales</a></li>
		<li <?php echo $title == "Human Resources" ? "class='current'" : ""; ?>><a href="<?php echo site_url("hrd");?>" class="shortcut-stats" title="Human Resources">Human Resources</a></li>
		<li <?php echo $title == "Financial" ? "class='current'" : ""; ?>><a href="<?php echo site_url("financial");?>" class="shortcut-settings" title="Finance & Accounting">Finance & Accounting</a></li>
	</ul>

	<!-- Sidebar/drop-down menu -->
	<section id="menu" role="complementary">

		<!-- This wrapper is used by several responsive layouts -->
		<div id="menu-content">

			<header>
				Administrator
			</header>
			<div id="profile">
				<!--<img src="<?php echo base_url("media")?>/img/user.png" width="64" height="64" alt="User name" class="user-icon">-->
				<img src="<?php echo base_url("media/img/logo") . "/" . $this->session->userdata("company_logo"); ?>" style="width: 100px; height: 60px;" class="user-icon">
				Hello
				<span class="name">Admin <b></b></span>
			</div>

			<!-- By default, this section is made for 4 icons, see the doc to learn how to change this, in "basic markup explained" -->
			<ul id="access" class="children-tooltip">
				<li><a href="<?php echo site_url("home"); ?>" title="Home"><span class="icon-home"></span></a></li>
				<li><a href="javascript:void(0);" title="Profile"><span class="icon-user"></span></a></li>
				<li><a href="<?php echo site_url("configuration"); ?>" title="Configuration"><span class="icon-gear"></span></a></li>
			</ul>

			<section class="navigable">
				<?php echo $menu;?>
			</section>
		</div>
		<!-- End content wrapper -->

		<!-- This is optional -->
		<footer id="menu-footer">
			<p class="button-height" style="font-size: 87%; font-weight: bold;">
				&copy; 2012. All right reserved. | <a href="<?php echo site_url("login/do_logout"); ?>">Logout</a>
			</p>
		</footer>

	</section>
	<!-- End sidebar/drop-down menu -->

	<!-- JavaScript at the bottom for fast page loading -->

	<!-- Scripts -->
	<script src="<?php echo base_url("media")?>/js/setup.js"></script>

	<!-- Template functions -->
	<script src="<?php echo base_url("media")?>/js/developr.input.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.message.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.modal.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.navigable.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.notify.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.scroll.js"></script>
	<!--<script src="<?php echo base_url("media")?>/js/developr.progress-slider.js"></script>-->
	<script src="<?php echo base_url("media")?>/js/developr.tooltip.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.confirm.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.agenda.js"></script>
	<script src="<?php echo base_url("media")?>/js/developr.tabs.js"></script>		<!-- Must be loaded last -->

	<!-- Tinycon -->
	<script src="<?php echo base_url("media")?>/js/libs/tinycon.min.js"></script>

	<script>
		var serverTime = "";
		var isAfterLogin = "<?php echo !$this->session->userdata("is_after_login"); $this->session->set_userdata("is_after_login", TRUE);?>";
		
		
		function updateServerTime() {
			$.ajax({
				url: "<?php echo site_url("util/util/date_time"); ?>",
				dataType: 'json',
				success: function(response) {
					serverTime = response.serverTime;
					
					$("#server-time").html(response.month + " " + response.day + ", " + response.year + " - <strong>" + response.hour + ":" + response.minute + ":" + response.second + "</strong>");
				},
				async: true
			});
			setTimeout(function(){updateServerTime();}, 200);
		}
		
		updateServerTime();
		
		function showAfterLoginMessages() {
			$("#after-login-messages-container").html('<?php echo $this->session->userdata("after_login_messages"); ?>');
			$("#after-login-messages-container").modal({
				title: "Please Read Before Continue...",
				width: $(window).width(),
				height: $(window).height(),
				resizable: false,
				actions: {
					'Close' : {
						color: 'red',
						click: function(win) { win.closeModal(); }
					}
				},
				buttons: {
					'Continue . . .': {
						classes: "green-gradient glossy full-width",
						click: function(win) { win.closeModal(); }
					}
				},
				onClose: function() {
					$("#after-login-messages-container").html("");
				}
			});
		}
		if(isAfterLogin) showAfterLoginMessages();
		
		// Call template init (optional, but faster if called manually)
		$.template.init();

		// Favicon count
		Tinycon.setBubble();

		// If the browser support the Notification API, ask user for permission (with a little delay)
		if (notify.hasNotificationAPI() && !notify.isNotificationPermissionSet())
		{
			setTimeout(function()
			{
				notify.showNotificationPermission('Your browser supports desktop notification, click here to enable them.', function()
				{
					// Confirmation message
					if (notify.hasNotificationPermission())
					{
						notify('Notifications API enabled!', 'You can now see notifications even when the application is in background', {
							icon: 'img/demo/icon.png',
							system: true
						});
					}
					else
					{
						notify('Notifications API disabled!', 'Desktop notifications will not be used.', {
							icon: 'img/demo/icon.png'
						});
					}
				});

			}, 2000);
		}

		/*
		 * Handling of 'other actions' menu
		 */

		var otherActions = $('#otherActions'),
			current = false;

		// Other actions
		$('.list .button-group a:nth-child(2)').menuTooltip(otherActions, {

			classes: ['with-mid-padding'],

			onShow: function(target)
			{
				// Remove auto-hide class
				target.parent().removeClass('show-on-parent-hover');
			},

			onRemove: function(target)
			{
				// Restore auto-hide class
				target.parent().addClass('show-on-parent-hover');
			}
		});

		// Delete button
		$('.list .button-group a:last-child').data('confirm-options', {

			onShow: function()
			{
				// Remove auto-hide class
				$(this).parent().removeClass('show-on-parent-hover');
			},

			onConfirm: function()
			{
				// Remove element
				$(this).closest('li').fadeAndRemove();

				// Prevent default link behavior
				return false;
			},

			onRemove: function()
			{
				// Restore auto-hide class
				$(this).parent().addClass('show-on-parent-hover');
			}

		});

		// Demo modal
		function openModal()
		{
			$.modal({
				content: '<p>This is an example of modal window. You can open several at the same time (click links below!), move them and resize them.</p>'+
						  '<p>The plugin provides several other functions to control them, try below:</p>'+
						  '<ul class="simple-list with-icon">'+
						  '    <li><a href="javascript:void(0)" onclick="openModal()">Open new blocking modal</a></li>'+
						  '    <li><a href="javascript:void(0)" onclick="$.modal.alert(\'This is a non-blocking modal, you can switch between me and the other modal\', { blocker: false })">Open non-blocking modal</a></li>'+
						  '    <li><a href="javascript:void(0)" onclick="$(this).getModalWindow().setModalTitle(\'\')">Remove title</a></li>'+
						  '    <li><a href="javascript:void(0)" onclick="$(this).getModalWindow().setModalTitle(\'New title\')">Change title</a></li>'+
						  '    <li><a href="javascript:void(0)" onclick="$(this).getModalWindow().loadModalContent(\'ajax-demo/auto-setup.html\')">Load Ajax content</a></li>'+
						  '</ul>',
				title: 'Example modal window',
				width: 300,
				scrolling: false,
				actions: {
					'Close' : {
						color: 'red',
						click: function(win) { win.closeModal(); }
					},
					'Center' : {
						color: 'green',
						click: function(win) { win.centerModal(true); }
					},
					'Refresh' : {
						color: 'blue',
						click: function(win) { win.closeModal(); }
					},
					'Abort' : {
						color: 'orange',
						click: function(win) { win.closeModal(); }
					}
				},
				buttons: {
					'Close': {
						classes:	'huge blue-gradient glossy full-width',
						click:		function(win) { win.closeModal(); }
					}
				},
				buttonsLowPadding: true
			});
		};

		// Demo alert
		function openAlert()
		{
			$.modal.alert('This is an alert message', {
				buttons: {
					'Thanks, captain obvious': {
						classes:	'huge blue-gradient glossy full-width',
						click:		function(win) { win.closeModal(); }
					}
				}
			});
		};

		// Demo prompt
		function openPrompt()
		{
			var cancelled = false;

			$.modal.prompt('Please enter a value between 5 and 10:', function(value)
			{
				value = parseInt(value);
				if (isNaN(value) || value < 5 || value > 10)
				{
					$(this).getModalContentBlock().message('Please enter a correct value', { append: false, classes: ['red-gradient'] });
					return false;
				}

				$.modal.alert('Value: <strong>'+value+'</strong>');

			}, function()
			{
				if (!cancelled)
				{
					$.modal.alert('Oh, come on....');
					cancelled = true;
					return false;
				}
			});
		};

		// Demo confirm
		function openConfirm()
		{
			$.modal.confirm('Challenge accepted?', function()
			{
				$.modal.alert('Me gusta!');

			}, function()
			{
				$.modal.alert('Meh.');
			});
		};

		/*
		 * Agenda scrolling
		 * This example shows how to remotely control an agenda. most of the time, the built-in controls
		 * using headers work just fine
		 */

			// Days
		var daysName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

			// Name display
			agendaDay = $('#agenda-day'),

			// Agenda scrolling
			agenda = $('#agenda').scrollAgenda({
				first: 2,
				onRangeChange: function(start, end)
				{
					if (start != end)
					{
						agendaDay.text(daysName[start].substr(0, 3)+' - '+daysName[end].substr(0, 3));
					}
					else
					{
						agendaDay.text(daysName[start]);
					}
				}
			});

		// Remote controls
		$('#agenda-previous').click(function(event)
		{
			event.preventDefault();
			agenda.scrollAgendaToPrevious();
		});
		$('#agenda-today').click(function(event)
		{
			event.preventDefault();
			agenda.scrollAgendaFirstColumn(2);
		});
		$('#agenda-next').click(function(event)
		{
			event.preventDefault();
			agenda.scrollAgendaToNext();
		});

		// Demo loading modal
		function openLoadingModal()
		{
			var timeout;

			$.modal({
				contentAlign: 'center',
				width: 240,
				title: 'Loading',
				content: '<div style="line-height: 25px; padding: 0 0 10px"><span id="modal-status">Contacting server...</span><br><span id="modal-progress">0%</span></div>',
				buttons: {},
				scrolling: false,
				actions: {
					'Cancel': {
						color:	'red',
						click:	function(win) { win.closeModal(); }
					}
				},
				onOpen: function()
				{
						// Progress bar
					var progress = $('#modal-progress').progress(100, {
							size: 200,
							style: 'large',
							barClasses: ['anthracite-gradient', 'glossy'],
							stripes: true,
							darkStripes: false,
							showValue: false
						}),

						// Loading state
						loaded = 0,

						// Window
						win = $(this),

						// Status text
						status = $('#modal-status'),

						// Function to simulate loading
						simulateLoading = function()
						{
							++loaded;
							progress.setProgressValue(loaded+'%', true);
							if (loaded === 100)
							{
								progress.hideProgressStripes().changeProgressBarColor('green-gradient');
								status.text('Done!');
								/*win.getModalContentBlock().message('Content loaded!', {
									classes: ['green-gradient', 'align-center'],
									arrow: 'bottom'
								});*/
								setTimeout(function() { win.closeModal(); }, 1500);
							}
							else
							{
								if (loaded === 1)
								{
									status.text('Loading data...');
									progress.changeProgressBarColor('blue-gradient');
								}
								else if (loaded === 25)
								{
									status.text('Loading assets (1/3)...');
								}
								else if (loaded === 45)
								{
									status.text('Loading assets (2/3)...');
								}
								else if (loaded === 85)
								{
									status.text('Loading assets (3/3)...');
								}
								else if (loaded === 92)
								{
									status.text('Initializing...');
								}
								timeout = setTimeout(simulateLoading, 50);
							}
						};

					// Start
					timeout = setTimeout(simulateLoading, 2000);
				},
				onClose: function()
				{
					// Stop simulated loading if needed
					clearTimeout(timeout);
				}
			});
		};

	</script>

	<!-- Charts library -->
	<!-- Load the AJAX API -->
	<script src="http://www.google.com/jsapi"></script>
	<script>

		/*
		 * This script is dedicated to building and refreshing the demo chart
		 * Remove if not needed
		 */

		// Demo chart
		var chartInit = false,
			drawVisitorsChart = function()
			{
				// Create our data table.
				var data = new google.visualization.DataTable();
				var raw_data = [['Website', 50, 73, 104, 129, 146, 176, 139, 149, 218, 194, 96, 53],
								['Shop', 82, 77, 98, 94, 105, 81, 104, 104, 92, 83, 107, 91],
								['Forum', 50, 39, 39, 41, 47, 49, 59, 59, 52, 64, 59, 51],
								['Others', 45, 35, 35, 39, 53, 76, 56, 59, 48, 40, 48, 21]];

				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

				data.addColumn('string', 'Month');
				for (var i = 0; i < raw_data.length; ++i)
				{
					data.addColumn('number', raw_data[i][0]);
				}

				data.addRows(months.length);

				for (var j = 0; j < months.length; ++j)
				{
					data.setValue(j, 0, months[j]);
				}
				for (var i = 0; i < raw_data.length; ++i)
				{
					for (var j = 1; j < raw_data[i].length; ++j)
					{
						data.setValue(j-1, i+1, raw_data[i][j]);
					}
				}

				// Create and draw the visualization.
				// Learn more on configuration for the LineChart: http://code.google.com/apis/chart/interactive/docs/gallery/linechart.html
				var div = $('#demo-chart'),
					divWidth = div.width();
				new google.visualization.LineChart(div.get(0)).draw(data, {
					title: 'Monthly unique visitors count',
					width: divWidth,
					height: $.template.mediaQuery.is('mobile') ? 180 : 265,
					legend: 'right',
					yAxis: {title: '(thousands)'},
					backgroundColor: $.template.ie7 ? '#494C50' : 'transparent',	// IE7 and lower do not support transparency
					legendTextStyle: { color: 'white' },
					titleTextStyle: { color: 'white' },
					hAxis: {
						textStyle: { color: 'white' }
					},
					vAxis: {
						textStyle: { color: 'white' },
						baselineColor: '#666666'
					},
					chartArea: {
						top: 35,
						left: 30,
						width: divWidth-40
					},
					legend: 'bottom'
				});

				// Message only when resizing
				if (chartInit)
				{
					notify('Chart resized', 'The width change event has been triggered.', {
						icon: 'img/demo/icon.png'
					});
				}

				// Ready
				chartInit = true;
			};

		// Load the Visualization API and the piechart package.
		google.load('visualization', '1', {
			'packages': ['corechart']
		});

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawVisitorsChart);

		// Watch for block resizing
		$('#demo-chart').widthchange(drawVisitorsChart);

		// Respond.js hook (media query polyfill)
		$(document).on('respond-ready', drawVisitorsChart);
		
	</script>
	<script>
		var _gaq=[['_setAccount','UA-10643639-5'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
</body>
</html>
<!-- Localized -->