<div class="subtitle">
	<h3 class="underline">
		<span class="thin" style="color: white;"><?php echo $subtitle;?></span>
		<span class="button-group float-right">
			<?php if($list_view):?>
				<label for="button-list-view" class="button green-active icon-list" title="List View">
					<input type="radio" name="button-set-view" id="button-list-view" value="list-view" checked> List
				</label>
			<?php endif;?>

			<?php if($form_view):?>
				<label for="button-form-view" class="button green-active icon-page-list" title="Form View">
					<input type="radio" name="button-set-view" id="button-form-view" value="form-view"> Form
				</label>
			<?php endif;?>

			<?php if($calendar_view):?>
				<label for="button-calendar-view" class="button green-active icon-calendar" title="Calendar View">
					<input type="radio" name="button-set-view" id="button-calendar-view" value="calendar-view"> Calendar
				</label>
			<?php endif;?>
		</span>
	</h3>
</div>

<div class="with-padding" id="container">
	<?php echo $list_view ? "<div class='hidden manipulation-view' id='list-view'>" . $list_page . "</div>" : ""; ?>
	<?php echo $form_view ? "<div class='hidden manipulation-view' id='form-view'>" . $form_page . "</div>" : ""; ?>
	<?php echo $calendar_view ? "<div class='hidden manipulation-view' id='calendar-view'>" . $calendar_page . "</div>" : ""; ?>
</div>

<script type="text/javascript">
$(function() {
	//$(".manipulation-view").removeClass("hidden");
	$("#" + $("input:radio[name=button-set-view]:checked").val()).removeClass("hidden");
	$("input:radio[name=button-set-view]").change(function() {
		$(".manipulation-view").addClass("hidden");
		$("#" + $(this).val()).removeClass("hidden");
	});
});
</script>