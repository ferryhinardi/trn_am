<div class="subtitle">
	<h3 class="underline">
		<span class="thin" style="color: white;">Configuration</span>
	</h3>
</div>

<div class="with-padding" id="container">
<p class="button-height">
	<table style="width: 100%">
		<tr><td>
			<span class="button-group commit-button">
				<a href="javascript:void(0)" class="button" id="button-save">Save</a>
			</span>
		</td></tr>
	</table>
</p>

<form id="form-configuration" method="post" enctype="multipart/form-data" class="columns">
	<div class="twelve-columns div-form">
		<table class="table-form">
			<tr>
				<th colspan="3"><h5 class="align-left thin underline">General Information</h5></th>
			</tr>
			<tr>
				<td class="align-right" style="width: 125px;"><label for="application-name">Application Name :</label></td>
				<td><input type="text" name="application-name" maxlength="30" id="application-name" class="text" value="<?php echo $resultset[0]["application_name"]; ?>"></td>
			</tr>
			<tr>
				<td class="align-right"><label for="company-name">Company Name :</label></td>
				<td><input type="text" name="company-name" maxlength="200" id="company-name" class="text" value="<?php echo $resultset[0]["company_name"]; ?>"></td>
			</tr>
			<tr>
				<td class="align-right"><label for="company-address">Company Address :</label></td>
				<td><input type="text" name="company-address" maxlength="200" id="company-address" class="text" value="<?php echo $resultset[0]["company_address"]; ?>"></td>
			</tr>
			<tr>
				<td class="align-right"><label for="company-contact-number-1">Contact No. 1 :</label></td>
				<td><input type="text" name="company-contact-number-1" maxlength="200" id="company-contact-number-1" class="text" value="<?php echo $resultset[0]["company_contact_number_1"]; ?>"></td>
			</tr>
			<tr>
				<td class="align-right"><label for="company-contact-number-2">Contact No. 2 :</label></td>
				<td><input type="text" name="company-contact-number-2" maxlength="200" id="company-contact-number-2" class="text" value="<?php echo $resultset[0]["company_contact_number_2"]; ?>"></td>
			</tr>
		</table>
	</div>
	
	<div class="twelve-columns div-form">
		<table class="table-form">
			<tr>
				<th colspan="3"><h5 class="align-left thin underline">Logo</h5></th>
			</tr>
			<tr>
				<td class="align-right" style="width: 125px;"><label for="company-logo">Company Logo :</label></td>
				<td><input type="file" name="company-logo" maxlength="30" id="company-logo" class="text"></td>
			</tr>
			<tr>
				<td></td>
				<td><img id="preview-logo" style="width: 200px; height: 120px; border: 1px solid #ddd; padding: 5px" src="<?php echo base_url("media");?>/img/logo/<?php echo $resultset[0]["company_logo"]; ?>" alt="<?php echo $this->session->userdata("application_name");?>"></td>
			</tr>
		</table>
	</div>
	
	<div class="twelve-columns div-form">
		<table class="table-form">
			<tr>
				<th colspan="3"><h5 class="align-left thin underline">After Login Messages</h5></th>
			</tr>
			<tr>
				<td><div id="editor-wrapper"><textarea name="after-login-messages" id="after-login-messages"><?php echo $resultset[0]["after_login_messages"]; ?></textarea></div></td>
			</tr>
		</table>
	</div>
</form>
</div>

<script src="<?php echo base_url("media");?>/js/libs/CLEditor/jquery.cleditor.min.js"></script>
<script type="text/javascript">
var editorTextarea = $('#after-login-messages'),
	editor = editorTextarea.cleditor({
		width: '100%',
		height:	350,
		controls:    
			"bold italic underline strikethrough | font size " +
			"style | color highlight removeformat | bullets numbering | outdent " +
			"indent | alignleft center alignright justify | undo redo | " +
			"cut copy paste ",
	})[0];
	
function validate(application_name, company_name, company_contact_number_1, company_contact_number_2) {
	var error_count = 0, error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
	var phone_format = /^((?:\+62|62)|0)[2-9]{1}[0-9]+$/;
	
	$(".error-field").removeClass("error-field");
	
	if(application_name == "") {
		error_messages += "<li>Application Name Must be Filled !</li>";
		$("#application-name").addClass("error-field");
		error_count++;
	} else if(application_name.length > 30) {
		error_messages += "<li>Length of Application Name Must not be Greater than 30 !</li>";
		$("#application-name").addClass("error-field");		
		error_count++;
	} 
	if(company_name == "") {
		error_messages += "<li>Company Name Must be Filled !</li>";
		$("#company-name").addClass("error-field");
		error_count++;
	} else if(company_name.length > 200) {
		error_messages += "<li>Length of Company Name Must not be Greater than 200 !</li>";
		$("#company-name").addClass("error-field");		
		error_count++;
	} 
	if(!phone_format.test(company_contact_number_1) && company_contact_number_1 != ""){
		error_messages += "<li>Contact Number 1 Must be in Phone Number Format!</li>";
		$("#company-contact-number-1").addClass("error-field");
		error_count++;
	}
	if(!phone_format.test(company_contact_number_2) && company_contact_number_2 != ""){
		error_messages += "<li>Contact Number 2 Must be in Phone Number Format!</li>";
		$("#company-contact-number-2").addClass("error-field");
		error_count++;
	}
	error_messages += "</ul>";
	
	return [(error_count == 0), error_messages];
}

function manipulation() {
	var 
		application_name = $("#application-name").val(),
		company_name = $("#company-name").val(),
		company_address = $("#company-address").val(),
		company_contact_number_1 = $("#company-contact-number-1").val(),
		company_contact_number_2 = $("#company-contact-number-2").val(),
		company_logo = $("#company-logo").val(),
		after_login_messages = $("#after-login-messages").val();
	
	var result_validation = validate(application_name, company_name, company_contact_number_1, company_contact_number_2);
	
	if(!result_validation[0]) {
		$.modal.alert(result_validation[1]);
		$(".modal .button").focus();
	} else {
		$("#form-configuration").ajaxSubmit({
			type: "POST",
			url: "<?php echo site_url('configuration/update'); ?>",
			dataType: "json",
			success: function(response) {
				if(response.status == "success") {
					notify("Notification", 
						"Data Successfully Modified", {
						system: false,
						vPos: "top",
						hPos: "right",
						autoClose: true,
						icon: "",
						iconOutside: "outside",
						closeButton: true,
						showCloseOnHover: true,
						groupSimilar: true
					});					
					if(response.file_name != "" && response.file_name != "company_logo") 
						$("#preview-logo").attr("src", "<?php echo base_url("media");?>/img/logo/" + response.file_name);
				} else if(response.status == "failed") {
					$.modal.alert(response.error_messages);
					$(".modal .button").focus();
					for(idx in response.field_occured) {
						$("#" + response.field_occured[idx]).addClass("error-field");
					}
				}
			},
			error: serverError,
			async: false,
			cache: true
		});
	}
}

$(function() {
	$("#button-save").click(function() {
		manipulation();
	});
	
	$("#company-logo").change(function() {
		$("#form-configuration").ajaxSubmit({
			type: "POST",
			url: "<?php echo site_url('configuration/preview_logo'); ?>",
			dataType: "json",
			success: function(response) {
				$("#preview-logo").attr("src", "<?php echo base_url("media");?>/img/logo/logo_temp/" + response.file_name);
			},
			error: serverError,
			async: false,
			cache: true
		});
	});
});
</script>