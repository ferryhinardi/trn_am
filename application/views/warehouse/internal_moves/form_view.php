<p class="button-height">
	<table style="width: 100%">
		<tr><td>
			<span class="button-group manipulation-button">
				<a href="javascript:void(0)" class="button" id="button-new">New</a>
				<a href="javascript:void(0)" class="button" id="button-edit">Edit</a>
				<a href="javascript:void(0)" class="button" id="button-delete">Delete</a>
				<a href="javascript:void(0)" class="button" id="button-print">Print</a>
			</span>
			<span class="button-group manipulation-button">
				<a href="javascript:void(0)" class="button button-state" id="button-done" role="Done">Done</a>
			</span>
			<span class="button-group hidden commit-button">
				<a href="javascript:void(0)" class="button" id="button-save">Save</a>
				<a href="javascript:void(0)" class="button" id="button-cancel">Cancel</a>
			</span>
		</td>
		<td class="align-right">
			<span class="button-group manipulation-button">
				<a href="javascript:void(0)" class="button" id="button-prev">Prev</a>
				<a href="javascript:void(0)" class="button" id="button-next">Next</a>
			</span>
		</td></tr>
	</table>
</p>

<form method="post" action="" class="columns" onsubmit="return false">
	<div class="twelve-columns div-form">
		<input type="hidden" name="pk-id-item-internal-move" id="pk-id-item-internal-move" class="form-input">
		<input type="hidden" name="pk-id-item" id="pk-id-item" class="form-input">
		<input type="hidden" name="internal-move-state" id="internal-move-state" class="form-input">
		<input type="hidden" name="pk-id-item-units-of-measure" id="pk-id-item-units-of-measure" class="form-input">
		<input type="hidden" name="source-location-id" id="source-location-id" class="form-input">
		<input type="hidden" name="destination-location-id" id="destination-location-id" class="form-input">
		
		<table class="table-form">
			<tr> 	
				<th colspan="2"><h5 class="align-left thin underline">Internal Moves Information</h5></th>
			</tr>
			<tr>
				<td class="align-right" style="width: 120px;"><label for="partner-name">Internal Move No :</label></td>
				<td><input type="text" name="item-internal-move-code" id="item-internal-move-code" class="generate text" disabled></td>
			</tr>
			
			<tr>
				<td class="align-right" style="width: 120px;"><label for="contact-name">Date :</label></td>
				<td><input type="text" name="item-internal-move-date" id="item-internal-move-date" readonly class="form-input text-search" disabled></td>
			</tr>
		</table>
		<div class="twelve-columns standard-tabs margin-bottom large-margin-top" id="add-tabs" style="width: 100%">
		<ul class="tabs">
			<li class="active"><a href="#tab-1">Stock Moves</a></li>
			<li><a href="#tab-2">Notes</a></li>
		</ul>

		<div class="tabs-content" >
			<div id="tab-1" class="with-padding columns">
				<div id="stock_moves_grid_container" class="twelve-columns div-form">
					<table id="stock_moves_grid" ></table>
					<div id="stock_moves_pager"></div>
				</div>
			</div>

			<div id="tab-2" class="with-padding columns">
				<div class="twelve-columns div-form">
					<table class="table-form">
						<tr>
							<td>
								<textarea name="item-internal-moves-notes" id="item-internal-moves-notes" class="form-input text" disabled style="width:100%;height:120px;"></textarea>
							</td>
					</table>
				</div>
			</div>
		</div>
	</div>
		
	</div>
</form>

<?php echo $search_item_name;?>
<?php echo $search_uom_name;?>
<?php echo $search_production_lot_name;?>
<?php echo $search_pack_name;?>
<?php echo $search_source_location_name;?>
<?php echo $search_destination_location_name;?>
<?php echo $search_partner_name;?>

<script type="text/javascript">
var oper = "";
var after_modify = false;
var stock_moves_id = "";
var stock_moves_counter = 0;
var stock_moves_oper = "";

function toggleState(state){
	$("#stock_moves_grid").jqGrid("setGridParam", {
		datatype: state ? "local" : "json"
	});
	$(".form-input").attr("disabled", !state);

	if(state){ 
		$(".manipulation-button").addClass("hidden");
		$(".confirm-button").addClass("hidden");
		$(".commit-button").removeClass("hidden");
		$("#stock_moves_grid-new, .stock-moves-edit, .stock-moves-delete").removeClass("hidden");
		} else {
		
		$("#stock_moves_grid").trigger("reloadGrid");
		$(".manipulation-button").removeClass("hidden");
		$(".commit-button").addClass("hidden");
		$(".error-field").removeClass("error-field");
		$("#stock_moves_grid-new, #stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .stock-moves-edit, .stock-moves-delete, .button-search-supplier").addClass("hidden");
			
		if(!after_modify) bindForm();
		oper = ""; 
	}
}

function validate(item_internal_move_date, pk_id_items) {
	var error_count = 0, error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
	
	$(".error-field").removeClass("error-field");
	
	if(oper == "add" || oper == "edit") {
		if(item_internal_move_date == "") {
			error_messages += "<li>Internal Move Date Must be Filled !</li>";
			$("#item-internal-move-date").addClass("error-field");		
			error_count++;
		}
		
		if(stock_moves_id != "") {
			error_messages += "<li>Stock Moves Transaction Must be Finished !</li>"
			error_count++;
		}
		
		if(pk_id_items == "") {
			error_messages += "<li> Item Must be Filled in Grid Below !</li>"
			error_count++;
		}
	}
	error_messages += "</ul>";
	
	return [(error_count == 0), error_messages];
}

function manipulation() {
	var 
		id = $("#pk-id-item-internal-move").val(),
		item_internal_move_date = $("#item-internal-move-date").val(),
		item_internal_move_notes = $("#item-internal-moves-notes").val();
	
	var 
		pk_id_items= [],
		quantitys= [],
		pk_id_item_units_of_measures= [],
		pk_id_production_lots= [],
		pk_id_item_packages= [],
		pk_id_item_packs= [],
		source_location_ids= [],
		destination_location_ids= [],
		pk_id_partners= [];
		
		
	var grid_ids = $("#stock_moves_grid").jqGrid("getDataIDs");
	var grid_count= $("#stock_moves_grid").getGridParam("records");
	
	for(var i = 0; i < grid_count; i++) {
		var data = $("#stock_moves_grid").getRowData(grid_ids[i]);
		pk_id_items.push(data.pk_id_item);
		quantitys.push(data.quantity);
		pk_id_item_units_of_measures.push(data.pk_id_item_units_of_measure);
		pk_id_production_lots.push(data.pk_id_production_lot);
		pk_id_item_packages.push(data.pk_id_item_package);
		pk_id_item_packs.push(data.pk_id_item_pack);
		source_location_ids.push(data.source_location_id);
		destination_location_ids.push(data.destination_location_id);
		pk_id_partners.push(data.pk_id_partner);
	}

	
	var result_validation = validate(item_internal_move_date, pk_id_items);
	
	if(!result_validation[0]) {
		$.modal.alert(result_validation[1]);
		$(".modal .button").focus();
	} else {
	
	if(oper=='add') generate();
	item_internal_move_code = $("#item-internal-move-code").val(),
	
	$.ajax({
			type: "POST",
			url: "<?php echo site_url('warehouse/internal_moves/manipulation'); ?>",
			data: {
				"oper": oper,
				"id": id,
				"item_internal_move_code": item_internal_move_code,
				"item_internal_move_date": item_internal_move_date,
				"item_internal_move_notes": item_internal_move_notes,
				"pk_id_items": pk_id_items.join("#$"),
				"quantitys": quantitys.join("#$"),
				"pk_id_item_units_of_measures": pk_id_item_units_of_measures.join("#$"),
				"pk_id_production_lots": pk_id_production_lots.join("#$"),
				"pk_id_item_packages": pk_id_item_packages.join("#$"),
				"pk_id_item_packs": pk_id_item_packs.join("#$"),
				"source_location_ids": source_location_ids.join("#$"),
				"destination_location_ids": destination_location_ids.join("#$"),
				"pk_id_partners": pk_id_partners.join("#$")
				
			},
			
			dataType: 'json',
			beforeSend: function() {},
			success: function(response) {
				if(response.status == "success") {
					notify("Notification", 
						"Data Successfully " + (oper == "add" ? "Inserted" : (oper == "edit" ? "Modified" : "Deleted")), {
						system: false,
						vPos: "top",
						hPos: "right",
						autoClose: true,
						icon: "",
						iconOutside: "outside",
						closeButton: true,
						showCloseOnHover: true,
						groupSimilar: true
					});
						
					
					if(oper != "del") {
						after_modify = true;
						$("#pk-id-item-internal-move").val(response.new_id);			
						
						if(oper == "add")
							$("#button-edit, #button-delete, #button-done").removeClass("hidden");
						
						$("#stock_moves_grid").jqGrid("setGridParam", {
							postData: {id_internal_move: response.new_id}
						}).trigger("reloadGrid");
					} else {
						$(".generate").val("");
						movePrev();
					}
					
					$("#internal_moves_grid").trigger("reloadGrid");
					toggleState(false);
					
				} else if(response.status == "failed") {
					$.modal.alert(response.error_messages);
					$(".modal .button").focus();
					for(idx in response.field_occured) {
						$("#" + response.field_occured[idx]).addClass("error-field");
					}
				}
			},
			error: serverError,
			async: false,
			cache: true
		});
	}
}

function generate(){
	$.ajax({
		    type: "POST",
		    url: "<?php echo site_url('warehouse/internal_moves/generateCode'); ?>",
			dataType: 'json',
			beforeSend: function() {},
			success: function(response) {
				$("#item-internal-move-code").val(response);
			},
			error: serverError,
			async: false,
			cache: true
		 
		});
}

	$(function() {
	
	$('#item-internal-move-date').datetimepicker( {
        changeMonth: true,
        changeYear: true,
		yearRange: 'c-50:c+0',
		dateFormat: 'yy-mm-dd',
		timeFormat: 'hh:mm',
        addSliderAccess: true,
		sliderAccessArgs: {touchonly: false}        
		
    });
	
	$("#button-cancel").click(function() {
		$(".generate").val("");
		oper = "";
		toggleState(false);
		editClicked = false;
		
	});
	
	$(".button-state").click(function() {
		var id = $("#pk-id-item-internal-move").val();
		var state = $(this).attr("role");
		if(id != "") stateClicked(id, state);
	});
	
	$("#button-print").click(function() {
		var id = $("#pk-id-item-internal-move").val();
		if(id != "") gridPrintClicked(id, 1);
	});

	$("#button-new").click(function() {
		toggleState(true);
		$("#item-internal-move-code").val("");
		$(".form-input").val("");
		$("#stock_moves_grid").jqGrid("clearGridData");
		$("#item-internal-move-date").val(serverTime);
		oper = "add";
	});
	
	$("#button-edit").click(function() {
		if($("#pk-id-item-internal-move").val() == "") {
			$.modal.alert("No Data Selected.");
			$(".modal .button").focus();
			return;
		}
		toggleState(true);
		oper = "edit";
	});
	
	$("#button-delete").click(function() {
		if($("#pk-id-item-internal-move").val() == "") {
			$.modal.alert("No Data Selected.");
			$(".modal .button").focus();
			return;
		}
		
		$.modal.confirm("Do you really want to delete this record ?", function() {
			oper = "del";
			manipulation();
		}, function() {});
		$(".modal .button").focus();
	});
	
	$("#button-save").click(function() {
		manipulation();
		editClicked = false;
	});
	
	$("#button-next").click(function() {
		moveNext();
	});
	
	$("#button-prev").click(function() {
		movePrev();
	});
	
	$("input:radio[name=button-set-view]").change(function() {
		if($(this).val() == "form-view") toggleState(false);
	});

		$("#stock_moves_grid").jqGrid( {
		url: "<?php echo site_url("warehouse/internal_moves/stock_moves_list");?>",
		datatype: "json",
		postData: {id_internal_move: 0},
		ajaxGridOptions: {async: false},
		gridview: true,
		height: "auto",
		colNames:["", "", "ITEM&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchItemName()'></button>", "QTY", 
					"","", "UOM&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchUomName()'></button>",
					"","LOT&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchProductionLotName()'></button>","PACKAGE",
					"","SERIAL&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchSerialName()'></button>",
					"","SOURCE&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchSourceLocationName()'></button>",
					"","DEST&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchDestinationLocationName()'></button>",
					"","MAKLOON&nbsp;<button class='button-search-supplier hidden tiny icon-search button' onclick='searchPartnerName()'></button>",""],
		colModel:[
			{name:"edit_act", index:"edit_act", search: false, sortable: false, resizable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center},
			{name:"pk_id_item", index:"pk_id_item", hidden: true, editable: true},
			{name:"item_name", index:"item_name", sortable: false, width:200, editable: true, cellattr: default_cellattr_left},
			{name:"quantity", index:"quantity", sortable: false, width: 80, editable: true, formatter:"number", formatoptions:{thousandsSeparator: ",", decimalPlaces: 3}, cellattr: default_cellattr_right},
			{name:"pk_id_item_units_of_measure", index:"pk_id_item_units_of_measure", editable: true, hidden: true},
			{name:"pk_id_item_uom_category", index:"pk_id_item_uom_category", editable: true, hidden: true},
			{name:"uom_name", index:"uom_name", sortable: false , width:65, editable: true, cellattr: default_cellattr_left},
			{name:"pk_id_production_lot", index:"pk_id_production_lot", sortable: false, editable: true, hidden: true},
			{name:"production_lot_name", index:"production_lot_name", width:100, sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"pk_id_item_package", index:"pk_id_item_package", width: 80, edittype:"select", formatter:"select", editoptions:{value:"<?php echo $package;?>"}, sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"pk_id_item_pack", index:"pk_id_item_pack", sortable: false, editable: true, hidden: true},
			{name:"pack_name", index:"pack_name", sortable: false, width:100, editable: true, cellattr: default_cellattr_left},
			{name:"source_location_id", index:"source_location_id", sortable: false, editable: true, hidden: true},
			{name:"source_location_name", index:"source_location_name", width:200,sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"destination_location_id", index:"destination_location_id", sortable: false, editable: true, hidden: true},
			{name:"destination_location_name", index:"destination_location_name",width:150, sortable: false, editable: true, cellattr: default_cellattr_left},
			{name:"pk_id_partner", index:"pk_id_partner", sortable: false, editable: true, hidden: true},
			{name:"partner_name", index:"partner_name", sortable: false, width:200,editable: true, cellattr: default_cellattr_left},			
			{name:"del_act", index:"del_act", search: false, sortable: false, resizable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center}
		],
		pgbuttons: false,
		pginput: false,
		pgtext: false,
		mtype: "POST",
		pager: "#stock_moves_pager",
		toppager: true,
		loadComplete: function() {
			$("#stock_moves_grid-new, #stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .stock-moves-edit, .stock-moves-delete").addClass("hidden");
			
			stock_moves_id = "";
			stock_moves_counter = $(this).jqGrid("getGridParam", "records");
		},
		loadError : serverError
	})
	
	.jqGrid("navGrid", "#stock_moves_pager",{cloneToTop:true, del:false, add:false, edit:false, search:false, refresh:false}, {}, {}, {}, {multipleSearch: true, multipleGroup:false, showQuery: false})
	.jqGrid("navButtonAdd","#stock_moves_grid_toppager_left",{id: "stock_moves_grid-new", caption:"New", buttonicon:"ui-icon-plus", onClickButton: function() {gridStockMovesNewClicked();}, title:"New"})
	.jqGrid("navButtonAdd","#stock_moves_grid_toppager_left",{id: "stock_moves_grid-save", caption:"Save", buttonicon:"ui-icon-disk", onClickButton: function() {gridStockMovesSaveClicked(stock_moves_id);}, title:"Save"})
	.jqGrid("navButtonAdd","#stock_moves_grid_toppager_left",{id: "stock_moves_grid-save-duplicate", caption:"Save & Duplicate", buttonicon:"ui-icon-disk", onClickButton: function() {gridStockMovesSaveClicked(stock_moves_id, true);}, title:"Save & Duplicate"})
	.jqGrid("navButtonAdd","#stock_moves_grid_toppager_left",{id: "stock_moves_grid-cancel", caption:"Cancel", buttonicon:"ui-icon-close", onClickButton: function() {gridStockMovesCancelClicked(stock_moves_id);}, title:"Cancel"});
	
	$("#jqgh_stock_moves_grid_partner_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_item_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_quantity").addClass("jqgrid-column-right");
	$("#jqgh_stock_moves_grid_uom_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_production_lot_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_pk_id_item_package").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_pack_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_source_location_name").addClass("jqgrid-column-left");
	$("#jqgh_stock_moves_grid_destination_location_name").addClass("jqgrid-column-left");
	
	var stockMovesTopPagerDiv = $('#stock_moves_grid_toppager')[0];     
	$("#search_stock_moves_grid_top", stockMovesTopPagerDiv).remove();      
	$("#refresh_stock_moves_grid_top", stockMovesTopPagerDiv).remove();  
	
	$("#stock_moves_grid_container").bind("resize", function() {
		$("#stock_moves_grid").jqGrid("setGridWidth", ($("#stock_moves_grid_container").width()), false);
	}).trigger("resize");

	
});

function gridStockMovesEditClicked(clicked_id) {
	if(stock_moves_id == "") {
		$("#stock_moves_grid").jqGrid("editRow", clicked_id, false);
		stock_moves_id = clicked_id;
		stock_moves_oper = "edit";
		$("#stock_moves_grid-new, .stock-moves-edit, .stock-moves-delete").addClass("hidden");
		$("#stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .button-search-supplier").removeClass("hidden");
	}
}
function gridStockMovesNewClicked() {
	if(stock_moves_id == "") 
			$("#stock_moves_grid").jqGrid("addRowData", ++stock_moves_counter, {
				edit_act: "<button type='button' title='Edit' class='stock-moves-edit link-edit tiny icon-pencil' onclick='gridStockMovesEditClicked(" + stock_moves_counter + ")'></button>",
				pk_id_item: "",
				item_name: "",
				quantity: 0,
				pk_id_item_units_of_measure: "",
				pk_id_item_uom_category: "",
				uom_name: "",
				pk_id_production_lot: "",
				production_lot_name: "",
				pk_id_item_package: "",
				pk_id_item_pack: "",
				pack_name: "",
				source_location_id: "",
				source_location_name: "",
				destination_location_id: "",
				destination_location_name: "",
				pk_id_partner: "",
				partner_name: "",
				del_act: "<button type='button' title='Delete' class='stock-moves-delete link-edit tiny icon-cross' onclick='gridStockMovesDelClicked(" + stock_moves_counter + ")'></button>"
				
		}, "first");
		else{
			var data_duplicate = $("#stock_moves_grid").jqGrid("getRowData", stock_moves_id);
		
			$("#stock_moves_grid").jqGrid("addRowData", ++stock_moves_counter, {
			edit_act: "<button type='button' title='Edit' class='stock-moves-edit link-edit tiny icon-pencil' onclick='gridStockMovesEditClicked(" + stock_moves_counter + ")'></button>",
			pk_id_item: data_duplicate.pk_id_item,
			item_name: data_duplicate.item_name,
			quantity: 0,
			pk_id_item_units_of_measure: data_duplicate.pk_id_item_units_of_measure,
			pk_id_item_uom_category: data_duplicate.pk_id_item_uom_category,
			uom_name: data_duplicate.uom_name,
			pk_id_production_lot: data_duplicate.pk_id_production_lot,
			production_lot_name: data_duplicate.production_lot_name,
			pk_id_item_package: data_duplicate.pk_id_item_package,
			pk_id_item_pack: data_duplicate.pk_id_item_pack,
			pack_name: data_duplicate.pack_name,
			source_location_id: data_duplicate.source_location_id,
			source_location_name: data_duplicate.source_location_name,
			destination_location_id: data_duplicate.destination_location_id,
			destination_location_name: data_duplicate.destination_location_name,
			pk_id_partner: data_duplicate.pk_id_partner,
			partner_name: data_duplicate.partner_name,
			del_act: "<button type='button' title='Delete' class='stock-moves-delete link-edit tiny icon-cross' onclick='gridStockMovesDelClicked(" + stock_moves_counter + ")'></button>"
			
		}, "first");
		
		}
	
	$("#stock_moves_grid").jqGrid("editRow", stock_moves_counter, false);
	stock_moves_id = stock_moves_counter;
	stock_moves_oper = "add";
	$("#stock_moves_grid-new, .stock-moves-edit, .stock-moves-delete").addClass("hidden");
	$("#stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .button-search-supplier").removeClass("hidden");
}
function gridStockMovesDelClicked(clicked_id) {
	$("#stock_moves_grid").jqGrid("delRowData", clicked_id);
}
function gridStockMovesSaveClicked(clicked_id, duplicate) {
	var duplicate = duplicate || false;
	var valid = true;
	var id_item = $("#" + clicked_id + "_pk_id_item").val();
	var quantity1 = $("#" + clicked_id + "_quantity").val();
	var id_item_units_of_measure = $("#" + clicked_id + "_pk_id_item_units_of_measure").val();
	var source_location_id1 = $("#" + clicked_id + "_source_location_id").val();
	var destination_location_id1 = $("#" + clicked_id + "_destination_location_id").val();
	var id_partner = $("#" + clicked_id + "_pk_id_partner").val();
	
	if(id_item == "") {
		$.modal.alert("Item Name Must be Choosed !");
		$("#" + clicked_id + "_item_name").addClass("error-field").focus().select();
		valid = false;
	} else if(quantity1 == "") {
		$.modal.alert("Quantity Must be Filled !");
		$("#" + clicked_id + "_quantity").addClass("error-field").focus().select();
		valid = false;
	} else if(quantity1 <= 0) {
		$.modal.alert("Quantity Must be Greater Than 0 !");
		$("#" + clicked_id + "_quantity").addClass("error-field").focus().select();
		valid = false;
	} else if(isNaN(quantity1)) {
		$.modal.alert("Format Quantity Must be Numeric !");
		$("#" + clicked_id + "_quantity").addClass("error-field").focus().select();
		valid = false;
	} else if(id_item_units_of_measure == "") {
		$.modal.alert("UOM Must be Choosed !");
		$("#" + clicked_id + "_uom_name").addClass("error-field").focus().select();
		valid = false;
	} else if(source_location_id1 == "") {
		$.modal.alert("Source Location Must be Choosed !");
		$("#" + clicked_id + "_source_location_name").addClass("error-field").focus().select();
		valid = false;
	} else if(destination_location_id1 == "") {
		$.modal.alert("Destnation Location Must be Choosed !");
		$("#" + clicked_id + "_destination_location_name").addClass("error-field").focus().select();
		valid = false;
	}
	
	if(valid) {
		$("#stock_moves_grid").jqGrid("saveRow", clicked_id, true, "clientArray");
		$("#stock_moves_grid").jqGrid("restoreRow", clicked_id, false);
		
		if(duplicate) gridStockMovesNewClicked();
		else {
			stock_moves_id = "";
		$("#stock_moves_grid-new, .stock-moves-edit, .stock-moves-delete").removeClass("hidden");
		$("#stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .button-search-supplier").addClass("hidden");
		}
	}
}
function gridStockMovesCancelClicked(clicked_id) {
	$("#stock_moves_grid").jqGrid("restoreRow", clicked_id, false);
	if(stock_moves_oper == "add") $("#stock_moves_grid").jqGrid("delRowData", clicked_id);
	stock_moves_id = "";
	$("#stock_moves_grid-new, .stock-moves-edit, .stock-moves-delete").removeClass("hidden");
	$("#stock_moves_grid-save, #stock_moves_grid-save-duplicate, #stock_moves_grid-cancel, .button-search-supplier").addClass("hidden");
}
</script>