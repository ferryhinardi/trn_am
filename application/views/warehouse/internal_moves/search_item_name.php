<div id="search-item-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchItemName() {
		$("#search-item-name-container").html(
			"<h4 class='thin underline'>Item Grid</h4>" +
			"<table id='search-item-name-grid'></table><div id='search-item-name-pager'></div>"
		);
		
		$("#search-item-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/item_name_search_list");?>",
			datatype: "json",
			height: "auto",
			gridview: true,
			colNames:["ITEM NAME","","",""],
			colModel:[
				{name:"item_name", index:"item_name", width: 400, cellattr: default_cellattr_left},
				{name:"uom_name", index:"uom_name", hidden: true},
				{name:"pk_id_item_units_of_measure", index:"pk_id_item_units_of_measure", hidden: true},
				{name:"pk_id_item_uom_category", index:"pk_id_item_uom_category", hidden: true},
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-item-name-pager",
			sortname: "item_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-item-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-item-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-item-name-grid_item_name").addClass("jqgrid-column-left");
		
		$("#search-item-name-container").modal({
			title: "Select Item Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-item-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-item-name-grid").jqGrid("getRowData", sel_id);
							
							$("#" + stock_moves_id + "_pk_id_item").val(sel_id);
							$("#" + stock_moves_id + "_item_name").val(row_data.item_name);
							$("#" + stock_moves_id + "_uom_name").val(row_data.uom_name);
							$("#" + stock_moves_id + "_pk_id_item_units_of_measure").val(row_data.pk_id_item_units_of_measure);
							$("#" + stock_moves_id + "_pk_id_item_uom_category").val(row_data.pk_id_item_uom_category);
						}
						
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-item-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>