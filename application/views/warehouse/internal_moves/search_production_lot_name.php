<div id="search-production-lot-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchProductionLotName() {
		$("#search-production-lot-name-container").html(
			"<h4 class='thin underline'>Production Lot Grid</h4>" +
			"<table id='search-production-lot-name-grid'></table><div id='search-production-lot-name-pager'></div>"
		);
		
		$("#search-production-lot-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/production_lot_name_search_list");?>",
			datatype: "json",
			height: "auto",
			gridview: true,
			colNames:["PRODUCTION LOT NAME"],
			colModel:[
				{name:"production_lot_name", index:"production_lot_name", width: 400, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-production-lot-name-pager",
			sortname: "production_lot_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-production-lot-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-production-lot-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-production-lot-name-grid_production_lot_name").addClass("jqgrid-column-left");
		
		$("#search-production-lot-name-container").modal({
			title: "Select Production Lot Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-production-lot-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-production-lot-name-grid").jqGrid("getRowData", sel_id);
							$("#" + stock_moves_id + "_pk_id_production_lot").val(sel_id);
							$("#" + stock_moves_id + "_production_lot_name").val(row_data.production_lot_name);
						}
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + stock_moves_id + "_pk_id_production_lot").val("");
						$("#" + stock_moves_id + "_production_lot_name").val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-production-lot-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>