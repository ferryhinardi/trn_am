<div id="search-destination-location-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchDestinationLocationName() {
		$("#search-destination-location-name-container").html(
			"<h4 class='thin underline'>Destination Location Grid</h4>" +
			"<table id='search-destination-location-name-grid'></table><div id='search-destination-location-name-pager'></div>"
		);
		
		$("#search-destination-location-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/destination_location_name_search_list");?>",
			datatype: "json",
			height: "auto",
			gridview: true,
			colNames:["DESTINATION LOCATION NAME"],
			colModel:[
				{name:"destination_location_name", index:"destination_location_name", width: 400, cellattr: default_cellattr_left},
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-destination-location-name-pager",
			sortname: "location_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-destination-location-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-destination-location-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-destination-location-name-grid_destination_location_name").addClass("jqgrid-column-left");
		
		$("#search-destination-location-name-container").modal({
			title: "Select Destination Location Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-destination-location-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-destination-location-name-grid").jqGrid("getRowData", sel_id);
							$("#" + stock_moves_id + "_destination_location_id").val(sel_id);
							$("#" + stock_moves_id + "_destination_location_name").val(row_data.destination_location_name);
						}
						
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-destination-location-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>