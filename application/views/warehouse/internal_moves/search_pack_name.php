<div id="search-pack-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchSerialName() {
		$("#search-pack-name-container").html(
			"<h4 class='thin underline'>Serial Grid</h4>" +
			"<table id='search-pack-name-grid'></table><div id='search-pack-name-pager'></div>"
		);
		
		$("#search-pack-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/pack_name_search_list");?>",
			datatype: "json",
			height: "auto",
			gridview: true,
			colNames:["SERIAL NAME"],
			colModel:[
				{name:"pack_name", index:"pack_name", width: 400, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-pack-name-pager",
			sortname: "pack_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-pack-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-pack-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-pack-name-grid_pack_name").addClass("jqgrid-column-left");
		
		$("#search-pack-name-container").modal({
			title: "Select Serial Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-pack-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-pack-name-grid").jqGrid("getRowData", sel_id);
							$("#" + stock_moves_id + "_pk_id_item_pack").val(sel_id);
							$("#" + stock_moves_id + "_pack_name").val(row_data.pack_name);
						}
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + stock_moves_id + "_pk_id_item_pack").val("");
						$("#" + stock_moves_id + "_pack_name").val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-pack-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>