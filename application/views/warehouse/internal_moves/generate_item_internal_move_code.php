 <?php 
 
 public function generateCode($tableName, $idField, $dateField, $incLength) {
//ambil kode paling terakhir sesuai dengan bulan dan tahun pada jam server
        $queryCode = "SELECT MAX({$idField})AS last_code FROM `{$tableName}` 
                    WHERE DATE_FORMAT({$dateField}, '%y') = DATE_FORMAT(NOW(), '%y')
                    AND DATE_FORMAT({$dateField}, '%m') = DATE_FORMAT(NOW(), '%m')
                    ";
                    
        $increment = ""; //nilai yang di-increment disiapin.
        $rsLastCode = $this->db->query($queryCode); //ambil resultset dari query pada variable queryCode
        $lastCode = 0; //siapin variable buat tampung kode terakhir.
        
        //increment diisi dengan panjang sesuai dengan parameter $incLength. 
        //Contoh: 000 -> berarti diasumsikan tiap bulan data tidak melebihi 999
        for($i=0; $i<$incLength; $i++) $increment .= "0"; 
        
        foreach($rsLastCode->result() as $rs) $lastCode = $rs->last_code; //tampung resultset kode terakhir ke variable $lastCode
        
        //Kalo resultset-nya kosong berarti melewati "if" dan masuk ke "else" dan udah pasti bernilai "1"
        if($lastCode != "") {
            //potong lastCode dan ambil nilai angka yang di-increment
            //Contoh. INT/1205001 -> ambil 001 nya aja. terus ditampung ke $lastCodeArr
            //INT/1205001 berarti substr($lastCode, 8); utk penggunaan substr googling aja.
            //gw pake "($incLength + 1)" utk parameter kedua karena kebetulan gw butuh potong dari index ke-4
            $lastCodeArr     = substr($lastCode, ($incLength + 1));  
            
            //increment kode yg tadi uda dipotong. dan jangan lupa di-substr lagi tapi dengan nilai negative.
            //Contoh. nilai increment dari hasil looping adalah 000. terus digabung  dengan hasil increment. jadinya 0002
            //Ingat pake "." dan bukan "+". Kalo pake + jadi "2" dan bukan "0002".
            //Setelah dapet nilai "0002", di-substr lagi dimulai dari index 3 dari kanan atau "-3" jadi dapet "002".
            $increment         = substr(($increment . (++$lastCodeArr)), -($incLength));
        } else $increment     = substr(($increment . "1"), -($incLength)); //Kalo masuk else udah pasti "001"
        
        //nilai increment tinggal digabung dengan format yg udah ditentukan.
        return date("y") . date("m") . $increment;    //-> GeGeTenG
}