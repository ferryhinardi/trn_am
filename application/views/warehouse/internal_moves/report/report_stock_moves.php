<div style="font:normal 12px arial,sans-serif">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td width="201">
			<img width="150" height="100" src="<?php echo $this->config->item('header_logo').$configurations->company_logo; ?>"/>		</td>
	  <td width="301">
		<p style="font:bold 25px;color:#073F07"><?php echo $configurations->company_name; ?> </p>
				<p style="font:bold 18px;color:#700000">INTERNAL MOVES</p></td>
	</tr>
    <tr>
    	<td style="color:#A80000"><?php echo $configurations->company_address; ?></td>
    </tr>
	<tr>
		<td style="color:#A80000">
			<?php 
			if($configurations->company_contact_number_1 != "") echo $configurations->company_contact_number_1; 
			if($configurations->company_contact_number_2 != "") echo ' | '.$configurations->company_contact_number_2; ?>		</td>
		<td align="right"> <?php echo date('d F Y'); ?></td>
	</tr>
</table>
<hr />

<div style="padding-bottom:10px;padding-top:10px">
<table style="BORDER: #cccccc 0.5px solid" width="50%" cellpadding="5" cellspacing="0" rules="all">
	<tr>
    	<td style="color:#073F07">INTERNAL MOVE NO    : </td>
		<td style="color:#073F07"><?php echo $internal_moves->item_internal_move_code; ?></td>
    </tr>
	<tr>
    	<td style="color:#073F07">INTERNAL MOVE DATE  : </td>
		<td style="color:#073F07"><?php echo $internal_moves->item_internal_move_date; ?></td>
    </tr>
</div>
</table>

<div style="padding-bottom:10px;padding-top:10px">
<table style="BORDER: #cccccc 0.5px solid;border-collapse: collapse" width="100%" cellpadding="5" cellspacing="0" rules="all">
	<tr>
    	<td style="color:#073F07" align="right">NO.</td>
        <td style="color:#073F07" align="left">ITEM</td>
        <td style="color:#073F07" align="right">QTY</td>
        <td style="color:#073F07" align="left">UOM</td>
        <td style="color:#073F07" align="left">LOT</td>
		<td style="color:#073F07" align="left">PACKAGE</td>
		<td style="color:#073F07" align="left">SERIAL</td>
		<td style="color:#073F07" align="left">SOURCE</td>
		<td style="color:#073F07" align="left">DEST</td>
		<td style="color:#073F07" align="left">MAKLOON</td>
	</tr>
	
	<?php $counter = 1; ?>
        <?php foreach($stock_moves as $stock_moves_list): ?>
    <tr>
    	<td style="color:#073F07" align="center"><?php echo $counter++; ?></td>
        <td style="color:#073F07" align="left"><?php echo $stock_moves_list->item_name; ?></td>
        <td style="color:#073F07" align="right"><?php echo $stock_moves_list->quantity; ?></td>
        <td style="color:#073F07" align="left"><?php echo $stock_moves_list->uom_name; ?></td>
        <td style="color:#073F07" align="left"><?php echo $stock_moves_list->production_lot_name; ?></td>
		<td style="color:#073F07" align="left"><?php echo $stock_moves_list->package_name; ?></td>
		<td style="color:#073F07" align="left"><?php echo $stock_moves_list->pack_name; ?></td>
		<td style="color:#073F07" align="left"><?php echo $stock_moves_list->source_location_name; ?></td>
		<td style="color:#073F07" align="left"><?php echo $stock_moves_list->destination_location_name; ?></td>
		<td style="color:#073F07" align="left"><?php echo $stock_moves_list->partner_name; ?></td>
    </tr>
	 <?php endforeach; ?>
</table>
</div>

<div style="padding-bottom:10px;padding-top:10px">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<td style="color:#073F07">KETERANGAN : </td>
    </tr>
	<tr>
    	<td height="70px" style="BORDER: #cccccc 0.5px solid; color:#073F07;vertical-align: top"><?php echo $internal_moves->item_internal_move_notes; ?> </td>
    </tr>
</table>
</div>

<!-- <table width="670px" cellpadding="0" cellspacing="0">
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
	<tr>
    	<th><img width="670" height="100" src="<?php echo $this->config->item('footer_signature').'footer_signature_stock_moves.png'?>" /></th>
    </tr>
</table> -->
</div>