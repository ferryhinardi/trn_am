<div id="search-partner-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchPartnerName() {
		$("#search-partner-name-container").html(
			"<h4 class='thin underline'>Makloon Grid</h4>" +
			"<table id='search-partner-name-grid'></table><div id='search-partner-name-pager'></div>"
		);
		
		$("#search-partner-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/makloon_name_search_list");?>",
			datatype: "json",
			height: "auto",
			gridview: true,
			colNames:["PARTNER MAKLOON NAME"],
			colModel:[
				{name:"partner_makloon_name", index:"partner_makloon_name", width: 400, cellattr: default_cellattr_left},
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-partner-name-pager",
			sortname: "partner_makloon_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-partner-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-partner-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-partner-name-grid_partner_makloon_name").addClass("jqgrid-column-left");
		
		$("#search-partner-name-container").modal({
			title: "Select Makloon Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy",
					click: function(win) { 
						var sel_id = $("#search-partner-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-partner-name-grid").jqGrid("getRowData", sel_id);
							$("#" + stock_moves_id + "_pk_id_partner").val(sel_id);
							$("#" + stock_moves_id + "_partner_name").val(row_data.partner_makloon_name);
						}
						
						win.closeModal(); 
					}
				}, 'Remove': {
					classes: "green-gradient glossy",
					click: function(win) { 
						$("#" + stock_moves_id + "_pk_id_partner").val("");
						$("#" + stock_moves_id + "_partner_name").val("");
					
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-partner-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>