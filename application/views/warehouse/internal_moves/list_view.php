<h4 class="thin underline">Filter</h4>
	<table class="table-form" style="width: 100%">
		<tr>
			<td>
				<span class="button-group">
							<label class="button green-active" title="Draft">
								<input type="checkbox" name="button-set-state-search" value="Draft" id="button-state-draft" checked> Draft
							</label>
							<label class="button green-active" title="Done">
								<input type="checkbox" name="button-set-state-search" value="Done" id="button-state-done"> Done
							</label>
				</span> 
			</td>
			<td>
				<label for="serch_date_from">From :</label>
				<input type="text" name="date_from" id="serch_date_from" readonly>

				<label for="serch_date_until">To :</label>
				<input type="text" name="date_until" id="serch_date_until" readonly>
						
				<a href="javascript:void(0)" class="button" id="button-search">Search</a>
				<a href="javascript:void(0)" class="button" id="button-clear">Clear</a>
				
			</td>
		</tr>
	</table>

<h4 class="thin underline">Internal Moves Grid</h4>
<table id="internal_moves_grid"></table>
<div id="internal_moves_pager"></div>

<script type="text/javascript">
var curr_idx_sel = 0;
var editClicked = false;

function bindForm() {
	if($("#internal_moves_grid").jqGrid("getGridParam", "records") > 0) {
		var sel_id = $("#internal_moves_grid").jqGrid("getGridParam", "selrow");
		if(sel_id == null) {
			$(".form-input").val("");
			$("#stock_moves_grid").jqGrid("clearGridData");
			return;
		}
		
		var row_data = $("#internal_moves_grid").jqGrid("getRowData", sel_id);
		$("#pk-id-item-internal-move").val(sel_id);
		$("#item-internal-move-code").val(row_data.item_internal_move_code);
		$("#item-internal-move-date").val(row_data.item_internal_move_date);
		$("#item-internal-moves-notes").val(row_data.item_internal_move_notes);
		$("#internal-move-state").val(row_data.internal_move_state);
		
		$("#button-edit, #button-delete, #button-print, #button-done").removeClass("hidden");
		
		if (row_data.internal_move_state == "Draft")
			$("#button-done").removeClass("hidden"); 
		else if(row_data.internal_move_state == "Done")
			$("#button-edit, #button-delete, #button-done").addClass("hidden");
		else $(".button-state").addClass("hidden"); 
			
		$("#stock_moves_grid").jqGrid("setGridParam", {
			postData: {id_internal_move: sel_id}
		}).trigger("reloadGrid");

	} 
	else 
	{
		$(".form-input").val("");
		$("#stock_moves_grid").jqGrid("clearGridData");
		$(".button-state").addClass("hidden");
	}
}

function movePrev() {
	curr_idx_sel--;
	setGridSelection()
}

function moveNext() {
	curr_idx_sel++;
	setGridSelection()
}

function stateClicked(current_id, state) {
	$.modal.confirm("Do you really want to change the state of this record into \"" + state + "\" ?", function() {
		$.ajax({
			type: "POST",
			url: "<?php echo site_url("warehouse/internal_moves/set_state"); ?>",
			data: {
				"id": current_id,
				"state": state
			},
			dataType: 'json',
			beforeSend: function() {},
			success: function(response) {
				notify("Notification", 
					"Data Successfully Modified", {
					system: false,
					vPos: "top",
					hPos: "right",
					autoClose: true,
					icon: "",
					iconOutside: "outside",
					closeButton: true,
					showCloseOnHover: true,
					groupSimilar: true
				});
				$("#internal_moves_grid").trigger("reloadGrid");
			},
			error: serverError,
			async: false,
			cache: true
		});
	}, function() {});
	$(".modal .button").focus();
}

function setGridSelection() {
	var curr_records = $("#internal_moves_grid").jqGrid("getGridParam", "reccount");
	var curr_page = $("#internal_moves_grid").jqGrid("getGridParam", "page");
	var last_page = $("#internal_moves_grid").jqGrid("getGridParam", "lastpage");
	var curr_row = curr_idx_sel + 1;
	if(curr_records > 0) {
		if(curr_row > curr_records) {
			if(curr_page == last_page) curr_idx_sel--;
			else {
				$("#internal_moves_grid").jqGrid("setGridParam", {page: parseInt(curr_page) + 1}).trigger("reloadGrid");
				curr_idx_sel = 0; 
			}
		} else if(curr_row < 1) {
			if(curr_page == 1) curr_idx_sel++;
			else {
				$("#internal_moves_grid").jqGrid("setGridParam", {page: parseInt(curr_page) - 1});
				curr_idx_sel = $("#internal_moves_grid").trigger("reloadGrid").jqGrid("getGridParam", "rowNum") - 1;
			}
		} else {	
			var curr_id =  $("#internal_moves_grid").jqGrid("getDataIDs")[curr_idx_sel];
			$("#internal_moves_grid").jqGrid("setSelection", curr_id);
		}
	}
}

function gridEditClicked(clicked_id) {
	$("#internal_moves_grid").jqGrid("setSelection", clicked_id);
	editClicked = true;
	$("#button-form-view, #button-edit").click();
}

function gridPrintClicked(clicked_id,type,states) {
	if(type == 1)
	{
		var internal_move_code = $("#item-internal-move-code").val();
		internal_move_code = internal_move_code.replace('/','');
		window.open('internal_moves/report/'+type+'/'+clicked_id+'/'+internal_move_code+'/0/0/0');		
	}
	else if(type == 2)
	{	
		var
		date_from = ($("#serch_date_from").val() != "") ? new Date ($("#serch_date_from").val()) : 0,
		date_until = ($("#serch_date_until").val() != "") ? new Date ($("#serch_date_until").val()) : 0;
		
		if(date_from != 0 && date_until !=0)
		{
			var
			curr_date = date_from.getDate(),
			curr_month = date_from.getMonth() + 1,
			curr_year = date_from.getFullYear(),
			date_from = curr_year + "-" + curr_month + "-" + curr_date;
			
			var
			curr_date = date_until.getDate(),
			curr_month = date_until.getMonth() + 1,
			curr_year = date_until.getFullYear(),
			date_until = curr_year + "-" + curr_month + "-" + curr_date;
			
		}		
		window.open('internal_moves/report/'+type+'/0/0/'+states+'/'+date_from+'/'+date_until);	
	}
}

$(function() {

	var states = ["Draft"], states2 = "Draft", status1, statusdraft, statusdone;
	
	$("#button-search").click(function() {
		states = [], states2 = "", status1 = "", statusdraft = "", statusdone = "";
		
		$("input:checkbox[name=button-set-state-search]").each(function() {
			if($(this).is(":checked")) {
				states.push($(this).val());
				states2 += $(this).val(); }
		});	
		if(states2 == "DraftDone") states2 = "all";

		if($("#serch_date_from").val() != "")
		{
			if($("#serch_date_until").val() == "")
			{
				var error_messages = "<h4 class='thin underline'>Correct the Following Error(s)</h4><ul class='align-left'>";
				error_messages += "<li>Date Destination Must be Filled !</li>";
				error_messages += "</ul>";
				
				$("#search_date_until").addClass("error-field");		
				$.modal.alert(error_messages);
				status1 = "failed";
			}
		}
		
		if(status1 != "failed")
		$("#internal_moves_grid").jqGrid("setGridParam", {
			postData: {type:"search", state: states.join("','"), date_from: $("#serch_date_from").val(), date_until: $("#serch_date_until").val()}
		}).trigger("reloadGrid");
		
	});
	
	$("#button-clear").click(function() {
		$("#serch_date_from").val("");
		$("#serch_date_until").val("");
		
		$("input:checkbox[name=button-set-state-search]").each(function() {
			if($(this).is(":checked")) {
				if($(this).val() == "Done") {
					statusdone = "Clear";
					$(this).click(); } }
			else if($(this).val() == "Draft") {
				statusdraft = "Clear";
				$(this).click(); }
		});
		
		$("#internal_moves_grid").trigger("reloadGrid");
		
		
		
	});
	
	$("#button-state-draft").click(function() {
		if(statusdraft == "Clear") $(this).removeAttr('checked');
	});
	
	$("#button-state-done").click(function() {
		if(statusdone == "Clear") $(this).attr('checked','checked');
	});

	$('#serch_date_from').datetimepicker( {
        changeMonth: true,
        changeYear: true,
		yearRange: 'c-50:c+0',
		dateFormat: 'yy-mm-dd',
		showTime: false,
        addSliderAccess: true,
		sliderAccessArgs: {touchonly: false}        
		
    });
	
	$('#serch_date_until').datetimepicker( {
        changeMonth: true,
        changeYear: true,
		yearRange: 'c-50:c+0',
		dateFormat: 'yy-mm-dd',
		showTime: false,
		addSliderAccess: true,
		sliderAccessArgs: {touchonly: false}        
		
    });

	$("input:radio[name=button-set-view]").change(function() {
		if($(this).val() == "list-view") editClicked = false;
	});
	
	$("#internal_moves_grid").jqGrid( {
		url: "<?php echo site_url("warehouse/internal_moves/internal_moves_list");?>",
		datatype: "json",
		postData: {type:"", state: "", date_from:"", date_until:""},
		gridview: true,
		height: "auto",
		colNames:["","", "INTERNAL MOVE NO.", "DATE","STATE","","",""],
		colModel:[
			{name:"edit_act", index:"edit_act", search: false, resizable: false, sortable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center},
			{name:"id_internal_move", index:"id_internal_move", hidden: true},
			{name:"item_internal_move_code", index:"item_internal_move_code", width:500, cellattr: default_cellattr_left},
			{name:"item_internal_move_date", index:"item_internal_move_date", width:500, cellattr: default_cellattr_left},
			{name:"internal_move_state", index:"internal_move_state", width:120, fixed: true, cellattr: default_cellattr_center},
			{name:"item_internal_move_notes", index:"item_internal_move_notes", hidden: true},
			{name:"state_act", index:"state_act", search: false, resizable: false, sortable: false, width:30, fixed: true, frozen: true, cellattr: default_cellattr_center},
			{name:"print_act", index:"print_act", search: false, sortable: false, resizable: false, width:30, fixed: true, cellattr: default_cellattr_center}
		],
		rowNum: 10,
		rowList: [10,20,30],
		mtype: "POST",
		pager: "#internal_moves_pager",
		sortname: "item_internal_move_code",
		sortorder: "desc",
		toppager: true,
		viewrecords: true,
		gridComplete: function() {
			var rowID = $(this).getDataIDs();

			for (var i = 0; i < rowID.length; i++) {
				var rowData = $(this).jqGrid("getRowData", rowID[i]);
				if(rowData.internal_move_state == 'Done') $(this).jqGrid("setRowData", rowID[i],false,{color: "grey"});		
			}
		
			setGridSelection();
		},
		onSelectRow: function() {
			if(!editClicked) { 
				var sel_id = $(this).jqGrid("getGridParam", "selrow");
				curr_idx_sel = $(this).jqGrid("getInd", sel_id) - 1; 
				if(!after_modify) bindForm();
				after_modify = false;
			}
		},
		loadError : serverError
	})
		
	.jqGrid("navGrid", "#internal_moves_pager",{cloneToTop:true, del:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true, multipleGroup:false, showQuery: false})
	.jqGrid("navButtonAdd","#internal_moves_grid_toppager_left",{caption:"New", buttonicon:"ui-icon-plus", onClickButton: function() {$("#button-form-view, #button-new").click();}, title:"New"})
	.jqGrid("navButtonAdd","#internal_moves_grid_toppager_left",{caption:"Print", id:"button-print-all", buttonicon:"ui-icon-print", onClickButton:  function() {gridPrintClicked('','2',states2);}, title:"Print"});
	
	var topPagerDiv = $('#internal_moves_grid_toppager')[0];     
	$("#search_internal_moves_grid_top", topPagerDiv).remove();      
	$("#refresh_internal_moves_grid_top", topPagerDiv).remove();  
	$("#jqgh_internal_moves_grid_item_internal_move_code").addClass("jqgrid-column-left");
	$("#jqgh_internal_moves_grid_item_internal_move_date").addClass("jqgrid-column-left");
	$("#jqgh_internal_moves_grid_internal_move_state").addClass("jqgrid-column-center");
		
	$("#container").bind("resize", function() {
		$("#internal_moves_grid").jqGrid("setGridWidth", ($("#container").width()), true);
	}).trigger("resize");
});
</script>