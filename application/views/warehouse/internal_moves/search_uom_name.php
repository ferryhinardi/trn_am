<div id="search-uom-name-container" style="width: 500px;"></div>
<script type="text/javascript">
	function searchUomName() {
		$("#search-uom-name-container").html(
			"<h4 class='thin underline'>UOM Grid</h4>" +
			"<table id='search-uom-name-grid'></table><div id='search-uom-name-pager'></div>"
		);
		
		$("#search-uom-name-grid").jqGrid( {
			url: "<?php echo site_url("warehouse/internal_moves/uom_name_search_list");?>",
			datatype: "json",
			gridview: true,
			postData: {pk_id_item_uom_category: $("#" + stock_moves_id + "_pk_id_item_uom_category").val()},
			height: "auto",
			colNames:["","UOM NAME"],
			colModel:[
				{name:"pk_id_item_uom_category", index:"pk_id_item_uom_category",hidden: true},
				{name:"uom_name", index:"uom_name", width: 400, cellattr: default_cellattr_left}
			],
			autowidth: true,
			rowNum: 10,
			rowList: [10,20,30],
			mtype: "POST",
			pager: "#search-uom-name-pager",
			sortname: "uom_name",
			sortorder: "asc",
			viewrecords: true,
			gridComplete: function() {
				$("#search-uom-name-container").centerModal(true);
			},
			loadError : serverError
		}).jqGrid("navGrid", "#search-uom-name-pager",{del:false, view:false, add:false, edit:false, search:true}, {}, {}, {}, {multipleSearch: true});
		
		$("#jqgh_search-uom-name-grid_uom_name").addClass("jqgrid-column-left");
		
		$("#search-uom-name-container").modal({
			title: "Select UOM Name",
			width: 500,
			scrolling: false,
			resizable: false,
			actions: {
				'Close' : {
					color: 'red',
					click: function(win) { win.closeModal(); }
				}
			},
			buttons: {
				'Ok': {
					classes: "green-gradient glossy full-width",
					click: function(win) { 
						var sel_id = $("#search-uom-name-grid").jqGrid("getGridParam", "selrow");
						if(sel_id == null) {
							$.modal.alert("No Data Selected.");
							return;
						}
						if(stock_moves_id != "") {
							var row_data = $("#search-uom-name-grid").jqGrid("getRowData", sel_id);
							$("#" + stock_moves_id + "_pk_id_item_units_of_measure").val(sel_id);
							$("#" + stock_moves_id + "_pk_id_item_uom_category").val(row_data.pk_id_item_uom_category);
							$("#" + stock_moves_id + "_uom_name").val(row_data.uom_name);
						}
						win.closeModal(); 
					}
				}
			},
			onClose: function() {
				$("#search-uom-name-container").html("");
			},
			buttonsLowPadding: true
		});
	}
</script>