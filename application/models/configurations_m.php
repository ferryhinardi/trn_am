<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configurations_m extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get_configuration() {
		$query = "
			SELECT * FROM configurations
			LIMIT 1
		";
		
		return $this->db->query($query);
	}
	
	function modify(
		$application_name, $company_name, $company_address, 
		$company_contact_number_1, $company_contact_number_2, $company_logo, $after_login_messages
	) {
		$this->db->set("application_name", $application_name);
		$this->db->set("company_name", $company_name);
		$this->db->set("company_address", $company_address == "" ? null : $company_address);
		$this->db->set("company_contact_number_1", $company_contact_number_1 == "" ? null : $company_contact_number_1);
		$this->db->set("company_contact_number_2", $company_contact_number_2 == "" ? null : $company_contact_number_2);
		$this->db->set("after_login_messages", $after_login_messages == "" ? null : $after_login_messages);
		if($company_logo != "" && $company_logo != "company_logo") $this->db->set("company_logo", $company_logo);
		
		$this->db->update("configurations");
	}
}
