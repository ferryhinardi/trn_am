<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_categories_m extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function insert($item_category_name, $parent_item_category_id=null) {
		if($item_category_name != null) $this->db->set("item_category_name", $item_category_name);
		if($parent_item_category_id != null) $this->db->set("parent_item_category_id", $parent_item_category_id);
	
		$this->db->insert("item_categories"); 
		return $this->db->insert_id();
	}
	
	function update($pk_id_item_category, $item_category_name=null, $parent_item_category_id=null) {
		if($item_category_name != null) $this->db->set("item_category_name", $item_category_name);
		$this->db->set("parent_item_category_id", $parent_item_category_id == "" ? null : $parent_item_category_id);
		
		if($pk_id_item_category != null) {
			$this->db->where("pk_id_item_category", $pk_id_item_category);
			$this->db->update("item_categories");
		}
	}
	
	function delete($pk_id_item_category) {
		if($pk_id_item_category != null) {
			$this->db->where("pk_id_item_category", $pk_id_item_category);
			$this->db->delete("item_categories");
		}
	}
	
	function process_conditions($params) {
		$where = "";
		if($params["search_on"]) {
			$searchstr = $params["json_filter"];
			
			$qwery = "";
			//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
			$qopers = array(
						  'eq'=>" = ",
						  'ne'=>" <> ",
						  'lt'=>" < ",
						  'le'=>" <= ",
						  'gt'=>" > ",
						  'ge'=>" >= ",
						  'bw'=>" LIKE ",
						  'bn'=>" NOT LIKE ",
						  'in'=>" IN ",
						  'ni'=>" NOT IN ",
						  'ew'=>" LIKE ",
						  'en'=>" NOT LIKE ",
						  'cn'=>" LIKE " ,
						  'nc'=>" NOT LIKE " );
			if ($searchstr) {
				$jsona = json_decode($searchstr, true);				
				
				if(is_array($jsona)){
					$gopr = $jsona['groupOp'];
					$rules = $jsona['rules'];
					
					$i =0;
					foreach($rules as $key=>$val) {
					
						$field = $val['field'];
						$op = $val['op'];
						$v = $val['data'];
						if($v && $op) {
							$i++;
							switch ($field) {
								default :
									if($op=='bw' || $op=='bn') $v =  "'" . addslashes($v) . "%'";
									else if ($op=='ew' || $op=='en') $v =  "'%" . addcslashes($v) . "'";
									else if ($op=='cn' || $op=='nc') $v =  "'%" . addslashes($v) . "%'";
									else $v =  "'" . addslashes($v) . "'";
							}
							if ($i == 1) $qwery = " AND ";
							else $qwery .= " " .$gopr." ";
							switch ($op) {
								case 'in' :
								case 'ni' :
									$qwery .= $field.$qopers[$op]." (".$v.")";
									break;
								default:
									$qwery .= $field.$qopers[$op].$v;
							};
						}
					}
				}
			}
			
			$where = $qwery;	
		}
		
		return $where;
	}
	
	function get_parents($parent_item_category_id) {
		$query = "SELECT * FROM item_categories WHERE pk_id_item_category = {$parent_item_category_id}";
		
		return $this->db->query($query);
	}
	
	function get_children($pk_id_item_category) {
		$query = "SELECT * FROM item_categories WHERE parent_item_category_id = {$pk_id_item_category}";
		
		return $this->db->query($query);
	}
	
	function get_item_category_list($params) {
		$query = "
			SELECT 
				child.pk_id_item_category AS id_item_category, 
				child.item_category_name AS child_item_category_name, 
				child.parent_item_category_id AS parent_id, 
				parent.item_category_name AS parent_item_category_name
			FROM item_categories child
			LEFT JOIN item_categories parent ON child.parent_item_category_id = parent.pk_id_item_category
			WHERE 1=1 "
			. ($params["invalid_id"] != "" ? "AND child.pk_id_item_category NOT IN ({$params["invalid_id"]}) " : "")
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
}
