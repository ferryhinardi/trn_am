<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Internal_moves_m extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function insert(
		$item_internal_move_code, $item_internal_move_date, 
		$item_internal_move_notes, $pk_id_items, $quantitys, $pk_id_item_units_of_measures, 
		$pk_id_production_lots,$pk_id_item_packages, $pk_id_item_packs, 
		$source_location_ids, $destination_location_ids, $pk_id_partners
	) {
		if($item_internal_move_code != null) $this->db->set("item_internal_move_code", $item_internal_move_code);
		if($item_internal_move_date != null) $this->db->set("item_internal_move_date", $item_internal_move_date);
		if($item_internal_move_notes != null) $this->db->set("item_internal_move_notes", $item_internal_move_notes);
	
		$this->db->insert("item_internal_moves"); 
		$new_id = $this->db->insert_id();
		
		for($i=0; $i<count($pk_id_items) && $i<count($quantitys) && $i<count($destination_location_ids) && $i<count($source_location_ids); $i++) {
		
			$this->db->set("pk_id_internal_move", $new_id);
			$this->db->set("stock_move_date", $item_internal_move_date);
			$this->db->set("pk_id_item", $pk_id_items[$i]);
			$this->db->set("quantity", $quantitys[$i]);
			$this->db->set("pk_id_item_units_of_measure", $pk_id_item_units_of_measures[$i]);
			$this->db->set("pk_id_production_lot", $pk_id_production_lots[$i] == "" ? null : $pk_id_production_lots[$i]);
			$this->db->set("pk_id_item_package", $pk_id_item_packages[$i] == "" ? null : $pk_id_item_packages[$i]);
			$this->db->set("pk_id_item_pack", $pk_id_item_packs[$i] == "" ? null : $pk_id_item_packs[$i]);
			$this->db->set("source_location_id", $source_location_ids[$i]);
			$this->db->set("destination_location_id", $destination_location_ids[$i]);
			$this->db->set("pk_id_partner", $pk_id_partners[$i] == "" ? null : $pk_id_partners[$i]);
			
			
			$this->db->insert("stock_moves");
		}
		return $new_id;
	}
	
	function update(
		$pk_id_item_internal_move, $item_internal_move_code, $item_internal_move_date, 
		$item_internal_move_notes, $pk_id_items, $quantitys, $pk_id_item_units_of_measures, 
		$pk_id_production_lots,$pk_id_item_packages, $pk_id_item_packs, 
		$source_location_ids, $destination_location_ids, $pk_id_partners
	) {
		if($item_internal_move_code != null) $this->db->set("item_internal_move_code", $item_internal_move_code);
		if($item_internal_move_date != null) $this->db->set("item_internal_move_date", $item_internal_move_date);
		if($item_internal_move_notes != null) $this->db->set("item_internal_move_notes", $item_internal_move_notes);
		
		if($pk_id_item_internal_move != null) {
			$this->db->where("pk_id_item_internal_move", $pk_id_item_internal_move);
			$this->db->update("item_internal_moves");
			
			$this->db->where("pk_id_internal_move", $pk_id_item_internal_move);
			$this->db->delete("stock_moves");
			
			if($pk_id_items[0] != "" && $quantitys[0] != "" && $source_location_ids[0] != "" && $destination_location_ids[0] != "")
				for($i=0; $i<count($pk_id_items) && $i<count($quantitys) && $i<count($destination_location_ids) && $i<count($source_location_ids); $i++) {
				
					$this->db->set("pk_id_internal_move", $pk_id_item_internal_move);
					$this->db->set("pk_id_item", $pk_id_items[$i]);
					$this->db->set("stock_move_date", $item_internal_move_date);
					$this->db->set("quantity", $quantitys[$i]);
					$this->db->set("pk_id_item_units_of_measure", $pk_id_item_units_of_measures[$i]);
					$this->db->set("pk_id_production_lot", $pk_id_production_lots[$i] == "" ? null : $pk_id_production_lots[$i]);
					$this->db->set("pk_id_item_package", $pk_id_item_packages[$i] == "" ? null : $pk_id_item_packages[$i]);
					$this->db->set("pk_id_item_pack", $pk_id_item_packs[$i] == "" ? null : $pk_id_item_packs[$i]);
					$this->db->set("source_location_id", $source_location_ids[$i] == "" ? null : $source_location_ids[$i]);
					$this->db->set("destination_location_id", $destination_location_ids[$i] == "" ? null : $destination_location_ids[$i]);
					$this->db->set("pk_id_partner", $pk_id_partners[$i] == "" ? null : $pk_id_partners[$i]);
			
			$this->db->insert("stock_moves");
		}
	}
}
	
	function delete($pk_id_item_internal_move) {
		if($pk_id_item_internal_move != null) {
			$this->db->where("pk_id_item_internal_move", $pk_id_item_internal_move);
			$this->db->delete("item_internal_moves");
		}
	}
	
	function update_state($pk_id_item_internal_move, $internal_move_state) {
		if($pk_id_item_internal_move != null) {
			$this->db->set("internal_move_state", $internal_move_state);
			$this->db->where("pk_id_item_internal_move", $pk_id_item_internal_move);
			$this->db->update("item_internal_moves");
			
			$this->db->set("stock_move_state", $internal_move_state);
			$this->db->where("pk_id_internal_move", $pk_id_item_internal_move);
			$this->db->update("stock_moves");
		}
	}
	
	function process_conditions($params) {
		$where = "";
		if($params["search_on"]) {
			$searchstr = $params["json_filter"];
			
			$qwery = "";
			//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
			$qopers = array(
						  'eq'=>" = ",
						  'ne'=>" <> ",
						  'lt'=>" < ",
						  'le'=>" <= ",
						  'gt'=>" > ",
						  'ge'=>" >= ",
						  'bw'=>" LIKE ",
						  'bn'=>" NOT LIKE ",
						  'in'=>" IN ",
						  'ni'=>" NOT IN ",
						  'ew'=>" LIKE ",
						  'en'=>" NOT LIKE ",
						  'cn'=>" LIKE " ,
						  'nc'=>" NOT LIKE " );
			if ($searchstr) {
				$jsona = json_decode($searchstr, true);				
				
				if(is_array($jsona)){
					$gopr = $jsona['groupOp'];
					$rules = $jsona['rules'];
					
					$i =0;
					foreach($rules as $key=>$val) {
					
						$field = $val['field'];
						$op = $val['op'];
						$v = $val['data'];
						if($v && $op) {
							$i++;
							switch ($field) {
								default :
									if($op=='bw' || $op=='bn') $v =  "'" . addslashes($v) . "%'";
									else if ($op=='ew' || $op=='en') $v =  "'%" . addcslashes($v) . "'";
									else if ($op=='cn' || $op=='nc') $v =  "'%" . addslashes($v) . "%'";
									else $v =  "'" . addslashes($v) . "'";
							}
							if ($i == 1) $qwery = " AND ";
							else $qwery .= " " .$gopr." ";
							switch ($op) {
								case 'in' :
								case 'ni' :
									$qwery .= $field.$qopers[$op]." (".$v.")";
									break;
								default:
									$qwery .= $field.$qopers[$op].$v;
							};
						}
					}
				}
			}
			
			$where = $qwery;	
		}
		
		return $where;
	}
		
	function get_internal_moves_list($params) {
	
		$query = "
			SELECT pk_id_item_internal_move as id_internal_move, item_internal_move_code, internal_move_state, DATE_FORMAT(item_internal_move_date, '%Y-%m-%d %H:%i') AS item_internal_move_date,
			item_internal_move_notes 
			FROM item_internal_moves  
			WHERE 1=1 AND internal_move_state = 'Draft' "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_internal_moves_list_search($params) {
	
		$query = "
			SELECT pk_id_item_internal_move as id_internal_move, item_internal_move_code, internal_move_state, DATE_FORMAT(item_internal_move_date, '%Y-%m-%d %H:%i') AS item_internal_move_date,
			item_internal_move_notes 
			FROM item_internal_moves  
			WHERE 1=1 "
			. $this->process_conditions($params);
		
		$query .= ($params['state'] != null) ? " AND internal_move_state IN( '" .$params['state']. "' ) " : "AND internal_move_state != 'Draft' && internal_move_state != 'Done' ";
		$query .= ($params['date_from'] != null) ? " AND DATE(item_internal_move_date) BETWEEN '" .$params['date_from']. "' " : "";
		$query .= ($params['date_until'] != null) ? " AND '" .$params['date_until']. "' " : "";
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
		
		
		return $this->db->query($query);
	}
	
	function get_stock_moves_list($id_internal_move) {
	
		$query = "
			SELECT sm.pk_id_item, sm.quantity, sm.pk_id_item_units_of_measure,
			sm.pk_id_production_lot, sm.pk_id_item_pack, sm.source_location_id, sm.destination_location_id,
			sm.pk_id_item_package, sm.pk_id_partner, pl.production_lot_name, ipack.pack_name, p.partner_name,
			i.item_name, iuom.uom_name, iuom.pk_id_item_uom_category, sl1.location_name AS source_location_name, sl2.location_name AS destination_location_name
			FROM stock_moves sm 
			LEFT JOIN items i ON sm.pk_id_item = i.pk_id_item
			LEFT JOIN item_units_of_measures iuom ON sm.pk_id_item_units_of_measure = iuom.pk_id_item_units_of_measure
			LEFT JOIN production_lots pl on sm.pk_id_production_lot = pl.pk_id_production_lot
			LEFT JOIN item_packages ip ON sm.pk_id_item_package = ip.pk_id_item_packages
			LEFT JOIN item_packs ipack ON sm.pk_id_item_pack = ipack.pk_id_item_pack
			LEFT JOIN partners p ON sm.pk_id_partner = p.pk_id_partner
			LEFT JOIN stock_locations sl1 ON sm.source_location_id = sl1.pk_id_stock_location
			LEFT JOIN stock_locations sl2 ON sm.destination_location_id = sl2.pk_id_stock_location
			
			WHERE pk_id_internal_move = {$id_internal_move} ";
			
			/*AND (p.is_active IS NULL OR p.is_active = 1)
			AND (i.is_active IS NULL OR i.is_active = 1)
			AND (iuom.is_active IS NULL OR iuom.is_active = 1) ";*/
			
		return $this->db->query($query);
	}
	
	function get_stock_moves_list_report($id_internal_move) {
	
	if($id_internal_move != '')
		$query = "
			SELECT sm.pk_id_item, sm.quantity, sm.pk_id_item_units_of_measure,
			sm.pk_id_production_lot, sm.pk_id_item_pack, sm.source_location_id, sm.destination_location_id,
			sm.pk_id_item_package, sm.pk_id_partner, pl.production_lot_name, ipack.pack_name, p.partner_name, ip.package_name,
			i.item_name, iuom.uom_name, iuom.pk_id_item_uom_category, sl1.location_name AS source_location_name, sl2.location_name AS destination_location_name
			FROM stock_moves sm 
			LEFT JOIN items i ON sm.pk_id_item = i.pk_id_item
			LEFT JOIN item_units_of_measures iuom ON sm.pk_id_item_units_of_measure = iuom.pk_id_item_units_of_measure
			LEFT JOIN production_lots pl on sm.pk_id_production_lot = pl.pk_id_production_lot
			LEFT JOIN item_packages ip ON sm.pk_id_item_package = ip.pk_id_item_packages
			LEFT JOIN item_packs ipack ON sm.pk_id_item_pack = ipack.pk_id_item_pack
			LEFT JOIN partners p ON sm.pk_id_partner = p.pk_id_partner
			LEFT JOIN stock_locations sl1 ON sm.source_location_id = sl1.pk_id_stock_location
			LEFT JOIN stock_locations sl2 ON sm.destination_location_id = sl2.pk_id_stock_location
			
			WHERE pk_id_internal_move = {$id_internal_move} ";
			
			else
			
			$query = "
			SELECT sm.pk_id_item, sm.quantity, sm.pk_id_item_units_of_measure,
			sm.pk_id_production_lot, sm.pk_id_item_pack, sm.source_location_id, sm.destination_location_id,
			sm.pk_id_item_package, sm.pk_id_partner, sm.pk_id_internal_move, pl.production_lot_name, ipack.pack_name, p.partner_name, ip.package_name,
			i.item_name, iuom.uom_name, iuom.pk_id_item_uom_category, sl1.location_name AS source_location_name, sl2.location_name AS destination_location_name
			FROM stock_moves sm 
			LEFT JOIN items i ON sm.pk_id_item = i.pk_id_item
			LEFT JOIN item_units_of_measures iuom ON sm.pk_id_item_units_of_measure = iuom.pk_id_item_units_of_measure
			LEFT JOIN production_lots pl on sm.pk_id_production_lot = pl.pk_id_production_lot
			LEFT JOIN item_packages ip ON sm.pk_id_item_package = ip.pk_id_item_packages
			LEFT JOIN item_packs ipack ON sm.pk_id_item_pack = ipack.pk_id_item_pack
			LEFT JOIN partners p ON sm.pk_id_partner = p.pk_id_partner
			LEFT JOIN stock_locations sl1 ON sm.source_location_id = sl1.pk_id_stock_location
			LEFT JOIN stock_locations sl2 ON sm.destination_location_id = sl2.pk_id_stock_location ";

			
		return $this->db->query($query)->result();
	}
	
	function get_internal_moves_list_report($id_internal_move, $state, $date_from, $date_until) {	
		
		if($id_internal_move != 0 && $id_internal_move != null) 
		{
			$query = "SELECT * FROM item_internal_moves  WHERE pk_id_item_internal_move = {$id_internal_move} ";		
			return $this->db->query($query)->row();
		}
		else 
		{
			$query = "SELECT * FROM item_internal_moves WHERE 1=1 ";	
			
			$query .= ($state != "" && $state != "all") ? " AND internal_move_state IN( '{$state}' )" : "";
			$query .= ($date_from != null && $date_from != 0) ? " AND DATE(item_internal_move_date) BETWEEN '{$date_from}' " : "";
			$query .= ($date_until != null && $date_until != 0) ? " AND '{$date_until}' " : "";

			return $this->db->query($query)->result();			
		}
				
}
	
	function get_item_search_list($params) {
		$query = "
			SELECT DISTINCT i.pk_id_item, i.item_name, iuom.uom_name, iuom.pk_id_item_units_of_measure, iuom.pk_id_item_uom_category
			FROM items i
			LEFT JOIN stock_moves sm ON i.pk_id_item = sm.pk_id_item
			LEFT JOIN item_units_of_measures iuom ON i.pk_id_item_units_of_measure = iuom.pk_id_item_units_of_measure
			
			WHERE i.is_active = 1 "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_uom_search_list($params) {
		$query = "
			SELECT pk_id_item_units_of_measure, pk_id_item_uom_category, uom_name
			FROM item_units_of_measures
			
			WHERE pk_id_item_uom_category = ". $params['pk_id_item_uom_category']. " AND is_active = 1"
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_production_lot_search_list($params) {
		$query = "
			SELECT pk_id_production_lot, production_lot_name
			FROM production_lots "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_package_search_list() {
		$query = "
			SELECT pk_id_item_packages, package_name
			FROM item_packages ";
				
		return $this->db->query($query);
	}
	
	function get_configuration() {	
		$query = "
			SELECT *
			FROM configurations ";
			
			return $this->db->query($query)->row();
	}
	
	function get_pack_search_list($params) {
		$query = "
			SELECT pk_id_item_pack, pack_name
			FROM item_packs "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_source_search_list($params) {
		$query = "
			SELECT pk_id_stock_location AS id_source_location, location_name AS source_location_name
			FROM stock_locations WHERE location_type = 'Internal Location' "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_destination_search_list($params) {
		$query = "
			SELECT pk_id_stock_location AS id_destination_location, location_name AS destination_location_name
			FROM stock_locations WHERE location_type = 'Internal Location' "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_makloon_search_list($params) {
		$query = "
			SELECT 
				pk_id_partner AS id_makloon_partner, partner_name AS partner_makloon_name
			FROM partners
			WHERE is_makloon = 1 AND is_active = 1 "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_row() {
		$query = "
			SELECT 
				pk_id_item_internal_move
			FROM item_internal_moves
			WHERE 1=1 ";
			
				
		return $this->db->query($query);
	}
	
	function getMaxCode($year, $month)
	{
		$query = "
			SELECT 
				CAST(MAX( CONVERT( RIGHT( item_internal_move_code, 4) , SIGNED )+1) AS CHAR) AS max_code
			FROM item_internal_moves
			WHERE SUBSTRING( item_internal_move_code, 5 , 2) = ".$year." AND  SUBSTRING( item_internal_move_code, 7 , 2) = ".$month
			;
			
		return $this->db->query($query);
	}
	
	function getMaxYear()
	{
		$query = "
			SELECT 
				CAST(MAX( CONVERT( SUBSTRING( item_internal_move_code, 5 , 2) , SIGNED )) AS CHAR) AS max_year
			FROM item_internal_moves
			WHERE 1=1 "
			;
			
		return $this->db->query($query);
	}
	
	function getMaxMonth($year)
	{
		$query = "
			SELECT 
				CAST(MAX( CONVERT( SUBSTRING( item_internal_move_code, 7 , 2) , SIGNED )) AS CHAR) AS max_month
			FROM item_internal_moves
			WHERE  SUBSTRING( item_internal_move_code, 5 , 2) = ".$year
			;
			
		return $this->db->query($query);
	}
}
