<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items_m extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get_curr_stock_onhand($pk_id_item) {
		return $this->db->query("SELECT GET_STOCK_ONHAND('{$pk_id_item}', 'ALL', 'ALL', 'ALL', 'ALL', NULL) AS curr_stock_onhand");
	}
	
	function stock_adjust(
		$pk_id_item, $stock_on_hand, $qty_adjust, $id_location_adjust, $id_packaging_adjust, $id_pack_adjust, 
		$id_lot_adjust, $location_loss_id, $pk_id_item_units_of_measure, $stock_move_date, $is_loss
	) {
		$this->db->set("pk_id_item", $pk_id_item);
		$this->db->set("pk_id_item_units_of_measure", $pk_id_item_units_of_measure);
		$this->db->set("stock_move_date", $stock_move_date);
		$this->db->set("stock_move_state", "Done");
		$this->db->set("pk_id_item_package", $id_packaging_adjust == "" ? null : $id_packaging_adjust);
		$this->db->set("pk_id_item_pack", $id_pack_adjust == "" ? null : $id_pack_adjust);
		$this->db->set("pk_id_production_lot", $id_lot_adjust == "" ? null : $id_lot_adjust);
		
		if($is_loss) {
			$this->db->set("source_location_id", $id_location_adjust);
			$this->db->set("destination_location_id", $location_loss_id);
			$this->db->set("quantity", $stock_on_hand - $qty_adjust);
		} else {
			$this->db->set("source_location_id", $location_loss_id);
			$this->db->set("destination_location_id", $id_location_adjust);
			$this->db->set("quantity", $qty_adjust - $stock_on_hand);
		}
		
		$this->db->insert("stock_moves"); 
		return $qty_adjust;
	}
	
	function insert(
		$item_name, $pk_id_item_category, $is_can_be_sold=1, $is_can_be_purchased=1, $is_can_be_produced=1, 
		$life_time=null, $pk_id_item_units_of_measure, $location_production_id=null, $location_loss_id=null, 
		$sale_price, $cost_price, $is_active=1, $pk_id_partners, $supplier_item_names
	) {
		if($item_name != null) $this->db->set("item_name", $item_name);
		if($pk_id_item_category != null) $this->db->set("pk_id_item_category", $pk_id_item_category);
		if($is_can_be_sold != null) $this->db->set("is_can_be_sold", $is_can_be_sold);
		if($is_can_be_purchased != null) $this->db->set("is_can_be_purchased", $is_can_be_purchased);
		if($is_can_be_produced != null) $this->db->set("is_can_be_produced", $is_can_be_produced);
		if($life_time != null) $this->db->set("life_time", $life_time);
		if($pk_id_item_units_of_measure != null) $this->db->set("pk_id_item_units_of_measure", $pk_id_item_units_of_measure);
		if($location_production_id != null) $this->db->set("location_production_id", $location_production_id);
		if($location_loss_id != null) $this->db->set("location_loss_id", $location_loss_id);
		if($sale_price != null) $this->db->set("sale_price", $sale_price);
		if($cost_price != null) $this->db->set("cost_price", $cost_price);
	
		$this->db->insert("items"); 
		$new_id = $this->db->insert_id();
		
		$total_detail = count($pk_id_partners);
		if(!($total_detail == 1 && $pk_id_partners[0] == "")) 
			for($i=0; $i<count($pk_id_partners); $i++) {
				$this->db->set("pk_id_item", $new_id);
				$this->db->set("pk_id_partner", $pk_id_partners[$i]);
				$this->db->set("supplier_item_name", $supplier_item_names[$i] == "" ? null : $supplier_item_names[$i]);
				
				$this->db->insert("item_suppliers");
			}
		
		return $new_id;
	}
	
	function update(
		$pk_id_item, $item_name, $pk_id_item_category, $is_can_be_sold=1, $is_can_be_purchased=1, $is_can_be_produced=1, 
		$life_time=null, $pk_id_item_units_of_measure, $location_production_id=null, $location_loss_id=null, 
		$sale_price, $cost_price, $is_active=1, $pk_id_partners, $supplier_item_names
	) {
		if($item_name != null) $this->db->set("item_name", $item_name);
		if($pk_id_item_category != null) $this->db->set("pk_id_item_category", $pk_id_item_category);
		if($is_can_be_sold != null) $this->db->set("is_can_be_sold", $is_can_be_sold);
		if($is_can_be_purchased != null) $this->db->set("is_can_be_purchased", $is_can_be_purchased);
		if($is_can_be_produced != null) $this->db->set("is_can_be_produced", $is_can_be_produced);
		$this->db->set("life_time", $life_time == "" ? null : $life_time);
		if($pk_id_item_units_of_measure != null) $this->db->set("pk_id_item_units_of_measure", $pk_id_item_units_of_measure);
		$this->db->set("location_production_id", $location_production_id == "" ? null : $location_production_id);
		$this->db->set("location_loss_id", $location_loss_id == "" ? null : $location_loss_id);
		if($sale_price != null) $this->db->set("sale_price", $sale_price);
		if($cost_price != null) $this->db->set("cost_price", $cost_price);
		if($is_active != null) $this->db->set("is_active", $is_active);
		
		if($pk_id_item != null) {
			$this->db->where("pk_id_item", $pk_id_item);
			$this->db->update("items");
			
			$this->db->where("pk_id_item", $pk_id_item);
			$this->db->delete("item_suppliers");
			
			if($pk_id_partners[0] != "")
				for($i=0; $i<count($pk_id_partners); $i++) {
					$this->db->set("pk_id_item", $pk_id_item);
					$this->db->set("pk_id_partner", $pk_id_partners[$i]);
					$this->db->set("supplier_item_name", $supplier_item_names[$i] == "" ? null : $supplier_item_names[$i]);
					
					$this->db->insert("item_suppliers");
				}
		}
	}
	
	function delete($pk_id_item) {
		if($pk_id_item != null) {
			$this->db->where("pk_id_item", $pk_id_item);
			$this->db->delete("items");
		}
	}
	
	function process_conditions($params) {
		$where = "";
		if($params["search_on"]) {
			$searchstr = $params["json_filter"];
			
			$qwery = "";
			//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
			$qopers = array(
						  'eq'=>" = ",
						  'ne'=>" <> ",
						  'lt'=>" < ",
						  'le'=>" <= ",
						  'gt'=>" > ",
						  'ge'=>" >= ",
						  'bw'=>" LIKE ",
						  'bn'=>" NOT LIKE ",
						  'in'=>" IN ",
						  'ni'=>" NOT IN ",
						  'ew'=>" LIKE ",
						  'en'=>" NOT LIKE ",
						  'cn'=>" LIKE " ,
						  'nc'=>" NOT LIKE " );
			if ($searchstr) {
				$jsona = json_decode($searchstr, true);				
				
				if(is_array($jsona)){
					$gopr = $jsona['groupOp'];
					$rules = $jsona['rules'];
					
					$i =0;
					foreach($rules as $key=>$val) {
					
						$field = $val['field'];
						$op = $val['op'];
						$v = $val['data'];
						if($v && $op) {
							$i++;
							switch ($field) {
								default :
									if($op=='bw' || $op=='bn') $v =  "'" . addslashes($v) . "%'";
									else if ($op=='ew' || $op=='en') $v =  "'%" . addcslashes($v) . "'";
									else if ($op=='cn' || $op=='nc') $v =  "'%" . addslashes($v) . "%'";
									else $v =  "'" . addslashes($v) . "'";
							}
							if ($i == 1) $qwery = " AND ";
							else $qwery .= " " .$gopr." ";
							switch ($op) {
								case 'in' :
								case 'ni' :
									$qwery .= $field.$qopers[$op]." (".$v.")";
									break;
								default:
									$qwery .= $field.$qopers[$op].$v;
							};
						}
					}
				}
			}
			
			$where = $qwery;	
		}
		
		return $where;
	}
	
	function get_item_supplier_list($pk_id_item) {
		$query = "
			SELECT itms.pk_id_partner AS id_partner, partner_name, contact_name, email, IFNULL(phone, IFNULL(mobile, fax)) AS contact_no, supplier_item_name
			FROM item_suppliers itms
			JOIN partners prt ON itms.pk_id_partner = prt.pk_id_partner
			WHERE pk_id_item = {$pk_id_item}
			AND is_supplier = 1 AND is_active = 1
			ORDER BY partner_name ASC
		";
		
		return $this->db->query($query);
	}
	
	function get_item_package_list() {
		$query = "
			SELECT pk_id_item_packages, package_name
			FROM item_packages
		";
		
		return $this->db->query($query);
	}
	
	function get_item_list($params) {
		$query = "
			SELECT item.*, UOM_CONVERSION(balance_quantity, uom_id, pk_id_item_units_of_measure) AS balance_quantity, posting_date FROM 
			(
				SELECT 
					pk_id_item,
					item_name,
					item.pk_id_item_category AS pk_id_item_category,
					item_category_name,
					parent_item_category_id,
					is_can_be_sold,
					is_can_be_purchased,
					is_can_be_produced,
					life_time,
					pk_id_item_units_of_measure,
						(
							SELECT uom_name 
							FROM item_units_of_measures 
							WHERE pk_id_item_units_of_measure = item.pk_id_item_units_of_measure
						) AS uom_name,
					GET_STOCK_ONHAND(pk_id_item, 'ALL', 'ALL', 'ALL', 'ALL', NULL) AS stock_on_hand_posting,
					stock_on_sale_posting,
					stock_on_purchase_posting,
					stock_on_produce_posting,
					location_production_id,
						(
							SELECT location_name 
							FROM stock_locations 
							WHERE pk_id_stock_location = item.location_production_id
						) AS location_production,
					location_loss_id,
						(
							SELECT location_name 
							FROM stock_locations 
							WHERE pk_id_stock_location = item.location_loss_id
						) AS location_loss,
					sale_price,
					cost_price,
					is_active
				FROM items item
				JOIN item_categories ON item.pk_id_item_category = item_categories.pk_id_item_category
			)AS item
			LEFT JOIN (
				SELECT SUM(balance_quantity) AS balance_quantity, DATE_FORMAT(posting_date, '%Y-%m-%d %H:%i') AS posting_date, pk_id_item AS item_id, pk_id_item_units_of_measure AS uom_id
				FROM stock_balances balance
				WHERE posting_date = IFNULL((
						SELECT MAX(posting_date) 
						FROM stock_balances 
						WHERE balance.pk_id_item = pk_id_item
						AND pk_id_partner IS NULL
						AND stock_balance_state = 'Done'
				), '0000-00-00')
				AND pk_id_partner IS NULL
				AND stock_balance_state = 'Done'
				GROUP BY pk_id_item
			)AS balance ON item.pk_id_item = balance.item_id
			WHERE 1=1 "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_supplier_search_list($params) {
		$query = "
			SELECT * FROM (
				SELECT pk_id_partner, partner_name, contact_name, email, IFNULL(phone, IFNULL(mobile, fax)) AS contact_no
				FROM partners 
				WHERE is_supplier = 1 AND is_active = 1 " .
				($params["id_not_in_filter"] != null && $params["id_not_in_filter"] != "" ? 
					"AND pk_id_partner NOT IN ({$params["id_not_in_filter"]})" : "") .
			") AS partners"
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_pack_list($params) {
		$query = "
			SELECT pk_id_item_pack, pack_name
			FROM item_packs "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_production_lot_list($params) {
		$query = "
			SELECT pk_id_production_lot, production_lot_name
			FROM production_lots "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
	
	function get_makloon_stock_list($params) {
		$query = "
			SELECT *
			FROM (
				SELECT pk_id_partner, partner_name, GET_STOCK_ONHAND({$params["pk_id_item"]}, 'ALL', 'ALL', 'ALL', 'ALL', pk_id_partner) AS stock_on_hand_posting, 0 AS stock_on_produce_posting
				FROM partners
				WHERE is_active = 1
				AND is_makloon = 1
				AND EXISTS (
					SELECT 'x' FROM stock_moves WHERE pk_id_partner = partners.pk_id_partner AND pk_id_item = {$params["pk_id_item"]}
				) OR EXISTS (
					SELECT 'x' FROM stock_balances WHERE pk_id_partner = partners.pk_id_partner AND pk_id_item = {$params["pk_id_item"]}
				) 
			) partners	
			WHERE 1=1 "
			. $this->process_conditions($params);
		
		$query .= ($params['sort_by'] != null) ? " ORDER BY ". $params['sort_by'].' '. $params['sort_direction'] :'';
		$query .= ($params['limit'] != null) ? " LIMIT ".(($params['limit']['start'] < 0) ? 0:$params['limit']['start']).','.$params['limit']['end']: '';
				
		return $this->db->query($query);
	}
}
